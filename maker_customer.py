from lxml import builder, etree


attr_qname = etree.QName('http://www.w3.org/2001/XMLSchema-instance', 'schemaLocation')
namespaces = {
    None: 'urn:oasis:names:specification:ubl:schema:xsd:AttachedDocument-2',
    'cac': 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2',
    'cbc': 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2',
    'ccts': 'urn:un:unece:uncefact:data:specification:CoreComponentTypeSchemaModule:2',
    'ext': 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2',
    'xades': 'http://uri.etsi.org/01903/v1.3.2#',
    'xades141': 'http://uri.etsi.org/01903/v1.4.1#',
    'ds': 'http://www.w3.org/2000/09/xmldsig#',
}

AttachedDocument = builder.ElementMaker(nsmap=namespaces)

ext_customer = builder.ElementMaker(namespace=namespaces['ext'])
cbc_customer = builder.ElementMaker(namespace=namespaces['cbc'])
cac_customer = builder.ElementMaker(namespace=namespaces['cac'])
ds_customer = builder.ElementMaker(namespace=namespaces['ds'])
xades_customer = builder.ElementMaker(namespace=namespaces['xades'])
xades141_customer = builder.ElementMaker(namespace=namespaces['xades141'])
