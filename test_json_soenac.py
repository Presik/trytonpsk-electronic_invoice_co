

# TAXES_CODE = {
#     'iva': '01',
#     'ico': '02',
#     'ica': '03',
#     'inaco': '04'
# }
#
# CLASIFICATION_TAX = {
#     '01': 'IVA',
#     '02': 'IC',
#     '03': 'ICA',
#     '04': 'INC',
#     '05': 'ReteIVA',
#     '06': 'ReteFuente',
#     '07': 'ReteICA',
#     '20': 'FtoHorticultura',
#     '21': 'Timbre',
#     '22': 'Bolsas',
#     '23': 'INCarbono',
#     '24': 'INCombustibles',
#     '25': 'Sobretasa Combustibles',
#     '26': 'Sordicom',
#     'ZZ': 'Otro',
#     }
#
# TYPE_PERSON = {
#     'persona_juridica': '1',
#     'persona_natural': '2',
# }
#
# ENVIRONMENT = {
#     '1': 'Produccion',
#     '2': 'Pruebas',
# }
#
# INVOICE_CODES = {
#     1: 'venta',
# }
#
# HEADERS = {
#            'Content-type': 'application/json',
#            'accept': 'application/json',
#            }
#
# MESSAGES = {
#     'company_id': 'Falta numero NIT de la empresa',
#     'company_registration': 'Falta la matricula mercantil de la empresa',
#     'company_name': 'Falta el nombre de la empresa',
#     'company_email': 'Falta el email de la empresa',
#     'company_phone': 'Falta el teléfono o celular de la empresa',
#     'company_city': 'Falta la ciudad de la empresa',
#     'company_city_code': 'Falta la ciudad de la empresa',
#     'company_address': 'Falta la direccion de la empresa',
#     'company_country_code': 'Falta el pais de la empresa',
#     'company_department': 'Falta el departamento de la empresa',
#     'company_department_code': 'Falta el departamento de la empresa',
#     'company_ciiu_code': 'Falta el codigo CIIU en el tercero de la empresa',
#     'company_postal_zone': 'Falta el codigo Postal en el tercero de la empresa',
#     'company_check_digit': 'Falta el digito de verificación en el tercero de la empresa',
#     'company_type_id': 'Falta el tipo de documento que identifica a la compañía',
#     'fiscal_regimen_company': 'Falta el Regimen Fiscal del tercero de la compañia',
#     'company_tax_level_code': 'Falta el Regimen de Impuestos del tercero de la compañia',
#     'currency': 'Falta el codigo de la moneda',
#     'party_name': 'Falta el nombre del cliente',
#     'party_id': 'Falta el id del cliente',
#     'party_address': 'Falta la direccion del cliente',
#     'issue_date': 'Falta la fecha de factura',
#     'party_country': 'Falta el pais del cliente',
#     'party_department': 'Falta el departmento del cliente',
#     # 'party_registration': 'Falta la matricula mercantil del tercero',
#     # 'party_postal_zone': 'Falta el codigo Postal en el tercero',
#     'party_city': 'Falta la ciudad del cliente',
#     'party_phone': 'Falta el telefono del cliente',
#     'party_email': 'Falta el correo del cliente',
#     'party_type_id': 'Falta el tipo de documento que identifica al cliente',
#     'party_tax_level_code': 'Falta definir el tipo de persona juridica / natural del cliente',
#     'payment_term': 'Falta el metodo de pago',
#     'fiscal_regimen_party': 'Falta el Regimen Fiscal del cliente',
#     'invoice_authorization': 'El campo Autorización de factura esta vacio',
#     'operation_type': 'El campo Tipo de Operación de factura esta vacio',
#     'invoice_type_code': 'El campo Tipo de Factura de factura esta vacio',
#     'payment_type': 'Falta el tipo de concepto en el plazo de pago de la factura',
#     'company_tributes': 'Falta definir el grupo de tributos de impuesto al que es responsable la compañía',
# }


# class ElectronicInvoicePsk(object):




    # def __init__(self, invoice, auth):
    #     self.invoice = invoice
    #     self.number = invoice.number
    #     #Company information -------------------------------------------------------------------------------------
    #     self.company_id = invoice.company.party.id_number
    #     self.company_type_id = invoice.company.party.type_document or ''
    #     self.company_registration = invoice.company.party.commercial_registration
    #     self.company_name = invoice.company.party.name
    #     self.company_phone = invoice.company.party.phone or invoice.company.party.mobile
    #     self.company_city = invoice.company.party.city_name
    #     self.company_city_code = invoice.company.party.department_code+invoice.company.party.city_code
    #     self.company_department_code = invoice.company.party.department_code
    #     self.company_address = invoice.company.party.street.replace('\n', '')
    #     self.company_check_digit = invoice.company.party.check_digit
    #     self.company_country_code = 'CO'
    #     self.company_postal_zone = [address.postal_code for address in invoice.company.party.addresses]
    #     self.company_ciiu_code = invoice.company.party.ciiu_code
    #     self.company_tributes = invoice.company.party.party_tributes or ''
    #     self.company_party_obligations = self.invoice.company.party.party_obligation_tax
    #     if not self.company_party_obligations:
    #         MESSAGES['company_party_obligations'] = 'Falta definir la obligaciones fiscales de la compañía'
    #
    #     if TYPE_PERSON.get(invoice.company.party.type_person):
    #         self.company_tax_level_code = TYPE_PERSON[invoice.company.party.type_person]
    #     self.company_department = invoice.company.party.department_name
    #     self.company_email = invoice.company.party.email
    #     self.fiscal_regimen_company = invoice.company.party.fiscal_regimen
    #     self.shop_location = None
    #     if hasattr(self.invoice, 'shop'):
    #         self.shop_location = self.invoice.shop.address
    #         MESSAGES['shop_location'] = 'Falta la dirección de la tienda'
    #     #Party information------------------------------------------------------------------------------------------
    #     self.department_code = invoice.party.department_code
    #     self.city_code = invoice.party.department_code+invoice.party.city_code
    #     self.party_name = invoice.party.commercial_name or  invoice.party.name
    #     self.party_postal_zone = [address.zip for address in invoice.party.addresses if address.zip] or ''
    #     self.party_id = invoice.party.id_number
    #     self.party_ciiu_code = invoice.party.ciiu_code or ''
    #     self.party_check_digit = invoice.party.check_digit
    #     self.party_type_id = invoice.party.type_document or ''
    #     self.supplier_tax_level_code = '2'
    #     self.invoice_type_name = 'Venta'
    #     self.party_registration = invoice.party.commercial_registration or ''
    #     self.brands = []
    #     self.models = []
    #     self.debit_note_concept = self.invoice.debit_note_concept
    #     self.credit_note_concept = self.invoice.credit_note_concept
    #     if invoice.invoice_type:
    #         num_add = '0'
    #         if self.invoice.invoice_type not in ['1', '2', '3', '4']:
    #             num_add = ''
    #         self.invoice_type_code = num_add + invoice.invoice_type
    #         self.invoice_type_name = invoice.invoice_type_string
    #         if self.invoice_type_code == '2':
    #             self.brands = ['not_brand' for line in self.lines if hasattr(line.product, 'brand') and not line.product.brand]
    #             self.models = ['not_reference' for line in self.lines if hasattr(line.product, 'reference') and not line.product.reference]
    #
    #     if TYPE_PERSON.get(invoice.party.type_person):
    #         self.party_tax_level_code = TYPE_PERSON[invoice.party.type_person]
    #     self.fiscal_regimen_party = invoice.party.fiscal_regimen
    #     self.party_address = invoice.party.street
    #     self.party_country = 'CO' #invoice.party.country_code
    #     self.party_department = invoice.party.department_name
    #     self.party_city = invoice.party.city_name
    #     self.party_phone = invoice.party.phone or invoice.party.mobile
    #     self.party_email = invoice.party.email
    #     self.reference = invoice.reference or ''
    #     self.concept = invoice.description or ''
    #     self.comment = invoice.comment or ''
    #     self.total_amount_words = invoice.total_amount_words or ''
    #     self.due_date = self.invoice.due_date
    #     self.payment_term = invoice.payment_term.name if invoice.payment_term else ''
    #     self.payment_type = invoice.payment_term.payment_type if invoice.payment_term else ''
    #     self.payment_means_code = invoice.payment_code or ''
    #     self.payment_method = invoice.payment_method or invoice.payment_term.description or ''
    #     self.untaxed_amount = str(abs(invoice.untaxed_amount))
    #     self.total_amount = str(abs(invoice.total_amount))
    #     self.tax_amount = str(abs(invoice.tax_amount))
    #     self.taxes = invoice.taxes
    #     self.not_classification_tax = [taxline.tax.name for taxline in self.taxes if not taxline.tax.classification_tax]
    #     self.currency = invoice.currency.code
    #     self.lines = invoice.lines
    #     self.issue_date = str(invoice.invoice_date) if invoice.invoice_date else None
    #     self.issue_time = str(invoice.create_date.time())[:8]
    #     self.status = 'ok'
    #     if not self.payment_means_code and self.payment_type == '1':
    #         MESSAGES['payment_means_code'] = 'Falta el código identificador en la configuración del modo de pago',
    #
    #     self.software_provider_id = auth.software_provider_id
    #     self.check_digit_provider = auth.check_digit_provider
    #     self.invoice_authorization = auth.number
    #     self.start_date_auth = str(auth.start_date_auth)
    #     self.end_date_auth = str(auth.end_date_auth)
    #     self.prefix = auth.sequence.prefix
    #     self.from_auth = str(auth.from_auth)
    #     self.to_auth = str(auth.to_auth)
    #     self.auth_environment = auth.environment
    #     self.operation_type = invoice.operation_type
    #     self.validate_invoice()
    #     self.type_document_reference = invoice.type_document_reference or ''
    #     self.number_document_reference = invoice.number_document_reference or ''
    #     self.cufe_document_reference = invoice.cufe_document_reference or ''
    #     self.date_document_reference = invoice.date_document_reference or ''
    #     self.elaborated = invoice.create_uid.name
    #     self.original_invoice_date = None
    #     self.original_invoice_number = None
    #     self.original_invoice_cufe = None
    #     self.original_invoice_invoice_type = None
    #
    #     if invoice.original_invoice:
    #         self.original_invoice_date = invoice.original_invoice.invoice_date
    #         self.original_invoice_number = invoice.original_invoice.number
    #         self.original_invoice_cufe = invoice.original_invoice.cufe
    #         self.original_invoice_invoice_type = invoice.original_invoice.invoice_type
    #
    #     self._create_electronic_invoice()
    #
    # def validate_invoice(self):
    #     for k in MESSAGES.keys():
    #         field_value = getattr(self, k)
    #         if not field_value:
    #             self.status = MESSAGES[k]
    #             break
    #         if self.not_classification_tax:
    #             self.status = 'Falta Asignar Clasificación a un impuesto'+";".join(self.not_classification_tax)
    #             break
    #         if len(self.brands) > 0:
    #             self.status = 'Las marcas de productos son obligatorias para los tipo Exportación'
    #             break
    #         if len(self.models) > 0:
    #             self.status = 'Las referencias o modelos de productos son obligatorias para los tipo Exportación'
    #             break
    #
    #
    #
    # def validate_value(self, value):
    #     if value < 0:
    #         return value * -1
    #     else:
    #         return value
    #
    # def send_invoice(self, id_number, dv):
    #     api_url = 'https://tryton.apifacturacionelectronica.xyz:443/api/ubl2.1/config/'+id_number+'/'+dv
    #
    #     sequence = 0
    #     invoice_lines = []
    #     prepaid_amount, discount = self._get_total()
    #     for line in self.lines:
    #         sequence += 1
    #         invoice_lines.append(self._get_line(line, str(sequence)))
    #
    #     params = {
    #                 "number": self.number,
    #                 "type_document_id": 1,
    #                 "customer": {
    #                 "identification_number": int(self.party_id),
    #                 "name": self.party_name,
    #                 "phone": int(self.party_phone),
    #                 "address": self.party_address,
    #                 "email": self.party_email,
    #                 "merchant_registration": "No tiene"
    #                 },
    #                 "legal_monetary_totals": {
    #                   "line_extension_amount": self.untaxed_amount,
    #                   "tax_exclusive_amount": self.tax_amount,
    #                   "tax_inclusive_amount": self.total_amount,
    #                   "allowance_total_amount": discount,
    #                   "charge_total_amount": "0.00",
    #                   "payable_amount": self.total_amount,
    #                 },
    #                 "invoice_lines": invoice_lines,
    #             }
    #     request = json.dumps(params)
    #
    #     response = requests.post(api_url, headers=HEADERS, data=request)
    #
    #     # print(dir(response))
    #     # res_dic = json.loads(res)
    #     res = response.json()
    #     message = ''
    #     if response.status_code == 200:
    #         message = 'Conexión exitosa' + res['message']
    #     else:
    #         message = 'Error '+ res['message']
    #
    #     return message
    #
    # def send_credit_note(self, id_number, dv):
    #     api_url = 'https://tryton.apifacturacionelectronica.xyz:443/api/ubl2.1/config/'+id_number+'/'+dv
    #
    #     sequence = 0
    #     invoice_lines = []
    #     prepaid_amount, discount = self._get_total()
    #     for line in self.lines:
    #         sequence += 1
    #         invoice_lines.append(self._get_line(line, str(sequence)))
    #
    #     params = {
    #               "billing_reference": {
    #                 "number": self.original_invoice_number,
    #                 "uuid": self.original_invoice_cufe,
    #                 "issue_date": str(self.original_invoice_date),
    #               },
    #               "number": self.number,
    #               "type_document_id": 5,
    #               "customer": {
    #                   "identification_number": int(self.party_id),
    #                   "name": self.party_name,
    #                   "phone": int(self.party_phone),
    #                   "address": self.party_address,
    #                   "email": self.party_email,
    #                   "merchant_registration": "No tiene"
    #               },
    #               "legal_monetary_totals": {
    #                 "line_extension_amount": self.untaxed_amount,
    #                 "tax_exclusive_amount": self.tax_amount,
    #                 "tax_inclusive_amount": self.total_amount,
    #                 "allowance_total_amount": discount,
    #                 "charge_total_amount": "0.00",
    #                 "payable_amount": self.total_amount,
    #               },
    #               "credit_note_lines": invoice_lines,
    #             }
    #
    #     request_json = json.dumps(params)
    #     response = requests.post(api_url, headers=HEADERS, data=request_json)
    #
    #     res = response.json()
    #     message = ''
    #     if response.status_code == 200:
    #         message = 'Conexión exitosa' + res['message']
    #     else:
    #         message = 'Error '+ res['message']
    #
    #     return message
    #
    # def send_debit_note(self, id_number, dv):
    #     api_url = 'https://tryton.apifacturacionelectronica.xyz:443/api/ubl2.1/config/'+id_number+'/'+dv
    #
    #     sequence = 0
    #     invoice_lines = []
    #     prepaid_amount, discount = self._get_total()
    #     for line in self.lines:
    #         sequence += 1
    #         invoice_lines.append(self._get_line(line, str(sequence)))
    #
    #     params = {
    #               "billing_reference": {
    #                 "number": self.original_invoice_number,
    #                 "uuid": self.original_invoice_cufe,
    #                 "issue_date": str(self.original_invoice_date),
    #               },
    #               "number": self.number,
    #               "type_document_id": 5,
    #               "customer": {
    #                   "identification_number": int(self.party_id),
    #                   "name": self.party_name,
    #                   "phone": int(self.party_phone),
    #                   "address": self.party_address,
    #                   "email": self.party_email,
    #                   "merchant_registration": "No tiene"
    #               },
    #               "requested_monetary_totals": {
    #                 "line_extension_amount": self.untaxed_amount,
    #                 "tax_exclusive_amount": self.tax_amount,
    #                 "tax_inclusive_amount": self.total_amount,
    #                 "allowance_total_amount": discount,
    #                 "charge_total_amount": "0.00",
    #                 "payable_amount": self.total_amount,
    #               },
    #               "debit_note_lines": invoice_lines,
    #             }
    #
    #     request_json = json.dumps(params)
    #     response = requests.post(api_url, headers=HEADERS, data=request_json)
    #
    #     res = response.json()
    #     message = ''
    #     if response.status_code == 200:
    #         message = 'Conexión exitosa' + res['message']
    #     else:
    #         message = 'Error '+ res['message']
    #
    #     return message
    #
    #
    # def _get_line(self, line, sequence):
    #
    #     line_ = [
    #     {
    #         "unit_measure_id": 642,
    #         "invoiced_quantity": str(self.validate_value(round(line.quantity, 1))),
    #         "line_extension_amount": str(self.validate_value(line.amount)),
    #         "free_of_charge_indicator": true,
    #         "allowance_charges": [
    #         {
    #             "charge_indicator": false,
    #             "allowance_charge_reason": "Discount",
    #             "amount": "0.00",
    #             "base_amount": "0.00"
    #             }
    #         ],
    #           "tax_totals": self._get_tax_lines(line),
    #           "description": line.description if line.description else line.product.name,
    #           "code": line.product.code,
    #           "type_item_identification_id": int(sequence),
    #           "price_amount": str(self.validate_value(line.unit_price)),
    #           "base_quantity": str(self.validate_value(round(line.quantity, 1))),
    #     }
    #     ]
    #     return line_
    #
    # def _get_tax_lines(self, line):
    #     subtotals = []
    #     line_tax_amount = '0'
    #     line_tax_base = '0'
    #     per_unit_amount = '0'
    #     for t in line.taxes:
    #         if not t.classification_tax:
    #             continue
    #         # Agregar condicional en impuesto al consumo
    #         line_tax_amount = str(round(t.rate * self.validate_value(line.amount), 2))
    #         line_tax_base = str(round(self.validate_value(line.amount), 2))
    #         percentage = (t.rate * 100)
    #         tax_id = t.classification_tax
    #
    #         subtotals.append(
    #             {
    #             "tax_id": int(tax_id),
    #             "tax_amount": line_tax_amount,
    #             "taxable_amount": line_tax_base,
    #             "percent": str(round(percentage, 2))
    #             }
    #         )
    #     return subtotals,
    #
    # def _get_total(self):
    #     discount = '0.00'
    #     _, prepaid_amount = self._get_prepaid_amount()
    #     if hasattr(self.invoice, 'sale'):
    #         if hasattr(self.invoice.sale, 'total_discount'):
    #             if self.invoice.sale.total_discount:
    #                 discount = str(round(self.validate_value(self.invoice.sale.total_discount), 2))
    #         elif hasattr(self.invoice.lines[0], 'discount'):
    #             discount = sum([(line.gross_unit_price - line.unit_price) for line in self.invoice.lines])
    #     return prepaid_amount, discount
    #
    # def _get_prepaid_amount(self):
    #     date_payment = ''
    #     amount_payment = ''
    #     _sale = self._get_sale_origin()
    #     date_payment = self.invoice.payment_lines[0].origin.date if self.invoice.payment_lines else ''
    #     amount_payment = sum([line.credit for line in self.invoice.payment_lines])
    #     if _sale and hasattr(_sale, 'payments') and _sale.payments:
    #         amount_payment = sum([payment.amount for payment in _sale.payments])
    #         date_payment = _sale.payments[0].date
    #     return date_payment, amount_payment
