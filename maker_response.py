#!/usr/bin/python
from lxml import builder, etree

sts = builder.ElementMaker(namespace='dian:gov:co:facturaelectronica:Structures-2-1')
attr_qname = etree.QName('http://www.w3.org/2001/XMLSchema-instance', 'schemaLocation')
namespaces = {
    'ext': 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2',
    'cac': 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2',
    'cbc': 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2',
    None: 'urn:oasis:names:specification:ubl:schema:xsd:ApplicationResponse-2',
    'sts': 'dian:gov:co:facturaelectronica:Structures-2-1',
    'xades': 'http://uri.etsi.org/01903/v1.3.2#',
    'ds': 'http://www.w3.org/2000/09/xmldsig#',
}
    # 'xades141': 'http://uri.etsi.org/01903/v1.4.1#',
    # 'xsi': 'http://www.w3.org/2001/XMLSchema-instance',

root = builder.ElementMaker(nsmap=namespaces)

ext = builder.ElementMaker(namespace=namespaces['ext'])
cbc = builder.ElementMaker(namespace=namespaces['cbc'])
cac = builder.ElementMaker(namespace=namespaces['cac'])
ds = builder.ElementMaker(namespace=namespaces['ds'])
xades = builder.ElementMaker(namespace=namespaces['xades'])
# xades141 = builder.ElementMaker(namespace=namespaces['xades141'])
attr_xsd = "urn:oasis:names:specification:ubl:schema:xsd:ApplicationResponse-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-ApplicationResponse-2.1.xsd"