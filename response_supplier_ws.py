#!/usr/bin/python
# -*- coding: utf-8 -*-
''' Authentication functions for Stupendo Webservices '''
import base64
import json
import requests
import xmltodict
import zipfile
from io import BytesIO
from xml.etree import cElementTree as ElementTree
from .builder_response import TYPE_EVENT
from datetime import datetime
# from reportlab.pdfgen import canvas


def get_last_number_invoice(client, id_number, invoice_type, token, point_sale=1):
    res_last_number = client.service.UltimoNumeroComprobante(
        id_number, point_sale, invoice_type, token)
    return res_last_number


def create_zip(xml_signed, file_name_xml):
    memory_zip = BytesIO()
    with zipfile.ZipFile(memory_zip, 'x', allowZip64=True) as zf:
        zf.writestr(file_name_xml,  xml_signed,
                    compress_type=zipfile.ZIP_DEFLATED)
        zf.close()
    return memory_zip.getbuffer()


def encode_invoice(raw_xml, process='encode'):
    if process == 'encode':
        file64 = base64.b64encode(raw_xml)
        file64_decod = file64.decode('utf-8')
    else:
        file64 = base64.b64decode(raw_xml)
        file64_decod = file64.decode('utf-8')

    return file64_decod


def send_request_status_psk(invoice, url_api):
    headers = {
        "Authorization": 'Bearer {}'.format(invoice.company.token_api),
        "Content-type": "application/json",
        "accept": "application/json",
    }
    cufe = invoice.cufe if invoice.cufe else invoice.get_cufe()
    url_api = url_api + cufe
    response = requests.post(url_api, headers=headers, data='')
    if response.status_code == 200:
        res = response.json()
        return res
    else:
        response = None


def send_invoice_psk(invoice, xml_invoice, api_send, url_sign, event=None):
    headers = {
        "Authorization": 'Bearer {}'.format(invoice.company.token_api),
        "Content-type": "application/json",
        "accept": "application/json",
    }
    file64_decod = encode_invoice(xml_invoice)

    params = {
        "xml_base64_bytes": file64_decod,
    }
    request = json.dumps(params)
    response = requests.post(url_sign, headers=headers, data=request)
    if response.status_code == 200:
        res = response.json()
        xml_signed = encode_invoice(res['xml_base64_bytes'], 'decode')
        if event:
            file_n = invoice.get_file_name(doc_type='ar', is_event=True) + '.xml'
            file_name = invoice.get_file_name(doc_type='z', is_event=True) + '.zip'
        else:
            file_n = invoice.get_file_name() + '.xml'
            file_name = invoice.get_file_name(doc_type='z') + '.zip'
        # invoice.create_file(xml_signed, file_n)
        zip = create_zip(xml_signed, file_n)
        zip_base64 = encode_invoice(zip)
        if not invoice.type == 'in' and invoice.authorization.environment == '2':
            api_send = api_send + '/' + invoice.authorization.test_set_id
        else:
            api_send = api_send
        if invoice.type == 'in' or (invoice.authorization and invoice.authorization.send_method_api == 'sincrono'):
            method = True
        else:
            method = False

        params = {
                  "sync": method,
                  "fileName": file_name,
                  "contentFile": zip_base64,
        }
        request = json.dumps(params)
        response = requests.post(api_send, headers=headers, data=request)
        # print(response, response.text, 'sender: ' + api_send)
        if response.status_code == 200:
            return response.json(), xml_signed
        else:
            return {'result': 'false', 'message': 'Error ' + str(response)}, xml_signed
    else:
        value = {'result': 'false',
                 'message': 'Error al firmar el documento ' + str(response.status_code), }
        return value, None


def send_invoice_est(url_api, api_key, raw_xml, id_number, filename, prefix='PRUE', phase=''):
    file64_decod = encode_invoice(raw_xml)
    response = requests.post(url_api, json={
        "nit": id_number,
        "api_key": api_key,
        "xml": file64_decod,
        "contingencia": "false",
        "prefijo": prefix,
        "adjunto": "",
    })
    # print('url: ', url_api, 'prefix:', prefix, 'nit: ', id_number, 'api_key: ', api_key, 'contingencia: false', 'adjunto: ', 'nombre_archivo: ', filename)
    # print(response)
    if response.status_code == 200:
        return json.loads(response.text)
    else:
        return {'result': 'false', 'message': 'Error de conexión ' + str(response.status_code)}


def get_status_events(invoice):
        headers = {
            "Authorization": 'Bearer {}'.format(invoice.company.token_api),
            "Content-type": "application/json",
            "accept": "application/json",
        }
        uri = f'https://presik.soenac.com/api/ubl2.1/status/events/{invoice.cufe}'
        response = requests.post(uri, headers=headers)
        if response.status_code == 200:
            res = response.json()
            xml_b64bytes = res['responseDian']['Envelope']['Body']['GetStatusEventResponse']['GetStatusEventResult']['XmlBase64Bytes']
            xml_decode = encode_invoice(xml_b64bytes, 'decode')
            xml_ = xmltodict.parse(xml_decode)
            result = {}
            for response_ in xml_['ApplicationResponse']['cac:DocumentResponse']:
                r = response_['cac:Response']
                reference = response_['cac:DocumentReference']
                event = r['cbc:ResponseCode']
                k = TYPE_EVENT[event]
                datetime_ = r['cbc:EffectiveDate'] + ' ' + r['cbc:EffectiveTime']
                datetime_ = datetime.strptime(datetime_, '%Y-%m-%d %H:%M:%S%z')
                result['response_' + k] = xml_decode.encode('utf8')
                result['date_' + k] = datetime_
                result['number_' + k] = reference['cbc:ID']
            return result


def get_email_reception(invoice):
    email = None
    headers = {
        "Authorization": 'Bearer {}'.format(invoice.company.token_api),
        "Content-type": "application/json",
        "accept": "application/json",
    }
    uri = f'https://presik.soenac.com/api/ubl2.1/mail/reception/{invoice.party.id_number}'
    response = requests.post(uri, headers=headers)
    if response.status_code == 200:
        res = response.json()
        email = res['email']
    return email