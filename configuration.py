# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import json
import requests
from trytond.model import fields, ModelView
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval, Id
from trytond.exceptions import UserError


OPERATION_TYPE = [
    ('', ''),
    ('09', 'Servicios AIU'),
    ('10', 'Estandar'),
    ('11', 'Mandatos bienes'),
]

TYPE_DOCUMENTS = [
    ('', ''),
    (1, 'Registro civil'),
    (2, 'Tarjeta de identidad'),
    (3, 'Cédula de ciudadanía'),
    (4, 'Tarjeta de extranjería'),
    (5, 'Cédula de extranjería'),
    (6, 'NIT'),
    (7, 'Pasaporte'),
    (8, 'Documento de identificación extranjero'),
    (9, 'NIT de otro país'),
    (10, 'NUIP'),
]

TYPE_ORGANIZATION = [
    ('', ''),
    (1, 'Persona Jurídica'),
    (2, 'Persona Natural'),
]

TYPE_REGIMEN = [
    ('', ''),
    (1, 'Impuesto sobre las ventas'),
    (2, 'No responsable de IVA'),
]

OPERATION_API = [
    ('', ''),
    ('create_company', 'Create Company'),
    ('update_company', 'Update Company'),
    ('update_certificate', 'Update Certificate'),
    ('update_software', 'Update Data Software'),
]

STATES = {
    'required': Eval('operation').in_(['create_company', 'update_company']),
    'invisible': Eval('operation').in_(['update_certificate', 'update_software']),
}

SOENAC_TOKEN = '3W0KPbDuNGfED4THgiLOY3b8KqhmqUPPN75dvngDNPeDz9JcKZYil19jYWzcvb1dKCtZAVRKif7QxmZq'

HEADERS = {
   "Authorization": 'Bearer {}'.format(SOENAC_TOKEN),
   "Content-type": "application/json",
   "accept": "application/json",
}

SOENAC_API = 'https://presik.soenac.com/api/ubl2.1/config/'


class AccountConfiguration(metaclass=PoolMeta):
    __name__ = 'account.configuration'
    einvoice_api_key = fields.Char('Invoice API Key')
    einvoice_api_uri = fields.Char('Electronic Invoice API URI')
    einvoice_provider_id = fields.Char('Electronic Invoice Provider ID')
    message_api = fields.Char('Message Api')
    # operation_type = fields.Selection(OPERATION_TYPE, 'Operation Type',)
    electronic_invoice_mail_template = fields.Many2One('email.template',
        'Electronic Invoice Mail Template')
    auto_send_mail = fields.Boolean('Auto-send Mail')
    certificate_sequence = fields.Many2One('ir.sequence',
        'Certificate Sequence', domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=', Id('electronic_invoice_co', 'sequence_type_digital_certificate')),
        ])
    default_account_discount = fields.MultiValue(fields.Many2One(
            'account.account', "Default Account Discount",
            domain=[
                ('party_required', '=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
                ]))
    sequence_application_response = fields.Many2One('ir.sequence.strict',
        'Sequence Application Response', domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            # ('sequence_type', '=', Id('electronic_invoice_co', 'sequence_type_zip')),
        ])
    receptionist_invoice = fields.Many2One('company.employee',
        'Receptionist Invoice')
    receptionist_good_service = fields.Many2One('company.employee',
        'Receptionist Good Service')
    event_mail_template = fields.Many2One('email.template',
        'Event Mail Template')

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in {'default_account_discount'}:
            return pool.get('account.configuration.default_account')
        return super(AccountConfiguration, cls).multivalue_model(field)


class ConfigurationDefaultAccount(metaclass=PoolMeta):

    __name__ = 'account.configuration.default_account'
    default_account_discount = fields.Many2One(
        'account.account', "Default Account Discount",
        domain=[
            ('party_required', '=', True),
            ('company', '=', Eval('company', -1)),
            ],
        depends=['company'])


class CreateCompanyElectronicInvoiceStart(ModelView):
    'Create Company Electronic Invoice Start'
    __name__ = 'account.create_company_electronic_invoice.start'
    operation = fields.Selection(OPERATION_API, 'Operation', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    type_document_identification_id = fields.Selection(TYPE_DOCUMENTS,
        'Type Document', states=STATES)
    type_organization_id = fields.Selection(TYPE_ORGANIZATION,
        'Type Organization',  states=STATES)
    type_regimen_id = fields.Selection(TYPE_REGIMEN, 'Type Regimen', states=STATES)
    type_liability_id = fields.Char('Type Liability', states=STATES)
    municipality_id = fields.Char('Munipality ID', states=STATES)
    merchant_registration = fields.Char('Merchant Registration', states=STATES)
    address = fields.Char('Address', states=STATES)
    phone = fields.Char('Phone', states=STATES)
    email = fields.Char('Email', states=STATES)
    software_id = fields.Char('Id Software', states={
        'invisible': ~Eval('operation').in_(['update_software']),
        'required': Eval('operation').in_(['update_software']),
    })
    pin_software = fields.Char('Pin Software', states={
        'invisible': Eval('operation') != 'update_software',
        'required': Eval('operation') == 'update_software',
    })
    environment = fields.Selection([
        ('', ''),
        ('1', 'Produccion'),
        ('2', 'Pruebas'),
        ], 'Environment', states={
        'invisible': Eval('operation') != 'update_software',
        'required': Eval('operation') == 'update_software',
    })
    certificate_base64 = fields.Text('Certificate (Base64)', states={
        'required': Eval('operation') == 'update_certificate',
        'invisible': Eval('operation') != 'update_certificate',
    })
    password_certificate = fields.Char('Password Certificate', states={
        'required': Eval('operation') == 'update_certificate',
        'invisible': Eval('operation') != 'update_certificate',
    })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_operation():
        return 'create_company'

    @staticmethod
    def default_software_id():
        company_id = Transaction().context.get('company')
        Company = Pool().get('company.company')
        company = Company(company_id)
        return company.software_id

    @staticmethod
    def default_password_certificate():
        company_id = Transaction().context.get('company')
        pool = Pool()
        Certificate = pool.get('account.invoice.digital_certificate')
        Date = pool.get('ir.date')
        certs = Certificate.search([
            ('company', '=', company_id),
            ('state', '=', 'active'),
            ('state', '=', 'active'),
            ('end_date', '>', Date.today()),
        ])
        if certs:
            return certs[0].password

    @staticmethod
    def default_certificate_base64():
        company_id = Transaction().context.get('company')
        pool = Pool()
        Certificate = pool.get('account.invoice.digital_certificate')
        Date = pool.get('ir.date')
        certs = Certificate.search([
            ('company', '=', company_id),
            ('state', '=', 'active'),
            ('state', '=', 'active'),
            ('end_date', '>', Date.today()),
        ])
        if certs:
            return certs[0].certificate_b64

    @staticmethod
    def default_type_document_identification_id():
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)
        if company and company.party and company.party.type_document:
            if company.party.type_document == '13':
                return 3
            else:
                return 6

    @staticmethod
    def default_type_organization_id():
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)
        if company and company.party and company.party.type_person:
            if company.party.type_person == 'persona_natural':
                return 2
            if company.party.type_person == 'persona_juridica':
                return 1

    @staticmethod
    def default_type_regimen_id():
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)
        if company and company.party.regime_tax:
            if company.party.regime_tax in [
                'regimen_simplificado', 'regimen_no_responsable']:
                return 2
            else:
                return 1

    @staticmethod
    def default_type_liability_id():
        return '29'

    @staticmethod
    def default_municipality_id():
        return '846'

    @staticmethod
    def default_phone():
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)
        if company and company.party.phone:
            return company.party.phone

    @staticmethod
    def default_merchant_registration():
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)
        if company and company.party.commercial_registration:
            return company.party.commercial_registration

    @staticmethod
    def default_address():
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)
        if company and company.party.street:
            return company.party.street

    @staticmethod
    def default_email():
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        company = Company(company_id)
        if company and company.party.email:
            return company.party.email


class CreateCompanyElectronicInvoice(Wizard):
    'Create Company Electronic Invoice'
    __name__ = 'account.create_company_electronic_invoice'
    start = StateView(
        'account.create_company_electronic_invoice.start',
        'electronic_invoice_co.create_company_electronic_invoice_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'create_', 'tryton-ok', default=True),
        ])
    create_ = StateTransition()
    done = StateView(
        'account.create_company_electronic_invoice.done',
        'electronic_invoice_co.create_company_electronic_invoice_done', [
            Button('Done', 'end', 'tryton-ok', default=True),
        ])

    @classmethod
    def __setup__(cls):
        super(CreateCompanyElectronicInvoice, cls).__setup__()

    def transition_create_(self):
        company = self.start.company
        Company = Pool().get('company.company')
        Configuration = Pool().get('account.configuration')
        config = Configuration(1)
        api_url = SOENAC_API + company.party.id_number
        phone_ = self.start.phone or 0
        params = {
            'type_document_identification_id': self.start.type_document_identification_id,
            'type_organization_id': self.start.type_organization_id,
            'type_regime_id': self.start.type_regimen_id,
            'type_liability_id': int(self.start.type_liability_id),
            'business_name': company.party.name,
            'merchant_registration': self.start.merchant_registration,
            'municipality_id': int(self.start.municipality_id),
            'address': self.start.address,
            'phone': int(phone_),
            'email': self.start.email
        }
        request_ = json.dumps(params)
        if self.start.operation == 'create_company':
            response = requests.post(api_url, headers=HEADERS, data=request_)
        elif self.start.operation == 'update_company':
            HEADERS['Authorization'] = 'Bearer {}'.format(self.start.company.token_api)
            response = requests.put(api_url, headers=HEADERS, data=request_)
        elif self.start.operation == 'update_software':
            api_url = SOENAC_API + 'software'
            HEADERS['Authorization'] = 'Bearer {}'.format(self.start.company.token_api)
            params = {
                'id': self.start.software_id,
                'pin': self.start.pin_software,
            }
            if self.start.environment == '1':
                params = {
                    'id': self.start.software_id,
                    'pin': self.start.pin_software,
                    'url': 'https://vpfe.dian.gov.co/WcfDianCustomerServices.svc?wsdl',
                }
            request_ = json.dumps(params)
            response = requests.put(api_url, headers=HEADERS, data=request_)
        elif self.start.operation == 'update_certificate':
            HEADERS['Authorization'] = 'Bearer {}'.format(self.start.company.token_api)
            api_url = SOENAC_API + 'certificate'
            params = {
                'certificate': self.start.certificate_base64,
                'password': self.start.password_certificate,
            }
            request_ = json.dumps(params)
            response = requests.put(api_url, headers=HEADERS, data=request_)

        res = response.json()
        if response.status_code == 200:
            if response.ok is True:
                string = 'Conexión Establecida'
                if 'message' in res:
                    string = string + ': ' + res['message']
                if 'token' in res:
                    Company.write([company], {'token_api': res['token']})

                Configuration.write([config], {'message_api': string})
                return 'done'
        message = res['message']
        errors = res.get('errors')
        if errors:
            message = message + '\n' + str(errors)

        raise UserError('message_api',  'Error! ' + message)


class CreateCompanyElectronicInvoiceDone(ModelView):
    'Create Company Electronic Invoice Done'
    __name__ = 'account.create_company_electronic_invoice.done'
    result = fields.Char('Result', readonly=True)

    @staticmethod
    def default_result():
        Configuration = Pool().get('account.configuration')
        config = Configuration(1)
        if config and config.message_api:
            return config.message_api
