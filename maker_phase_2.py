#!/usr/bin/python
from lxml import builder, etree

attr_qname = etree.QName('http://www.w3.org/2001/XMLSchema-instance', 'schemaLocation')
namespaces = {
    None: 'urn:oasis:names:specification:ubl:schema:xsd:Invoice-2',
    'cac': 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2',
    'cbc': 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2',
    'ext': 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2',
    'sts': 'dian:gov:co:facturaelectronica:Structures-2-1',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    'xades': 'http://uri.etsi.org/01903/v1.3.2#',
    'xades141': 'http://uri.etsi.org/01903/v1.4.1#',
    'ds': 'http://www.w3.org/2000/09/xmldsig#',
}
Root = builder.ElementMaker(nsmap=namespaces)

# Solo para Presik-Soenac
# namespaces['sts'] = 'http://www.dian.gov.co/contratos/facturaelectronica/v1/Structures'
namespaces['sts'] = 'dian:gov:co:facturaelectronica:Structures-2-1'
Root_psk = builder.ElementMaker(nsmap=namespaces)
sts_psk = builder.ElementMaker(namespace='dian:gov:co:facturaelectronica:Structures-2-1')

# ----------------------Credit Note----------------------------------------------------
namespaces = {
    None: 'urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2',
    'cac': 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2',
    'cbc': 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2',
    'ext': 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2',
    'sts': 'dian:gov:co:facturaelectronica:Structures-2-1',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    'xades': 'http://uri.etsi.org/01903/v1.3.2#',
    'xades141': 'http://uri.etsi.org/01903/v1.4.1#',
    'ds': 'http://www.w3.org/2000/09/xmldsig#',
}
Root_cn = builder.ElementMaker(nsmap=namespaces)
# Solo para Presik-Soenac
namespaces['sts'] = 'dian:gov:co:facturaelectronica:Structures-2-1'
Root_psk_cn = builder.ElementMaker(nsmap=namespaces)

#----------------------Debit Note----------------------------------------------------
namespaces = {
    None: 'urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2',
    'cac': 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2',
    'cbc': 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2',
    'ext': 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2',
    'sts': 'dian:gov:co:facturaelectronica:Structures-2-1',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    'xades': 'http://uri.etsi.org/01903/v1.3.2#',
    'xades141': 'http://uri.etsi.org/01903/v1.4.1#',
    'ds': 'http://www.w3.org/2000/09/xmldsig#',
}
Root_dn = builder.ElementMaker(nsmap=namespaces)

ext = builder.ElementMaker(namespace=namespaces['ext'])
sts = builder.ElementMaker(namespace=namespaces['sts'])
cbc = builder.ElementMaker(namespace=namespaces['cbc'])
cac = builder.ElementMaker(namespace=namespaces['cac'])
ds = builder.ElementMaker(namespace=namespaces['ds'])
xades = builder.ElementMaker(namespace=namespaces['xades'])
xades141 = builder.ElementMaker(namespace=namespaces['xades141'])
xsi = builder.ElementMaker(namespace=namespaces['xsi'])

# Solo para Presik-Soenac
namespaces['sts'] = 'dian:gov:co:facturaelectronica:Structures-2-1'
Root_psk_dn = builder.ElementMaker(nsmap=namespaces)

attr_xsd = "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-Invoice-2.1.xsd"
attr_xsd_cn = "urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-CreditNote-2.1.xsd"
attr_xsd_dn = "urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-DebitNote-2.1.xsd"
