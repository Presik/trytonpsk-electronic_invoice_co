#! -*- coding: utf8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from .builder_phase_2 import ElectronicInvoice_2
from .response_supplier_ws import send_invoice_est


API_DEMO_STUPENDO = "http://demo.stupendo.co/api/documento"
API_DEMO_STUPENDO_PHASE_2 = 'https://pruebas.estupendo.com.co/api/documento'
API_STUPENDO_PHASE_2 = 'https://app.estupendo.com.co/api/documento'
API_STUPENDO = 'https://app.stupendo.co/api/documento'


class EstInvoice(object):

    def __init__(self, invoice):
        self.invoice = invoice
        self.validate_phase(invoice.authorization.phase)

    def validate_phase(self, phase):
        if phase == '2':
            self._create_electronic_invoice_phase_2()
        else:
            self.invoice.get_message('No Existe Fase para la Autorización Electrónica')

    def _create_electronic_invoice_phase_2(self):
        ec_invoice = ElectronicInvoice_2(self.invoice, self.invoice.authorization)
        _type = ''
        if ec_invoice.status != 'ok':
            self.invoice.get_message(ec_invoice.status)
        if self.invoice.credit_note_concept and self.invoice.invoice_type == '91':
            _type = 'CN'
            xml_invoice = ec_invoice.make('CN')
        elif self.invoice.debit_note_concept and self.invoice.invoice_type == '92':
            _type = 'DN'
            xml_invoice = ec_invoice.make('DN')
        else:
            _type = 'F'
            xml_invoice = ec_invoice.make('F')

        self.invoice.test_print(xml_invoice, _type)

        prefix = self.invoice.authorization.sequence.prefix

        filename = self.invoice.number + str(self.invoice.invoice_date) + '.xml'
        id_number = self.invoice.company.party.id_number

        day_ = str(self.invoice.invoice_date.day)
        month_ = str(self.invoice.invoice_date.month)
        year_ = str(self.invoice.invoice_date.year)
        if self.invoice.authorization:
            if self.invoice.authorization.environment == '2' or self.invoice.authorization.environment == 'demo':
                url_api = API_DEMO_STUPENDO_PHASE_2
                # api = "https://pruebas.estupendo.com.co/documento/pdf/"
                api = "https://stpd-dev-s3-pdf.s3.us-east-1.amazonaws.com/" + self.invoice.party.id_number \
                        + '/' + day_ + '/' + month_ + '/' + day_ + '/01/FCV-' + self.invoice.number \
                            + '-' + day_ + '_' + month_+ '_' + year_ +'.pdf'
            else:
                # api = "http://app.estupendo.co/documento/pdf/"
                api = "https://stpd-prd-s3-pdf.s3.us-east-1.amazonaws.com/" + self.invoice.party.id_number \
                + '/' + day_ + '/' + month_ + '/' + day_ + '/01/FCV-' + self.invoice.number \
                + '-' + day_ + '_' + month_+ '_' + year_ +'.pdf'
                url_api = API_STUPENDO_PHASE_2

        self._send_electronic_invoice(url_api, api, xml_invoice, id_number, prefix, '2', filename)

    def _send_electronic_invoice(self, url_api, api, xml_invoice, id_number, prefix, phase, filename):
        try:
            res = send_invoice_est(
                url_api,
                self.invoice.authorization.api_key,
                xml_invoice,
                id_number,
                filename,
                prefix,
                phase,
            )
            if not res:
                return
            if res['result'] == True:
                state_response = 'submitted'
                if res['message'] == 'Documento Autorizado':
                    state_response = 'authorized'
                # provider_link = api + res['doc_id']
                self.invoice.write([self.invoice], {
                    'cufe': res['cufe'],
                    # 'provider_link': provider_link,
                    'electronic_state': state_response,
                    'electronic_message': res['message'],
                })
            else:
                self.invoice.get_message(res)
        except Exception as e:
            if res['message']:
                self.invoice.get_message(res['message'])
            else:
                self.invoice.get_message('error desconocido')
