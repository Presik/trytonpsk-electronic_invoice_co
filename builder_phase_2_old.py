from functools import partial
from decimal import Decimal
from datetime import timedelta
from lxml import etree, builder

from .maker_phase_2 import (Root, ext, sts, cbc, cac, attr_qname, attr_xsd, sts_psk, Root_psk_dn,
                            attr_xsd_cn, Root_cn, Root_dn, attr_xsd_dn, ds, Root_psk, Root_psk_cn)
from .maker_customer import (AttachedDocument, ext_customer, cbc_customer, cac_customer, ds_customer, xades_customer, xades141_customer)


CLASIFICATION_TAX = {
    '01': 'IVA',
    '02': 'IC',
    '03': 'ICA',
    '04': 'INC',
    '05': 'ReteIVA',
    '06': 'ReteFuente',
    '07': 'ReteICA',
    '20': 'FtoHorticultura',
    '21': 'Timbre',
    '22': 'Bolsas',
    '23': 'INCarbono',
    '24': 'INCombustibles',
    '25': 'Sobretasa Combustibles',
    '26': 'Sordicom',
    'ZZ': 'Otro',
    'NA': 'No Aceptada',
    'renta': 'renta',
    'autorenta': 'autorenta',
    }

TAXES_CODE_VALID = [key for key in CLASIFICATION_TAX.keys() if key.isdigit()]

TYPE_PERSON = {
    'persona_juridica': '1',
    'persona_natural': '2',
}

FISCAL_REGIMEN = {
    '48': 'RESPONSABLE DE IMPUESTO SOBRE LAS VENTAS – IVA',
    '49': 'NO RESPONSABLE DE IVA',
}

ENVIRONMENT = {
    '1': 'Produccion',
    '2': 'Pruebas',
}

INVOICE_CODES = {
    1: 'venta',
}

MESSAGES = {
    'software_id': 'Falta ID del software Facturador',
    'company_id': 'Falta numero NIT de la empresa',
    'company_registration': 'Falta la matricula mercantil de la empresa',
    'company_name': 'Falta el nombre de la empresa',
    'company_email': 'Falta el email de la empresa',
    'company_phone': 'Falta el teléfono o celular de la empresa',
    'company_city': 'Falta la ciudad de la empresa',
    'company_city_code': 'Falta la ciudad de la empresa',
    'company_address': 'Falta la direccion de la empresa',
    'company_country_code': 'Falta el pais de la empresa',
    'company_department': 'Falta el departamento de la empresa',
    'company_department_code': 'Falta el departamento de la empresa',
    'company_ciiu_code': 'Falta el codigo CIIU en el tercero de la empresa',
    'company_postal_zone': 'Falta el codigo Postal en el tercero de la empresa',
    'company_type_id': 'Falta el tipo de documento que identifica a la compañía',
    'fiscal_regimen_company': 'Falta el Regimen Fiscal del tercero de la compañia',
    'company_tax_level_code': 'Falta el Regimen de Impuestos del tercero de la compañia',
    'currency': 'Falta el codigo de la moneda',
    'party_name': 'Falta el nombre del cliente',
    'party_id': 'Falta el id del cliente',
    'party_address': 'Falta la direccion del cliente',
    'party_country_code': 'Falta el pais del cliente',
    'party_department_code': 'Falta el departamento del cliente',
    'party_city_code': 'Falta la ciudad del cliente',
    'issue_date': 'Falta la fecha de factura',
    'party_country_name': 'Falta el pais del cliente',
    'party_department': 'Falta el departmento del cliente',
    'party_city': 'Falta la ciudad del cliente',
    'party_phone': 'Falta el telefono del cliente',
    'party_email': 'Falta el correo del cliente',
    'party_type_id': 'Falta el tipo de documento que identifica al cliente',
    'party_tax_level_code': 'Falta definir el tipo de persona juridica / natural del cliente',
    'payment_term': 'Falta el metodo de pago',
    'fiscal_regimen_party': 'Falta el Regimen Fiscal del cliente',
    'invoice_authorization': 'El campo Autorización de factura esta vacio',
    'operation_type': 'El campo Tipo de Operación de factura esta vacio',
    'invoice_type_code': 'El campo Tipo de Factura de factura esta vacio',
    'payment_type': 'Falta el tipo de concepto en el plazo de pago de la factura',
    'company_tributes': 'Falta definir el grupo de tributos de impuesto al que es responsable la compañía',
}


def rvalue(value):
    # value = str(abs(value))
    # return value[:value.find('.') + 3]
    return str(round(abs(value), 2))


def tax_valid(tax):
    rate = hasattr(tax, 'rate')
    fixed = hasattr(tax, 'fixed')
    code = tax.classification_tax
    if code in TAXES_CODE_VALID and (rate and rate > 0 or fixed and fixed > 0):
        return True
    return False

def tax_valid_witholding(tax):
    rate = hasattr(tax, 'rate')
    fixed = hasattr(tax, 'fixed')
    code = tax.classification_tax
    if code and code != 'NA' and (rate and rate < 0 or fixed and fixed < 0):
        return True
    return False


class ElectronicInvoice_2(object):

    def __init__(self, invoice, auth):
        self.type = invoice.type
        self.invoice = invoice
        self.auth = auth
        self.number = invoice.number if invoice.type == 'out' else invoice.number_alternate
        # Company information --------------------------------------------------
        self.company_id = invoice.company.party.id_number
        self.company_type_id = invoice.company.party.type_document or ''
        self.company_registration = invoice.company.party.commercial_registration
        self.company_name = invoice.company.party.name
        self.company_phone = invoice.company.party.phone or invoice.company.party.mobile
        self.company_city = invoice.company.party.city_name
        self.company_city_code = invoice.company.party.department_code+invoice.company.party.city_code
        self.company_department_code = invoice.company.party.department_code
        self.company_currency_id = invoice.company.currency.id
        self.company_address = invoice.company.party.street.replace('\n', '')
        self.company_check_digit = invoice.company.party.check_digit
        self.company_country_code = 'CO'
        self.company_country_name = 'Colombia'
        self.company_postal_zone = [address.postal_code for address in invoice.company.party.addresses]
        self.company_ciiu_code = invoice.company.party.ciiu_code
        self.company_tributes = invoice.company.party.party_tributes or []
        self.company_party_obligations = self.invoice.company.party.party_obligation_tax
        if not self.company_party_obligations:
            MESSAGES['company_party_obligations'] = 'Falta definir la obligaciones fiscales de la compañía'

        if TYPE_PERSON.get(invoice.company.party.type_person):
            self.company_tax_level_code = TYPE_PERSON[invoice.company.party.type_person]
        self.company_department = invoice.company.party.department_name
        self.company_email = invoice.company.party.email
        self.fiscal_regimen_company = invoice.company.party.fiscal_regimen
        self.shop_address = None
        if hasattr(self.invoice, 'shop') and self.invoice.shop:
            self.shop_address = self.invoice.shop.address or self.invoice.company.party.street
            if not self.shop_address:
                MESSAGES['shop_address'] = 'Falta la dirección de la tienda'

        # Party information------------------------------------------------------------------------------------------
        self.party_department_code = invoice.party.department_code or ''
        self.party_city_code = invoice.party.city_code or ''
        self.party_city_code = self.party_department_code+self.party_city_code
        self.party_name = invoice.party.name or invoice.party.commercial_name
        self.party_postal_zone = [address.postal_code for address in invoice.party.addresses if address.postal_code]
        self.party_id = invoice.party.id_number
        self.party_ciiu_code = invoice.party.ciiu_code or ''
        self.party_check_digit = invoice.party.check_digit
        self.party_type_id = invoice.party.type_document or ''
        self.supplier_tax_level_code = '2'
        self.party_tributes = invoice.party.party_tributes or []
        self.invoice_type_name = 'Venta'
        self.party_registration = invoice.party.commercial_registration or '00000'
        self.brands = []
        self.models = []
        self.debit_note_concept = self.invoice.debit_note_concept
        self.credit_note_concept = self.invoice.credit_note_concept
        self.lines = invoice.lines
        self.invoice_type_code = invoice.invoice_type if invoice.invoice_type else ''
        if self.invoice_type_code in ['1', '2', '3', '4']:
            self.invoice_type_code = '0' + invoice.invoice_type
        if self.invoice_type_code == '02':
            self.brands = ['not_brand' for line in self.lines
                           if hasattr(line.product.template, 'brand') and not line.product.brand]
            self.models = ['not_reference' for line in self.lines
                           if hasattr(line.product.template, 'reference') and not line.product.reference]

        self.invoice_type_name = invoice.invoice_type_string
        # if TYPE_PERSON.get(invoice.party.type_person):
        self.party_tax_level_code = TYPE_PERSON[invoice.party.type_person] if invoice.party.type_person else None
        self.fiscal_regimen_party = invoice.party.fiscal_regimen
        self.party_address = invoice.party.street
        self.party_country_code = invoice.party.get_country_iso(invoice.party.country_code, 'code')  # 'CO'
        self.party_country_name = invoice.party.get_country_iso(invoice.party.country_code, 'name')  # 'CO'
        self.party_department = invoice.party.department_name if self.party_country_code == 'CO' else ''
        self.party_city = invoice.party.city_name if self.party_country_code == 'CO' else ''
        self.party_phone = invoice.party.phone or invoice.party.mobile
        self.party_email = invoice.party.email
        self.reference = invoice.reference or ''
        self.notes = invoice.comment or ' '
        self.concept = (invoice.description or '') + ' ' + (self.notes or '')
        self.comment = invoice.comment or ''
        self.total_amount_words = invoice.total_amount_words or ''
        self.due_date = str(self.invoice.due_date)
        self.payment_term = invoice.payment_term.name if invoice.payment_term else ''
        self.payment_type = invoice.payment_term.payment_type if invoice.payment_term else ''
        self.payment_means_code = invoice.payment_code or ''
        self.payment_method = invoice.payment_method or invoice.payment_term.description or ''
        self.untaxed_amount = str(abs(invoice.untaxed_amount))
        self.total_amount = str(abs(invoice.total_amount))
        self.tax_amount = str(abs(invoice.tax_amount))
        self.taxes = invoice.taxes
        self.not_classification_tax = [taxline.tax.name for taxline in self.taxes if not taxline.tax.classification_tax]
        self.currency = invoice.currency.code
        # self.issue_time = str(self.invoice.company.convert_timezone(invoice.create_date).time())[:8]
        self.issue_date, self.issue_time = invoice.get_datetime_local()
        self.status = 'ok'
        if self.payment_type == '2' or self.payment_means_code == '':
            self.payment_means_code = '1'
        self.software_id = auth.software_id
        self.software_provider_id = auth.software_provider_id
        self.check_digit_provider = auth.check_digit_provider
        self.invoice_authorization = auth.number
        self.start_date_auth = str(auth.start_date_auth)
        self.end_date_auth = str(auth.end_date_auth)
        self.prefix = auth.sequence.prefix
        self.from_auth = str(auth.from_auth)
        self.to_auth = str(auth.to_auth)
        self.auth_environment = auth.environment
        self.operation_type = invoice.operation_type
        self.validate_invoice()
        self.type_document_reference = invoice.type_document_reference or ''
        self.number_document_reference = invoice.number_document_reference or ''
        self.cufe_document_reference = invoice.cufe_document_reference or ''
        self.date_document_reference = invoice.date_document_reference or ''
        self.elaborated = invoice.create_uid.name
        self.original_invoice_date = None
        self.original_invoice_number = None
        self.original_invoice_cufe = None
        self.original_invoice_invoice_type = None

        if invoice.operation_type in ('20', '30'):
            self.original_invoice_date = invoice.date_document_reference
            self.original_invoice_number = invoice.number_document_reference
            self.original_invoice_cufe = invoice.cufe_document_reference
            self.original_invoice_invoice_type = invoice.type_invoice_reference

    def validate_invoice(self):
        for k in MESSAGES.keys():

            field_value = getattr(self, k)
            party_inf = ['party_department_code', 'party_city_code', 'party_department', 'party_city']
            if k in party_inf and getattr(self, 'party_country_code') != 'CO':
                continue
            elif not field_value:
                self.status = MESSAGES[k]
                break
            if self.not_classification_tax:
                self.status = 'Falta Asignar Clasificación a un impuesto'+";".join(self.not_classification_tax)
                break
            if len(self.brands) > 0:
                self.status = 'Las marcas de productos son obligatorias para tipo Exportación'
                break
            if len(self.models) > 0:
                self.status = 'Las referencias o modelos de productos son obligatorias para los tipo Exportación'
                break
            # for line in self.lines:
            #     if not line.product.code:
            #         self.status = f'El producto {line.product.name}, debe tener codigo'
            #         break

    def validate_value(self, value):
        if value < 0:
            return value * -1
        else:
            return value

    def _get_software_info(self):
        software_id_ = sts.SoftwareID()
        if self.software_id:
            software_id_ = sts.SoftwareID(self.software_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)")
        return software_id_

    def _get_head(self):
        if self.invoice.authorization.provider == 'psk':
            head = self._get_head_psk()
        else:
            head = self._get_head_est()

        return head

    def get_profile_head(self):
        profile_id = ''
        head = None
        invoice_type = ''
        scheme_name = ''
        if self.invoice.invoice_type in ('1', '2', '3', '4'):
            profile_id = 'DIAN 2.1: Factura Electrónica de Venta'
            head = Root_psk.Invoice()
            invoice_type = cbc.InvoiceTypeCode(self.invoice_type_code)
            scheme_name = 'CUFE-SHA384'
        elif self.invoice.invoice_type == '91':
            profile_id = 'DIAN 2.1: Nota Crédito de Factura Electrónica de Venta'
            head = Root_psk_cn.CreditNote()
            invoice_type = cbc.CreditNoteTypeCode(self.invoice_type_code)
            scheme_name = 'CUDE-SHA384'
        elif self.invoice.invoice_type == '92':
            profile_id = 'DIAN 2.1: Nota Débito de Factura Electrónica de Venta'
            head = Root_psk_dn.DebitNote()
            invoice_type = cbc.DebitNoteTypeCode(self.invoice_type_code)
            scheme_name = 'CUDE-SHA384'
        elif self.invoice.invoice_type == '05':
            profile_id = 'DIAN 2.1: documento soporte en adquisiciones efectuadas a no obligados a facturar.'
            head = Root_psk.Invoice()
            invoice_type = cbc.InvoiceTypeCode(self.invoice_type_code)
            scheme_name = 'CUDS-SHA384'
        elif self.invoice.invoice_type == '95':
            profile_id = 'DIAN 2.1: Nota Débito de Factura Electrónica de Venta'
            head = Root_psk_cn.CreditNote()
            invoice_type = cbc.CreditNoteTypeCode(self.invoice_type_code)
            scheme_name = 'CUDS-SHA384'
        return profile_id, head, invoice_type, scheme_name

    def _get_head_(self):
        software_id_ = self._get_software_info()
        profile_id, head, invoice_type, scheme_name = self.get_profile_head()

        elements = (ext.UBLExtensions(
                ext.UBLExtension(
                    ext.ExtensionContent(
                        sts_psk.DianExtensions(
                            sts_psk.InvoiceControl(
                                sts_psk.InvoiceAuthorization(self.invoice_authorization),
                                sts_psk.AuthorizationPeriod(
                                    cbc.StartDate(self.start_date_auth),
                                    cbc.EndDate(self.end_date_auth)
                                ),
                                sts_psk.AuthorizedInvoices(
                                    sts_psk.Prefix(self.prefix),
                                    sts_psk.From(self.from_auth),
                                    sts_psk.To(self.to_auth),
                                ),
                            ),
                            sts_psk.InvoiceSource(
                                cbc.IdentificationCode('CO', listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listSchemeURI="urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1")
                            ),
                            sts_psk.SoftwareProvider(
                                sts_psk.ProviderID(self.software_provider_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID=str(self.check_digit_provider), schemeName="31"),
                                sts_psk.SoftwareID(self.software_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            ),
                            sts_psk.SoftwareSecurityCode(self.invoice.get_security_code(), schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            sts_psk.AuthorizationProvider(
                                sts_psk.AuthorizationProviderID('800197268',
                                    schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID="4", schemeName="31"),
                            ),
                            sts_psk.QRCode(self.invoice.get_link_dian()),
                        ),
                    ),
                ),
                ext.UBLExtension(
                    ext.ExtensionContent(),
                ),
            ),
            cbc.UBLVersionID('UBL 2.1'),
            cbc.CustomizationID(self.operation_type),
            cbc.ProfileID(profile_id),
            cbc.ProfileExecutionID(self.auth_environment),
            cbc.ID(self.number),
            cbc.UUID(self.invoice.get_cufe(), schemeID=self.auth_environment, schemeName=scheme_name if self.type == 'out' else 'CUDS-SHA384'),
            cbc.IssueDate(self.issue_date),
            cbc.IssueTime(self.issue_time),
            # cbc.DueDate(self.due_date),
            invoice_type,
            cbc.Note(self.comment or ''),
            cbc.DocumentCurrencyCode(self.currency, listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listID="ISO 4217 Alpha"),
            cbc.LineCountNumeric(str(len(self.lines))))

        for e in elements:
            head.append(e)
        return head

    def _get_head_psk(self):
        software_id_ = self._get_software_info()

        head = Root_psk.Invoice(
            ext.UBLExtensions(
                ext.UBLExtension(
                    ext.ExtensionContent(
                        sts_psk.DianExtensions(
                            sts_psk.InvoiceControl(
                                sts_psk.InvoiceAuthorization(self.invoice_authorization),
                                sts_psk.AuthorizationPeriod(
                                    cbc.StartDate(self.start_date_auth),
                                    cbc.EndDate(self.end_date_auth)
                                ),
                                sts_psk.AuthorizedInvoices(
                                    sts_psk.Prefix(self.prefix),
                                    sts_psk.From(self.from_auth),
                                    sts_psk.To(self.to_auth),
                                ),
                            ),
                            sts_psk.InvoiceSource(
                                cbc.IdentificationCode('CO', listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listSchemeURI="urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1")
                            ),
                            sts_psk.SoftwareProvider(
                                sts_psk.ProviderID(self.software_provider_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID=str(self.check_digit_provider), schemeName="31"),
                                sts_psk.SoftwareID(self.software_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            ),
                            sts_psk.SoftwareSecurityCode(self.invoice.get_security_code(), schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            sts_psk.AuthorizationProvider(
                                sts_psk.AuthorizationProviderID('800197268',
                                    schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID="4", schemeName="31"),
                            ),
                            sts_psk.QRCode(self.invoice.get_link_dian()),
                        ),
                    ),
                ),
                ext.UBLExtension(
                    ext.ExtensionContent(),
                ),
            ),
            cbc.UBLVersionID('UBL 2.1'),
            cbc.CustomizationID(self.operation_type),
            cbc.ProfileID('DIAN 2.1: Factura Electrónica de Venta'),
            cbc.ProfileExecutionID(self.auth_environment),
            cbc.ID(self.number),
            cbc.UUID(self.invoice.get_cufe(), schemeID=self.auth_environment, schemeName="CUFE-SHA384" if self.type == 'out' else 'CUDS-SHA384'),
            cbc.IssueDate(self.issue_date),
            cbc.IssueTime(self.issue_time),
            # cbc.DueDate(self.due_date),
            cbc.InvoiceTypeCode(self.invoice_type_code),
            cbc.Note(self.comment or ''),
            # cbc.DocumentCurrencyCode('COP'),
            cbc.DocumentCurrencyCode(self.currency, listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listID="ISO 4217 Alpha"),
            cbc.LineCountNumeric(str(len(self.lines))),
        )
        return head

    def _get_head_est(self):
        software_id_ = self._get_software_info()
        note_start = ''
        note_end = ''
        i = 0
        for taxline in self.taxes:
            if taxline.tax.classification_tax == 'renta':
                note_start += 'RENTA=' + taxline.description + '-' + str(round(abs(taxline.base), 2)) + '-' + str(round(abs(taxline.amount), 2)) + '; '
            if taxline.tax.classification_tax == 'autorenta':
                note_start += 'AUTORENTA=' + taxline.description + '-' + str(round(abs(taxline.base), 2)) + '-' + str(round(abs(taxline.amount), 2)) + '; '
            if taxline.tax.classification_tax == 'NA':
                i += 1
                note_end += 'RETENCION' + str(i) + '=' + taxline.description + '-' + str(round(abs(taxline.base), 2)) + '-' + str(round(abs(taxline.amount), 2)) + '; '

        note_tax = note_start + note_end
        note_tax = note_tax[:-2]
        note_ = 'REFERENCIA=' + self.invoice.reference + '; CONCEPTO=' + self.concept + '; MEDIOS=' + self.payment_method + '; ' + note_tax + ' '

        head = Root.Invoice(
            ext.UBLExtensions(
                ext.UBLExtension(
                    ext.ExtensionContent(
                        sts.DianExtensions(
                            sts.InvoiceControl(
                                sts.InvoiceAuthorization(self.invoice_authorization),
                                sts.AuthorizationPeriod(
                                    cbc.StartDate(self.start_date_auth),
                                    cbc.EndDate(self.end_date_auth)
                                ),
                                sts.AuthorizedInvoices(
                                    sts.Prefix(self.prefix),
                                    sts.From(self.from_auth),
                                    sts.To(self.to_auth),
                                ),
                            ),
                            sts.InvoiceSource(
                                cbc.IdentificationCode('CO', listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listSchemeURI="urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1")
                            ),
                            sts.SoftwareProvider(
                                sts.ProviderID(self.software_provider_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID=str(self.check_digit_provider), schemeName="31"),
                                software_id_,
                            ),
                            sts.SoftwareSecurityCode("", schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            sts.AuthorizationProvider(
                                sts.AuthorizationProviderID('800197268',
                                    schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID="4", schemeName="31"),
                            ),
                            sts.QRCode(''),
                        ),
                    ),
                ),
            ),
            cbc.UBLVersionID('UBL 2.1'),
            cbc.CustomizationID(self.operation_type),
            cbc.ProfileID('DIAN 2.1: Factura Electrónica de Venta'),
            cbc.ProfileExecutionID(self.auth_environment),
            cbc.ID(self.number),
            cbc.UUID('', schemeID=self.auth_environment, schemeName="CUFE-SHA384"),
            cbc.IssueDate(self.issue_date),
            cbc.IssueTime(self.issue_time),
            # cbc.DueDate(self.due_date),
            cbc.InvoiceTypeCode(self.invoice_type_code),
            cbc.Note(note_ or ''),
            cbc.DocumentCurrencyCode(self.currency, listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listID="ISO 4217 Alpha"),
            cbc.LineCountNumeric(str(len(self.lines))),
        )
        return head

    def _get_invoice_period(self):
        invoice_period = cac.InvoicePeriod(
            cbc.StartDate(''),
            cbc.StartTime(''),
            cbc.EndDate(''),
            cbc.EndTime(''),
        )
        return invoice_period

    def _get_order_reference(self):
        reference = None
        if self.reference:
            order_reference = self.invoice.reference
            isue_date = str(self.invoice.invoice_date)

            reference = cac.OrderReference(
                cbc.ID(order_reference),
                cbc.IssueDate(isue_date),
            )
        return reference

    def _get_billing_reference(self):
        billing_reference = None
        document_reference = None
        if self.original_invoice_invoice_type:
            billing_reference = cac.BillingReference()

            tuple_elements = (
                cbc.ID(self.original_invoice_number),
                cbc.UUID(self.original_invoice_cufe,  schemeName="CUDE-SHA384"),
                cbc.IssueDate(str(self.original_invoice_date))
            )
            if self.invoice.invoice_type == '91' and self.operation_type == '20':
                document_reference = cac.InvoiceDocumentReference(*tuple_elements)

            elif self.invoice.invoice_type == '92' and self.operation_type == '30':
                document_reference = cac.DebitNoteDocumentReference(*tuple_elements)

            elif self.original_invoice_invoice_type in ['1', '2']:
                document_reference = cac.InvoiceDocumentReference(*tuple_elements)
        if document_reference is not None and billing_reference is not None:
            billing_reference.append(document_reference)
        return billing_reference

    def _get_despatch_document_reference(self):
        despatch_document_reference = cac.DespatchDocumentReference(
            cbc.ID(''),
            cbc.IssueDate('0001-01-01'),
        )
        return despatch_document_reference

    def _get_receipt_document_reference(self):
        receipt_document_reference = cac.ReceiptDocumentReference(
            cbc.ID(''),
            cbc.IssueDate(''),
        )
        return receipt_document_reference

    def _get_additional_document_reference(self):
        additional_document_reference = cac.AdditionalDocumentReference(
            cbc.ID(self.number_document_reference),
            cbc.UUID(self.cufe_document_reference,  schemeName="CUDE-SHA384", issueDate=self.date_document_reference),
            cbc.DocumentTypeCode(self.type_document_reference),
        )
        return additional_document_reference

    def _get_supplier_party(self):
        if self.auth.provider == 'estp':
            addictional_account_id = cbc.AdditionalAccountID(self.company_tax_level_code, schemeAgencyID="195")
        else:
            addictional_account_id = cbc.AdditionalAccountID(self.company_tax_level_code)

        supplier_party = cac.AccountingSupplierParty(
            addictional_account_id,
            cac.Party(
                cbc.IndustryClassificationCode(self.company_ciiu_code),
                cac.PartyName(
                    cbc.Name(self.company_name),
                ),
                self._get_physical_location(),
                self._get_party_tax_scheme(),
                cac.PartyLegalEntity(
                    cbc.RegistrationName(self.company_name),
                    cbc.CompanyID(self.company_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID=str(self.invoice.get_check_digit(self.company_id)), schemeName=self.company_type_id),
                    cac.CorporateRegistrationScheme(
                        cbc.ID(self.prefix),
                        cbc.Name(self.company_registration),
                    ),
                ),
                cac.Contact(
                    cbc.Name(self.company_name),
                    cbc.Telephone(self.company_phone),
                    cbc.Telefax(''),
                    cbc.ElectronicMail(self.company_email),
                    cbc.Note(''),
                ),
            ),

        )
        return supplier_party
        
    def _get_party(self, context):
        if (context == 'party' and self.type == 'in') or (context == 'company' and self.type == 'out'):
            party_ = cac.AccountingSupplierParty()
        else:
            party_ = cac.AccountingCustomerParty()
        
        party_.append(cbc.AdditionalAccountID(getattr(self, context+'_tax_level_code'), ))
        try:
           dv = str(self.invoice.get_check_digit(getattr(self, context+'_id')))
        except:
            dv = ''
        elements = [] 
        elements_append = elements.append
        if self.type == 'out' and context =='party':
            elements_append(
                cac.PartyIdentification(
                    cbc.ID(
                        self.party_id,
                        schemeAgencyID="195",
                        schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
                        schemeID=dv,
                        schemeName=self.party_type_id
                    ),
                )
            )
        if self.type == 'out' and context =='company':
            elements_append(cbc.IndustryClassificationCode(self.company_ciiu_code))
        
        if self.type == 'out':       
            elements_append(cac.PartyName(
                    cbc.Name(getattr(self, context+'_name')),
                )),

        elements_append(self._get_physical_location_(context)),
        elements_append(self._get_party_tax_scheme_(context)),
        if self.type == 'out':
            elements_append(
                cac.PartyLegalEntity(
                    cbc.RegistrationName(getattr(self, context+'_name')),
                    cbc.CompanyID(
                            getattr(self, context+'_id'),
                            schemeAgencyID="195",
                            schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
                            schemeID=dv,
                            schemeName=getattr(self, context+'_type_id')
                        ),
                    cac.CorporateRegistrationScheme(
                        *((cbc.ID(self.prefix),) if context == 'company' else tuple()),
                        cbc.Name(getattr(self, context+'_registration')),
                    )
                )
            )
            elements_append(cac.Contact(
                    cbc.Name(getattr(self, context+'_name')),
                    cbc.Telephone(getattr(self, context+'_phone')),
                    cbc.Telefax(''),
                    cbc.ElectronicMail(getattr(self, context+'_email')),
                    cbc.Note(''),
                ))

        if self.type == 'out' and context == 'party' and self._get_person_party():
            elements_append(self._get_person_party())

        party = cac.Party()
        for element in elements:
            party.append(element)
        party_.append(party)

        return party_

    def _get_physical_location_(self, context):
        physical_location = None
        if self.type == 'out' and context == 'company':
            physical_location = self._get_shop_physical_location()
        # FIX ME PostalZone
        if not physical_location:
            physical_location = cac.PhysicalLocation(
                cac.Address(
                    cbc.ID(getattr(self, context+'_city_code')),
                    cbc.CityName(getattr(self, context+'_city').upper()),
                    cbc.PostalZone('000000'),
                    cbc.CountrySubentity(getattr(self, context+'_department').upper()),
                    cbc.CountrySubentityCode(getattr(self, context+'_department_code')),
                    cac.AddressLine(
                        cbc.Line(getattr(self, context+'_address')),
                    ),
                    cac.Country(
                        cbc.IdentificationCode(getattr(self, context+'_country_code')),
                        cbc.Name(getattr(self, context+'_country_name'), languageID="es"),
                    ),
                ),

            )

        return physical_location

    def _get_party_tax_scheme_(self, context):
        cadena = self.get_tax_code()
        postal = '000000'
        if context == 'company':
            postal = self.company_postal_zone[0]

        try:
            dv = str(self.invoice.get_check_digit(getattr(self, context+'_id')))
        except:
            dv = ''
        party_tax_scheme = cac.PartyTaxScheme(
            cbc.RegistrationName(getattr(self, context+'_name')),
            cbc.CompanyID(
                    str(getattr(self, context+'_id')),
                    schemeID=dv,
                    schemeName=getattr(self, context+'_type_id'),
                    schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
                    schemeAgencyID="195"
                ),
            cbc.TaxLevelCode(cadena.replace(" ", ""), listName=self.fiscal_regimen_company),
            cac.RegistrationAddress(
                cbc.ID(getattr(self, context+'_city_code')),
                cbc.CityName(getattr(self, context+'_city').upper()),
                cbc.PostalZone(postal),
                cbc.CountrySubentity(getattr(self, context+'_department').upper()),
                cbc.CountrySubentityCode (getattr(self, context+'_department_code')),
                cac.AddressLine(
                    cbc.Line(getattr(self, context+'_address')),
                ),
                cac.Country(
                    cbc.IdentificationCode(getattr(self, context+'_country_code')),
                    cbc.Name(getattr(self, context+'_country_name'), languageID="es"),
                ),
            ),
        )
        if not getattr(self, context+'_tributes'):
            party_tax_scheme.append(
                cac.TaxScheme(
                    cbc.ID('ZZ'),
                    cbc.Name('No aplica'),
                    ),
                )
        for tribute in getattr(self, context+'_tributes'):
            party_tax_scheme.append(
                cac.TaxScheme(
                    cbc.ID(tribute.code),
                    cbc.Name(tribute.name),
                    ),
            )
        return party_tax_scheme

    # Si el emisor es Consorcio
    def _get_shareholder_party(self):
        return

    def _get_physical_location(self):
        physical_location = self._get_shop_physical_location()
        # FIX ME PostalZone
        if not physical_location:
            physical_location = cac.PhysicalLocation(
                cac.Address(
                    cbc.ID(self.company_city_code),
                    cbc.CityName(self.company_city.upper()),
                    cbc.PostalZone('000000'),
                    cbc.CountrySubentity(self.company_department.upper()),
                    cbc.CountrySubentityCode(self.company_department_code),
                    cac.AddressLine(
                        cbc.Line(self.company_address),
                    ),
                    cac.Country(
                        cbc.IdentificationCode(self.company_country_code),
                        cbc.Name("Colombia", languageID="es"),
                    ),
                ),

            )

        return physical_location

    def _get_shop_physical_location(self):
        shop_physical_location = None
        if self.shop_address:
            shop_physical_location = cac.PhysicalLocation(
                cac.Address(
                    cbc.ID(self.shop_address.department_code.code+self.shop_address.city_code.code),
                    cbc.CityName(self.shop_address.city_code.name.upper()),
                    cbc.PostalZone(self.company_postal_zone[0]),
                    cbc.CountrySubentity(self.shop_address.department_code.name.upper()),
                    cbc.CountrySubentityCode(self.shop_address.department_code.code),
                    cac.AddressLine(
                        cbc.Line(self.shop_address.street.replace('\n', '')),
                    ),
                    cac.Country(
                        cbc.IdentificationCode(self.company_country_code),
                        cbc.Name("Colombia", languageID="es"),
                    ),
                ),

            )
        return shop_physical_location

    def get_tax_code(self):
        party_obligation_tax = [of.code.replace(" ", "") for of in self.invoice.company.party.party_obligation_tax]
        if not party_obligation_tax:
            cadena = 'ZZ'
        else:
            cadena = ";".join(party_obligation_tax)
        return cadena

    def _get_party_tax_scheme(self):
        cadena = self.get_tax_code()

        party_tax_scheme = cac.PartyTaxScheme(
            cbc.RegistrationName(self.company_name),
            cbc.CompanyID(str(self.company_id), schemeID=str(self.invoice.get_check_digit(self.company_id)), schemeName=self.company_type_id, schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeAgencyID="195"),
            cbc.TaxLevelCode(cadena.replace(" ", ""), listName=self.fiscal_regimen_company),
            cac.RegistrationAddress(
                cbc.ID(self.company_city_code),
                cbc.CityName(self.company_city.upper()),
                cbc.PostalZone(self.company_postal_zone[0]),
                cbc.CountrySubentity(self.company_department.upper()),
                cbc.CountrySubentityCode (self.company_department_code),
                cac.AddressLine(
                    cbc.Line(self.company_address),
                ),
                cac.Country(
                    cbc.IdentificationCode("CO"),
                    cbc.Name("Colombia", languageID="es"),
                ),
            ),
        )
        if not self.company_tributes:
            party_tax_scheme.append(
                cac.TaxScheme(
                    cbc.ID('ZZ'),
                    cbc.Name('No aplica'),
                    ),
                )
        for tribute in self.company_tributes:
            party_tax_scheme.append(
                cac.TaxScheme(
                    cbc.ID(tribute.code),
                    cbc.Name(tribute.name),
                    ),
            )
        return party_tax_scheme

    def _get_customer_party(self):

        customer_party = cac.AccountingCustomerParty(
            cbc.AdditionalAccountID(self.party_tax_level_code),
        )
        party_ = cac.Party(
                cac.PartyIdentification(
                    cbc.ID(self.party_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
                        schemeID=str(self.invoice.get_check_digit(self.party_id)), schemeName=self.party_type_id),
                ),
                # cbc.IndustryClasificationCode(self.party_ciiu_code),
                cac.PartyName(
                    cbc.Name(self.party_name),
                ),
                self._get_physical_location_customer(),
                self._get_party_tax_scheme_customer(),
                cac.PartyLegalEntity(
                    cbc.RegistrationName(self.party_name),
                    cbc.CompanyID(self.party_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
                        schemeID=str(self.invoice.get_check_digit(self.party_id)), schemeName=self.party_type_id),
                    cac.CorporateRegistrationScheme(
                        cbc.Name(self.party_registration),
                    ),
                ),
                cac.Contact(
                    cbc.Name(self.party_name),
                    cbc.Telephone(self.party_phone),
                    cbc.Telefax(''),
                    cbc.ElectronicMail(self.party_email),
                    cbc.Note(''),
                ),
            )

        if self._get_person_party():
            party_.append(self._get_person_party())

        customer_party.append(party_)

        return customer_party
    
    def _get_person_party(self):
        person = None
        if str(self.party_tax_level_code) == '2':
            person = cac.Person(
                cbc.FirstName(self.party_name),
            )
        return person

    def _get_delivery_terms(self):
        delivery_terms = cac.DeliveryTerms(
            cbc.ID('1'),
            cbc.LossRiskResponsibilityCode('DAP')
        )
        return delivery_terms

    def _get_tax_representative_party(self):
        tax_representative = cac.TaxRepresentativeParty(
            cac.PartyIdentification(
                cbc.ID(self.company_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
                    schemeID=str(self.invoice.company.party.check_digit), schemeName=self.company_type_id),
            ),
        )
        return tax_representative

    def _get_physical_location_customer(self):

        physical_location = cac.PhysicalLocation(
            cac.Address(
                cbc.ID(self.party_city_code),
                cbc.CityName(self.party_city),
                cbc.PostalZone('000000'),
                cbc.CountrySubentity(self.party_department),
                cbc.CountrySubentityCode(self.party_department_code),
                cac.AddressLine(
                    cbc.Line(self.party_address),
                ),
                cac.Country(
                    cbc.IdentificationCode(self.party_country_code),
                    cbc.Name(self.party_country_name, languageID="es"),
                ),
            ),
        )
        return physical_location

    def _get_party_tax_scheme_customer(self):
        cadena = self.get_tax_code()

        party_tax_scheme = cac.PartyTaxScheme(
            cbc.RegistrationName(self.party_name),
            cbc.CompanyID(self.party_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
                schemeID=str(self.invoice.get_check_digit(self.party_id)), schemeName=self.party_type_id),
            cbc.TaxLevelCode(cadena.replace(" ", ""), listName=self.fiscal_regimen_party),
            *((cac.RegistrationAddress(
                cbc.ID(self.party_city_code),
                cbc.CityName(self.party_city),
                cbc.PostalZone('000000'),
                cbc.CountrySubentity(self.party_department),
                cbc.CountrySubentityCode(self.party_department_code),
                cac.AddressLine(
                    cbc.Line(self.party_address),
                ),
                cac.Country(
                    cbc.IdentificationCode(self.party_country_code),
                    cbc.Name(self.party_country_name, languageID="es"),
                ),
            )) if self.type == 'out' else tuple()),
        )
        if not self.party_tributes:
            party_tax_scheme.append(
                cac.TaxScheme(
                    cbc.ID('ZZ'),
                    cbc.Name('No aplica'),
                ),
            )
        for tribute in self.party_tributes:
            party_tax_scheme.append(
                cac.TaxScheme(
                    cbc.ID(tribute.code),
                    cbc.Name(tribute.name),
                ),
            )
        return party_tax_scheme

    def get_delivery_address(self):
        delivery = cac.Delivery(
            cbc.ActualDeliveryDate(self.issue_date), # datos de la empresa transportadora
            cbc.ActualDeliveryTime(self.issue_time), # datos de la empresa transportadora
            cac.DeliveryAddress(
                cbc.ID(self.company_city_code),
                cbc.CityName(self.company_city.upper()),
                cbc.PostalZone(self.company_postal_zone[0]),
                cbc.CountrySubentity(self.company_department.upper()),
                cbc.CountrySubentityCode(self.company_department_code),
                cac.AddressLine(
                    cbc.Line(self.company_address),
                ),
                cac.Country(
                    cbc.IdentificationCode(self.company_country_code),
                    cbc.Name("Colombia", languageID="es"),
                ),
            )

        )
        return delivery

    def get_payment_means(self):
        payment_means = cac.PaymentMeans(
            cbc.ID(self.payment_type),
            cbc.PaymentMeansCode(self.payment_means_code),
            cbc.PaymentDueDate(str(self.due_date)),
            cbc.PaymentID('1'),
        )
        return payment_means

    def get_payment_exchange_rate(self):
        payment_exchange = cac.PaymentExchangeRate(
            cbc.SourceCurrencyCode(self.currency),
            cbc.SourceCurrencyBaseRate('1.00'),
            cbc.TargetCurrencyCode('COP'),
            cbc.TargetCurrencyBaseRate('1.00'),
            cbc.CalculationRate(str(round((1/self.invoice.currency.rate), 2))),
            cbc.Date(str(self.invoice.currency_date)),
        )
        return payment_exchange

    def _get_prepaid_amount(self):
        date_payment = ''
        amount_payment = 0
        return date_payment, amount_payment

    def get_prepaid_payment(self):
        prepaid_payment = None
        date_payment, amount_payment = self._get_prepaid_amount()

        if amount_payment > 0:
            prepaid_payment = cac.PrepaidPayment(
                cbc.ID('1'),
                cbc.PaidAmount(str(amount_payment), currencyID=self.currency),
                cbc.ReceivedDate(str(date_payment)),
                cbc.PaidDate(str(date_payment)),
                cbc.PaidTime(self.issue_time),
                cbc.InstructionID(''),
            )
        return prepaid_payment

    def get_global_charges(self, charge, sequence):
        _charge = cac.AllowanceCharge(
                cbc.ID(str(sequence)),
                cbc.ChargeIndicator('false' if charge.charge_concept else 'true'),
                cbc.AllowanceChargeReasonCode(charge.charge_concept),
                cbc.AllowanceChargeReason(charge.description),
                cbc.MultiplierFactorNumeric(rvalue(charge.charge_percentage)),
                cbc.Amount(rvalue(charge.amount), currencyID=self.currency),
                cbc.BaseAmount(rvalue(charge.base_amount), currencyID=self.currency),
            )
        return _charge

    def _get_taxes(self, code):
        taxes = []
        sum_tax_amount = []
        base_unit_measure = '0.00'
        for taxline in self.taxes:
            if taxline.tax.classification_tax != code or taxline.tax.classification_tax in ['NA', 'renta', 'autorenta']:
                continue

            tax_id = taxline.tax.classification_tax
            tax_amount = rvalue(taxline.amount)
            tax_base = rvalue(taxline.base)
            taxable_amount = tax_base
            sum_tax_amount.append(float(tax_amount))
            if not tax_id:
                self.invoice.get_message('Falta definir la clasificación del impuesto ' + taxline.tax.name)

            if taxline.tax.type == 'fixed':
                if tax_id != '02':
                    tax_base = rvalue(taxline.tax.amount)
                    for line in self.lines:
                        quantity = []
                        tax_fixed = [tax for tax in line.taxes if tax.id == taxline.tax.id]
                        if len(tax_fixed) > 0:
                            quantity.append(line.quantity)
                    taxable_amount = rvalue(round(Decimal(sum(quantity)), 2))
                taxes.append(
                    cac.TaxSubtotal(
                        cbc.TaxableAmount(taxable_amount, currencyID=self.currency),
                        cbc.TaxAmount(tax_amount, currencyID=self.currency),
                        cbc.BaseUnitMeasure(tax_base, unitCode="EA"),
                        cbc.PerUnitAmount(tax_base, currencyID=self.currency),
                        cac.TaxCategory(
                                cbc.Percent('0.00'),
                                cac.TaxScheme(
                                    cbc.ID(tax_id),
                                    cbc.Name(CLASIFICATION_TAX[taxline.tax.classification_tax]),
                                ),
                        ),
                    ),
                )
            else:
                percent = str(abs(round(taxline.tax.rate * 100, 2)))
                taxes.append(
                    cac.TaxSubtotal(
                        cbc.TaxableAmount(tax_base, currencyID=self.currency),
                        cbc.TaxAmount(tax_amount, currencyID=self.currency),
                        cac.TaxCategory(
                            cbc.Percent(percent),
                            cac.TaxScheme(
                                cbc.ID(tax_id),
                                cbc.Name(CLASIFICATION_TAX[taxline.tax.classification_tax]),
                            ),
                        ),
                    ),
                )
        return sum(sum_tax_amount), taxes

    def _get_total(self):
        tax_exclusive_amount = 0
        tax_amount = sum([taxline.amount or 0 for taxline in self.taxes if tax_valid(taxline.tax)])
        if tax_amount != 0:
            tax_exclusive_amount = sum([
                taxline.base or 0 for taxline in self.taxes
                    if taxline.tax.rate > 0 and
                        taxline.tax.classification_tax in '01'
                ])

        tax_inclusive_amount = Decimal(self.untaxed_amount) + abs(tax_amount)
        payable_amount = tax_inclusive_amount
        discount = 0
        if hasattr(self.invoice, 'charges'):
            total_charges = self.invoice.total_charges
            payable_amount = payable_amount - total_charges

        _, prepaid_amount = self._get_prepaid_amount()
        if prepaid_amount > 0:
            payable_amount = payable_amount - prepaid_amount

        discount = rvalue(total_charges)
        if self.invoice_type_code != '92':
            return cac.LegalMonetaryTotal(
                cbc.LineExtensionAmount(self.untaxed_amount, currencyID=self.currency),
                cbc.TaxExclusiveAmount(str(abs(tax_exclusive_amount)), currencyID=self.currency),
                cbc.TaxInclusiveAmount(str(tax_inclusive_amount), currencyID=self.currency),
                cbc.AllowanceTotalAmount(discount, currencyID=self.currency),
                cbc.ChargeTotalAmount('0.00', currencyID=self.currency),
                cbc.PrepaidAmount(str(prepaid_amount), currencyID=self.currency),
                cbc.PayableAmount(str(payable_amount), currencyID=self.currency),
            )
        else:
            return cac.RequestedMonetaryTotal(
                cbc.LineExtensionAmount(self.untaxed_amount, currencyID=self.currency),
                cbc.TaxExclusiveAmount(str(abs(tax_exclusive_amount)), currencyID=self.currency),
                cbc.TaxInclusiveAmount(str(tax_inclusive_amount), currencyID=self.currency),
                cbc.AllowanceTotalAmount(discount, currencyID=self.currency),
                cbc.ChargeTotalAmount('0.00', currencyID=self.currency),
                cbc.PrepaidAmount(str(prepaid_amount), currencyID=self.currency),
                cbc.PayableAmount(str(payable_amount), currencyID=self.currency),
            )

    def validate_line_taxes(self, line):
        tax_codes = [tax.classification_tax for tax in line.taxes if tax.classification_tax in TAXES_CODE_VALID]
        order_tax_codes = sorted(list(set(tax_codes)))
        return order_tax_codes

    def _get_line(self, line, sequence):
        pack_quantity = str(line.pack_quantity) if line.pack_quantity else '1'

        invoice_line = cac.InvoiceLine(
            cbc.ID(sequence),
            *((cbc.UUID('Line'+sequence)) if self.type == 'out' else tuple()),
            cbc.InvoicedQuantity(str(self.validate_value(round(line.quantity, 6))), unitCode="EA"),
            cbc.LineExtensionAmount(str(self.validate_value(line.amount)), currencyID=self.currency),

        )
        if line.amount == 0.0:
            list_price = line.product.list_price
            amount = list_price if list_price and list_price > 0 else 1
            invoice_line.append(cac.PricingReference(
                cac.AlternativeConditionPrice(
                    cbc.PriceAmount(str(self.validate_value(amount)), currencyID=self.currency),
                    cbc.PriceTypeCode("01")
                )
            ))
        # FIX ME
        # cac.AllowanceCharge(
        # cbc.ChargeIndicator('false'),
        # cbc.Amount("0", currencyID=self.currency),
        # ),

        if line.taxes:
            tax_total = []
            order_tax_codes = self.validate_line_taxes(line)
            for code in order_tax_codes:
                line_tax_amount, subtotals = self._get_tax_lines(line, code)
                if line_tax_amount > 0:
                    tax_total = cac.TaxTotal(
                        cbc.TaxAmount(str(line_tax_amount), currencyID=self.currency),
                        *((cbc.RoundingAmount('0.00', currencyID=self.currency),
                        cbc.TaxEvidenceIndicator('false')) if self.type == 'out' else tuple()) ,
                        *subtotals
                    )
                elif line_tax_amount < 0:
                    tax_total = cac.WithholdingTaxTotal(
                        cbc.TaxAmount(
                            str(abs(line_tax_amount)),
                            currencyID=self.currency
                        ),
                        *subtotals
                    )
                if tax_total:
                    invoice_line.append(tax_total)
        invoice_line.append(cac.Item(
                cbc.Description(line.product.rec_name or line.product.description),
                cbc.PackSizeNumeric(pack_quantity),
                cbc.BrandName(line.product.brand.name if hasattr(line.product, 'brand') and line.product.brand else 'UND'),
                cbc.ModelName(line.product.reference if hasattr(line.product, 'reference') and line.product.reference else ''),
                cac.SellersItemIdentification(
                    cbc.ID(line.product.code or '', ),
                    cbc.ExtendedID('')
                ),
                cac.StandardItemIdentification(
                    cbc.ID(
                        line.product.code or '',
                        schemeID="999",
                        schemeName="description"
                    ),
                ),
            ),
        )
        invoice_line.append(
            cac.Price(
                cbc.PriceAmount(str(self.validate_value(line.unit_price)), currencyID=self.currency),
                cbc.BaseQuantity(str(self.validate_value(round(line.quantity, 1))), unitCode="EA")
            ),
        )
        return invoice_line

    def _get_line_(self, line, sequence):
        pack_quantity = str(line.pack_quantity) if line.pack_quantity else '1'
        line_quantity = str(self.validate_value(round(line.quantity, 6)))

        if self.invoice.invoice_type in ('1', '2', '3', '4', '05'):
            invoice_line = cac.InvoiceLine()
            element_quantity = cbc.InvoicedQuantity(line_quantity, unitCode="EA")
        elif self.invoice.invoice_type in ('95', '91'):
            invoice_line = cac.CreditNoteLine()
            element_quantity = cbc.CreditedQuantity(line_quantity, unitCode="EA")
        elif self.invoice.invoice_type in ('92'):
            invoice_line = cac.DebitNoteLine()
            element_quantity = cbc.DebitedQuantity(line_quantity, unitCode="EA")
        
        invoice_line.append(cbc.ID(sequence))
        if self.type == 'out' and self.invoice.invoice_type not in ('91', '92'):
            invoice_line.append(cbc.UUID('Line'+sequence))

        invoice_line.append(element_quantity)
        invoice_line.append(cbc.LineExtensionAmount(str(abs(self.validate_value(line.amount))), currencyID=self.currency))
        
        if self.type == 'in':
            invoice_line.append(cac.InvoicePeriod(
                cbc.StartDate(self.issue_date),
                cbc.DescriptionCode("1"),
                cbc.Description("Por operación"),
            ))
        if line.amount == 0.0 and self.type == 'out':
            list_price = line.product.list_price
            amount = list_price if list_price and list_price > 0 else 1
            invoice_line.append(cac.PricingReference(
                cac.AlternativeConditionPrice(
                    cbc.PriceAmount(str(self.validate_value(amount)), currencyID=self.currency),
                    cbc.PriceTypeCode("01")
                )
            ))
        # FIX ME
        # cac.AllowanceCharge(
        # cbc.ChargeIndicator('false'),
        # cbc.Amount("0", currencyID=self.currency),
        # ),

        if line.taxes:
            tax_total = []
            order_tax_codes = self.validate_line_taxes(line)
            for code in order_tax_codes:
                line_tax_amount, subtotals = self._get_tax_lines(line, code)
                if self.invoice.total_amount > 0:
                    if line_tax_amount > 0:
                        tax_total = cac.TaxTotal(
                            cbc.TaxAmount(str(line_tax_amount), currencyID=self.currency),
                            *((cbc.RoundingAmount('0.00', currencyID=self.currency),
                            cbc.TaxEvidenceIndicator('false')) if self.type == 'out' else tuple()) ,
                            *subtotals
                        )
                    elif line_tax_amount < 0:
                        tax_total = cac.WithholdingTaxTotal(
                            cbc.TaxAmount(
                                str(abs(line_tax_amount)),
                                currencyID=self.currency
                            ),
                            *subtotals
                        )
                elif self.invoice.invoice_type in ('91', '92', '95'):
                    if line_tax_amount < 0:
                        tax_total = cac.TaxTotal(
                        cbc.TaxAmount(str(abs(line_tax_amount)), currencyID=self.currency),
                        cbc.RoundingAmount('0.00', currencyID=self.currency),
                        *subtotals
                        )
                    elif line_tax_amount > 0:
                        tax_total = cac.WithholdingTaxTotal(
                        cbc.TaxAmount(str(abs(line_tax_amount)), currencyID=self.currency),
                        *subtotals
                        )

                if tax_total:
                    invoice_line.append(tax_total)
        invoice_line.append(cac.Item(
                cbc.Description(line.product.rec_name or line.product.description),
                cbc.PackSizeNumeric(pack_quantity),
                cbc.BrandName(line.product.brand.name if hasattr(line.product, 'brand') and line.product.brand else 'UND'),
                cbc.ModelName(line.product.reference if hasattr(line.product, 'reference') and line.product.reference else ''),
                cac.SellersItemIdentification(
                    cbc.ID(line.product.code or '', ),
                    cbc.ExtendedID('')
                ),
                cac.StandardItemIdentification(
                    cbc.ID(
                        line.product.code or '',
                        schemeID="999",
                        schemeName="description"
                    ),
                ),
            ),
        )
        invoice_line.append(
            cac.Price(
                cbc.PriceAmount(str(self.validate_value(line.unit_price)), currencyID=self.currency),
                cbc.BaseQuantity(str(self.validate_value(round(line.quantity, 1))), unitCode="EA")
            ),
        )
        return invoice_line

    def _get_tax_lines(self, line, code):
        subtotals = []
        line_tax_amount = '0'
        sum_tax_amount = []
        line_tax_base = '0'
        per_unit_amount = '0'
        for t in line.taxes:
            if not t.classification_tax:
                self.invoice.get_message('Falta asignar clasificación al impuesto ' + t.name)
            if t.classification_tax != code:
                continue

            tax_id = t.classification_tax
            if t.type == 'fixed':
                if tax_id == '02':
                    line_tax_amount = rvalue(Decimal(float(line.product.extra_tax) * line.quantity))
                    sum_tax_amount.append(Decimal(float(line.product.extra_tax) * line.quantity))
                    line_tax_base = rvalue(line.product.extra_tax)
                    line_taxable_amount = line_tax_base
                else:
                    line_tax_amount = rvalue(Decimal(float(t.amount) * line.quantity))
                    sum_tax_amount.append(Decimal(float(t.amount) * line.quantity))
                    line_tax_base = rvalue(t.amount)
                    line_taxable_amount = rvalue(line.quantity)
                subtotals.append(
                    cac.TaxSubtotal(
                        cbc.TaxableAmount(line_taxable_amount, currencyID=self.currency),
                        cbc.TaxAmount(line_tax_amount, currencyID=self.currency),
                        *((cbc.BaseUnitMeasure(line_tax_base, unitCode="EA"),
                        cbc.PerUnitAmount(line_tax_base, currencyID=self.currency)) if self.type == 'out' else tuple()),
                        cac.TaxCategory(
                            cbc.Percent('0.00'),
                            cac.TaxScheme(
                                cbc.ID(tax_id),
                                cbc.Name(CLASIFICATION_TAX[t.classification_tax]),
                            )
                        )
                    )
                )
            else:
                line_tax_amount = rvalue(t.rate * line.amount)
                sum_tax_amount.append(t.rate * line.amount)
                line_tax_base = rvalue(line.amount)
                percentage = abs((t.rate * 100))
                subtotals.append(
                    cac.TaxSubtotal(
                        cbc.TaxableAmount(line_tax_base, currencyID=self.currency),
                        cbc.TaxAmount(line_tax_amount, currencyID=self.currency),
                        cac.TaxCategory(
                            cbc.Percent(rvalue(percentage)),
                            cac.TaxScheme(
                                cbc.ID(tax_id),
                                cbc.Name(CLASIFICATION_TAX[t.classification_tax]),
                            ),
                        ),
                    ),
                )
        return float(sum(sum_tax_amount)), subtotals

    def _get_taxes_total(self, code_list, xml_invoice):
        if not code_list:
            return xml_invoice
        for code in code_list:
            sum_tax_amount, taxes = self._get_taxes(code)
            # if sum_tax_amount != 0:
            tax_total = cac.TaxTotal(
                            cbc.TaxAmount(rvalue(sum_tax_amount), currencyID=self.currency),
                            cbc.RoundingAmount('0.00', currencyID=self.currency),
                            )
            for tax in taxes:
                tax_total.append(tax)
            xml_invoice.append(tax_total)

        return xml_invoice

    def _get_withholding_total(self, code_list, xml_invoice):
        for code in code_list:
            sum_tax_amount, taxes = self._get_taxes(code)
            if sum_tax_amount != 0:
                withholding_total = cac.WithholdingTaxTotal(
                                cbc.TaxAmount(rvalue(sum_tax_amount), currencyID=self.currency),
                                )
                for tax in taxes:
                    withholding_total.append(tax)
                xml_invoice.append(withholding_total)
        return xml_invoice

    def _get_discrepancy_response(self):
        discrepancy_response = None
        if self.invoice.invoice_type == '92':
            discrepancy_response = cac.DiscrepancyResponse(
                cbc.ReferenceID(self.original_invoice_number),
                cbc.ResponseCode(self.debit_note_concept),
                cbc.Description('Factura anterior'),
            )
        elif self.invoice.invoice_type == '91':
            discrepancy_response = cac.DiscrepancyResponse(
                cbc.ReferenceID(self.original_invoice_number),
                cbc.ResponseCode(self.credit_note_concept),
                cbc.Description('Factura anterior'),
            )
        return discrepancy_response
# ----------------------------------------------- DEBIT NOTE FUNCTIONS------------------------------------------------------

    def _get_head_debit_note(self):
        if self.invoice.authorization.provider == 'psk':
            head = self._get_head_debit_note_psk()
        else:
            head = self._get_head_debit_note_est()

        return head

    def _get_head_debit_note_psk(self):

        head = Root_psk_dn.DebitNote(
            ext.UBLExtensions(
                ext.UBLExtension(
                    ext.ExtensionContent(
                        sts_psk.DianExtensions(
                            sts_psk.InvoiceControl(
                                sts_psk.InvoiceAuthorization(self.invoice_authorization),
                                sts_psk.AuthorizationPeriod(
                                    cbc.StartDate(self.start_date_auth),
                                    cbc.EndDate(self.end_date_auth)
                                ),
                                sts_psk.AuthorizedInvoices(
                                    sts_psk.Prefix(self.prefix),
                                    sts_psk.From(self.from_auth),
                                    sts_psk.To(self.to_auth),
                                ),
                            ),
                            sts_psk.InvoiceSource(
                                cbc.IdentificationCode('CO', listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listSchemeURI="urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1")
                            ),
                            sts_psk.SoftwareProvider(
                                sts_psk.ProviderID(self.software_provider_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID=str(self.check_digit_provider), schemeName="31"),
                                sts_psk.SoftwareID(self.software_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            ),
                            sts_psk.SoftwareSecurityCode(self.invoice.get_security_code(), schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            sts_psk.AuthorizationProvider(
                                sts_psk.AuthorizationProviderID('800197268',
                                    schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID="4", schemeName="31"),
                            ),
                            sts_psk.QRCode(self.invoice.get_link_dian()),
                        ),
                    ),
                ),
                ext.UBLExtension(
                    ext.ExtensionContent(),
                ),
            ),
            cbc.UBLVersionID('UBL 2.1'),
            cbc.CustomizationID(self.operation_type),
            cbc.ProfileID('DIAN 2.1: Nota Débito de Factura Electrónica de Venta'),
            cbc.ProfileExecutionID(self.auth_environment),
            cbc.ID(self.number),
            cbc.UUID(self.invoice.get_cufe('92'), schemeID=self.auth_environment, schemeName="CUDE-SHA384"),
            cbc.IssueDate(self.issue_date),
            cbc.IssueTime(self.issue_time),
            cbc.Note(self.comment),
            cbc.DocumentCurrencyCode(self.currency, listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listID="ISO 4217 Alpha"),
            cbc.LineCountNumeric(str(len(self.lines))),
        )
        return head

    def _get_head_debit_note_est(self):
        software_id_ = self._get_software_info()

        head = Root_dn.DebitNote(
            ext.UBLExtensions(
                ext.UBLExtension(
                    ext.ExtensionContent(
                        sts.DianExtensions(
                            sts.InvoiceControl(
                                sts.InvoiceAuthorization(self.invoice_authorization),
                                sts.AuthorizationPeriod(
                                    cbc.StartDate(self.start_date_auth),
                                    cbc.EndDate(self.end_date_auth)
                                ),
                                sts.AuthorizedInvoices(
                                    sts.Prefix(self.prefix),
                                    sts.From(self.from_auth),
                                    sts.To(self.to_auth),
                                ),
                            ),
                            sts.InvoiceSource(
                                cbc.IdentificationCode('CO', listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listSchemeURI="urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1")
                            ),
                            sts.SoftwareProvider(
                                sts.ProviderID(self.software_provider_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID=str(self.check_digit_provider), schemeName="31"),
                                software_id_,
                            ),
                            sts.SoftwareSecurityCode("", schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            sts.AuthorizationProvider(
                                sts.AuthorizationProviderID('800197268',
                                    schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID="4", schemeName="31"),
                            ),
                            sts.QRCode(''),
                        ),
                    ),
                ),
            ),
            cbc.UBLVersionID('UBL 2.1'),
            cbc.CustomizationID(self.operation_type),
            cbc.ProfileID('DIAN 2.1: Nota Débito de Factura Electrónica de Venta'),
            cbc.ProfileExecutionID(self.auth_environment),
            cbc.ID(self.number),
            cbc.UUID('', schemeID=self.auth_environment, schemeName="CUFE-SHA384"),
            cbc.IssueDate(self.issue_date),
            cbc.IssueTime(self.issue_time),
            # cbc.DueDate(self.due_date),
            # cbc.DebitNoteTypeCode(self.invoice_type_code),
            cbc.Note(self.comment),
            # cbc.DocumentCurrencyCode('COP'),
            cbc.DocumentCurrencyCode(self.currency, listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listID="ISO 4217 Alpha"),
            cbc.LineCountNumeric(str(len(self.lines))),
        )
        return head

    def _get_line_debit_note(self, line, sequence):
        pack_quantity = str(line.pack_quantity) if line.pack_quantity else '1'

        invoice_line = cac.DebitNoteLine(
            cbc.ID(sequence),
            # cbc.UUID('Line'+sequence),
            cbc.DebitedQuantity(str(self.validate_value(round(line.quantity, 6))), unitCode="EA"),
            cbc.LineExtensionAmount(str(self.validate_value(abs(line.amount))), currencyID=self.currency),
        )

        if line.taxes:
            tax_total = []
            tax_code_list = self.validate_line_taxes(line)
            for code in tax_code_list:
                line_tax_amount, subtotals = self._get_tax_lines(line, code)
                if self.invoice.total_amount > 0:
                    if line_tax_amount > 0:
                        tax_total = cac.TaxTotal(
                            cbc.TaxAmount(str(abs(line_tax_amount)), currencyID=self.currency),
                            cbc.RoundingAmount('0.00', currencyID=self.currency),
                            *subtotals
                        )
                    elif line_tax_amount < 0:
                        tax_total = cac.WithholdingTaxTotal(
                        cbc.TaxAmount(str(abs(line_tax_amount)), currencyID=self.currency),
                        *subtotals
                        )
                else:
                    if line_tax_amount < 0:
                        tax_total = cac.TaxTotal(
                        cbc.TaxAmount(str(abs(line_tax_amount)), currencyID=self.currency),
                        cbc.RoundingAmount('0.00', currencyID=self.currency),
                        *subtotals
                        )
                    elif line_tax_amount > 0:
                        tax_total = cac.WithholdingTaxTotal(
                        cbc.TaxAmount(str(abs(line_tax_amount)), currencyID=self.currency),
                        *subtotals
                        )
                if tax_total:
                    invoice_line.append(tax_total)

        invoice_line.append(cac.Item(
                cbc.Description(line.description if line.description else line.product.name),
                cbc.PackSizeNumeric(pack_quantity),
                cbc.BrandName(line.product.brand.name if hasattr(line.product, 'brand') and line.product.brand else 'UND'),
                cbc.ModelName(line.product.reference if hasattr(line.product, 'reference') and line.product.reference else ''),
                cac.SellersItemIdentification(
                    cbc.ID(line.product.code or '', ),
                    cbc.ExtendedID('')
                ),
            ),
        )
        invoice_line.append(
            cac.Price(
                cbc.PriceAmount(str(self.validate_value(line.unit_price)), currencyID=self.currency),
                cbc.BaseQuantity(str(self.validate_value(round(line.quantity, 1))), unitCode="EA")
            ),
        )
        return invoice_line
#----------------------------------------------- CREDIT NOTE FUNCTIONS------------------------------------------------------

    def _get_line_credit_note(self, line, sequence):
        pack_quantity = str(line.pack_quantity) if line.pack_quantity else '1'
        invoice_line = cac.CreditNoteLine(
            cbc.ID(sequence),
            # cbc.UUID('Line'+sequence),
            cbc.CreditedQuantity("%.6f" % round(self.validate_value(line.quantity), 2), unitCode="EA"),
            cbc.LineExtensionAmount(str(self.validate_value(abs(line.amount))), currencyID=self.currency),
        )

        if line.taxes:
            tax_total = []
            tax_code_list = self.validate_line_taxes(line)
            for code in tax_code_list:
                line_tax_amount, subtotals = self._get_tax_lines(line, code)
                if self.invoice.total_amount > 0:
                    if line_tax_amount > 0:
                        tax_total = cac.TaxTotal(
                            cbc.TaxAmount(str(abs(line_tax_amount)), currencyID=self.currency),
                            cbc.RoundingAmount('0.00', currencyID=self.currency),
                            *subtotals
                        )
                    elif line_tax_amount < 0:
                        tax_total = cac.WithholdingTaxTotal(
                        cbc.TaxAmount(str(abs(line_tax_amount)), currencyID=self.currency),
                        *subtotals
                        )
                else:
                    if line_tax_amount < 0:
                        tax_total = cac.TaxTotal(
                        cbc.TaxAmount(str(abs(line_tax_amount)), currencyID=self.currency),
                        cbc.RoundingAmount('0.00', currencyID=self.currency),
                        *subtotals
                        )
                    elif line_tax_amount > 0:
                        tax_total = cac.WithholdingTaxTotal(
                        cbc.TaxAmount(str(abs(line_tax_amount)), currencyID=self.currency),
                        *subtotals
                        )
                if tax_total:
                    invoice_line.append(tax_total)

        invoice_line.append(cac.Item(
                cbc.Description(line.description if line.description else line.product.name),
                cbc.BrandName(line.product.brand.name if hasattr(line.product, 'brand') and line.product.brand else 'UND'),
                cbc.ModelName(line.product.reference if hasattr(line.product, 'reference') and line.product.reference else ''),
                cac.SellersItemIdentification(
                    cbc.ID(line.product.code or '', ),
                    cbc.ExtendedID('')
                ),
            ),
        )
        # cbc.PackSizeNumeric(pack_quantity),
        invoice_line.append(
            cac.Price(
                cbc.PriceAmount(str(self.validate_value(line.unit_price)), currencyID=self.currency),
                cbc.BaseQuantity("%.2f" % round(self.validate_value(line.quantity), 2), unitCode="EA")
            ),
        )

        return invoice_line

    def _get_head_credit_note(self):
        if self.invoice.authorization.provider == 'psk':
            head = self._get_head_credit_note_psk()
        else:
            head = self._get_head_credit_note_est()

        return head

    def _get_head_credit_note_est(self):
        software_id_ = self._get_software_info()

        head = Root_cn.CreditNote(
            ext.UBLExtensions(
                ext.UBLExtension(
                    ext.ExtensionContent(
                        sts.DianExtensions(
                            sts.InvoiceControl(
                                sts.InvoiceAuthorization(self.invoice_authorization),
                                sts.AuthorizationPeriod(
                                    cbc.StartDate(self.start_date_auth),
                                    cbc.EndDate(self.end_date_auth)
                                ),
                                sts.AuthorizedInvoices(
                                    sts.Prefix(self.prefix),
                                    sts.From(self.from_auth),
                                    sts.To(self.to_auth),
                                ),
                            ),
                            sts.InvoiceSource(
                                cbc.IdentificationCode('CO', listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listSchemeURI="urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1")
                            ),
                            sts.SoftwareProvider(
                                sts.ProviderID(self.software_provider_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID=str(self.check_digit_provider), schemeName="31"),
                                software_id_,
                            ),
                            sts.SoftwareSecurityCode("", schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            sts.AuthorizationProvider(
                                sts.AuthorizationProviderID('800197268',
                                    schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID="4", schemeName="31"),
                            ),
                            sts.QRCode(''),
                        ),
                    ),
                ),
            ),
            cbc.UBLVersionID('UBL 2.1'),
            cbc.CustomizationID(self.operation_type),
            cbc.ProfileID('DIAN 2.1: Nota Crédito de Factura Electrónica de Venta'),
            cbc.ProfileExecutionID(self.auth_environment),
            cbc.ID(self.number),
            cbc.UUID('', schemeID=self.auth_environment, schemeName="CUDE-SHA384"),
            cbc.IssueDate(self.issue_date),
            cbc.IssueTime(self.issue_time),
            # cbc.DueDate(self.due_date),
            cbc.CreditNoteTypeCode(self.invoice_type_code),
            cbc.Note(self.comment),
            # cbc.DocumentCurrencyCode('COP'),
            cbc.DocumentCurrencyCode(self.currency, listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listID="ISO 4217 Alpha"),
            cbc.LineCountNumeric(str(len(self.lines))),
        )
        return head

    def _get_head_credit_note_psk(self):

        head = Root_psk_cn.CreditNote(
            ext.UBLExtensions(
                ext.UBLExtension(
                    ext.ExtensionContent(
                        sts_psk.DianExtensions(
                            sts_psk.InvoiceControl(
                                sts_psk.InvoiceAuthorization(self.invoice_authorization),
                                sts_psk.AuthorizationPeriod(
                                    cbc.StartDate(self.start_date_auth),
                                    cbc.EndDate(self.end_date_auth)
                                ),
                                sts_psk.AuthorizedInvoices(
                                    sts_psk.Prefix(self.prefix),
                                    sts_psk.From(self.from_auth),
                                    sts_psk.To(self.to_auth),
                                ),
                            ),
                            sts_psk.InvoiceSource(
                                cbc.IdentificationCode('CO', listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listSchemeURI="urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1")
                            ),
                            sts_psk.SoftwareProvider(
                                sts_psk.ProviderID(self.software_provider_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID=str(self.check_digit_provider), schemeName="31"),
                                sts_psk.SoftwareID(self.software_id, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            ),
                            sts_psk.SoftwareSecurityCode(self.invoice.get_security_code(), schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            sts_psk.AuthorizationProvider(
                                sts_psk.AuthorizationProviderID('800197268',
                                    schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)", schemeID="4", schemeName="31"),
                            ),
                            sts_psk.QRCode(self.invoice.get_link_dian()),
                        ),
                    ),
                ),
                ext.UBLExtension(
                    ext.ExtensionContent(),
                ),
            ),
            cbc.UBLVersionID('UBL 2.1'),
            cbc.CustomizationID(self.operation_type),
            cbc.ProfileID('DIAN 2.1: Nota Crédito de Factura Electrónica de Venta'),
            cbc.ProfileExecutionID(self.auth_environment),
            cbc.ID(self.number),
            cbc.UUID(self.invoice.get_cufe(), schemeID=self.auth_environment, schemeName="CUDE-SHA384"),
            cbc.IssueDate(self.issue_date),
            cbc.IssueTime(self.issue_time),
            cbc.CreditNoteTypeCode(self.invoice_type_code),
            cbc.Note(self.comment),
            cbc.DocumentCurrencyCode(self.currency, listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listID="ISO 4217 Alpha"),
            cbc.LineCountNumeric(str(len(self.lines))),
        )
        return head

#----------------------------------------------- MAKE ELECTRONIC INVOICE------------------------------------------------------

    def make(self, type):
        print('crea documento ', type)
        element = builder.ElementMaker()
        if type == 'Fq':
            xml_invoice = self._get_head()
            # xml_invoice.append(self._get_invoice_period())
            if self.auth.provider != 'estp':
                xml_invoice.append(self._get_order_reference())
            # si la factura proviene de una nota débito o crédito
            if self._get_billing_reference():
                xml_invoice.append(self._get_billing_reference())
                xml_invoice.append(self._get_despatch_document_reference())
            # xml_invoice.append(self._get_receipt_document_reference())
            # xml_invoice.append(self._get_additional_document_reference())
            xml_invoice.append(self._get_supplier_party())
            customer_party = self._get_customer_party()
            xml_invoice.append(customer_party)
            # FIX ME
            xml_invoice.append(self._get_delivery_terms())
            # xml_invoice.append(self._get_tax_representative_party())
            # xml_invoice.append(self.get_delivery_address())
            xml_invoice.append(self.get_payment_means())
            # if self.get_prepaid_payment():
            #     xml_invoice.append(self.get_prepaid_payment())
            if hasattr(self.invoice, 'charges'):
                sequence = 0
                for charge in self.invoice.charges:
                    sequence += 1
                    xml_invoice.append(self.get_global_charges(charge, sequence))
            if self.invoice_type_code == '02':
                xml_invoice.append(self.get_payment_exchange_rate())

            tax_code_list = [taxline.tax.classification_tax for taxline in self.taxes if tax_valid(taxline.tax)]
            tax_code_list = list(set(tax_code_list))
            xml_invoice = self._get_taxes_total(tax_code_list, xml_invoice)

            tax_code_list = [taxline.tax.classification_tax for taxline in self.taxes if taxline.tax.rate < 0 and
                              taxline.tax.classification_tax in TAXES_CODE_VALID]
            tax_code_list = list(set(tax_code_list))
            xml_invoice = self._get_withholding_total(tax_code_list, xml_invoice)

            totals = self._get_total()
            xml_invoice.append(totals)

            sequence = 0
            for line in self.lines:
                if line.unit_price < 0:
                    continue
                sequence += 1
                xml_invoice.append(self._get_line(line, str(sequence)))

            xml_invoice.attrib[attr_qname] = attr_xsd

        elif type == 'DNE':
            xml_invoice = self._get_head_debit_note()
            # if self.auth.provider == 'estp'
            #     xml_invoice.append(self._get_discrepancy_response())
            if self._get_billing_reference():
                xml_invoice.append(self._get_billing_reference())
            xml_invoice.append(self._get_supplier_party())
            xml_invoice.append(self._get_customer_party())
            xml_invoice.append(self.get_payment_means())

            if self.invoice.currency.id != self.company_currency_id:
                xml_invoice.append(self.get_payment_exchange_rate())

            tax_code_list = [taxline.tax.classification_tax for taxline in self.taxes if tax_valid(taxline.tax)]
            tax_code_list = list(set(tax_code_list))
            xml_invoice = self._get_taxes_total(tax_code_list, xml_invoice)

            tax_code_list = [taxline.tax.classification_tax for taxline in self.taxes if taxline.tax.classification_tax and taxline.tax.classification_tax != 'NA' and taxline.tax.rate and taxline.tax.rate < 0]
            tax_code_list = list(set(tax_code_list))
            xml_invoice = self._get_withholding_total(tax_code_list, xml_invoice)

            totals = self._get_total()
            xml_invoice.append(totals)

            sequence = 0
            for line in self.lines:
                if line.unit_price < 0:
                    continue
                sequence += 1
                xml_invoice.append(self._get_line_debit_note(line, str(sequence)))

            xml_invoice.attrib[attr_qname] = attr_xsd_dn

        elif type == 'CNE':
            xml_invoice = self._get_head_credit_note()
            if self.auth.provider == 'estp':
                xml_invoice.append(self._get_discrepancy_response())
            if self._get_billing_reference():
                xml_invoice.append(self._get_billing_reference())
            xml_invoice.append(self._get_supplier_party())
            xml_invoice.append(self._get_customer_party())
            xml_invoice.append(self.get_payment_means())

            if self.invoice.currency.id != self.company_currency_id:
                xml_invoice.append(self.get_payment_exchange_rate())

            tax_code_list = [taxline.tax.classification_tax for taxline in self.taxes if tax_valid(taxline.tax)]
            tax_code_list = list(set(tax_code_list))
            xml_invoice = self._get_taxes_total(tax_code_list, xml_invoice)

            tax_code_list = [taxline.tax.classification_tax for taxline in self.taxes if taxline.tax.classification_tax and taxline.tax.classification_tax != 'NA' and taxline.tax.rate and taxline.tax.rate < 0]
            tax_code_list = list(set(tax_code_list))
            xml_invoice = self._get_withholding_total(tax_code_list, xml_invoice)

            totals = self._get_total()
            xml_invoice.append(totals)

            sequence = 0
            for line in self.lines:
                if line.unit_price < 0:
                    continue
                sequence += 1
                xml_invoice.append(self._get_line_credit_note(line, str(sequence)))

            xml_invoice.attrib[attr_qname] = attr_xsd_cn

        elif type in ('E', 'EN', 'F', 'DN', 'CN'):
            xml_invoice = self._get_head_()

            if type in ('E', 'F') and self._get_order_reference():
                xml_invoice.append(self._get_order_reference())

            if type in ('F', 'CN', 'DN') and self._get_billing_reference() is not None:
                billing_reference = self._get_billing_reference()
                xml_invoice.append(billing_reference)
                # print('get reference',billing_reference)
                xml_invoice.append(self._get_despatch_document_reference())
            # for e in xml_invoice.getchildren():
            #     print(e, e.getchildren())
            # print(, xml_invoice.items())
            if type not in ('E', 'EN'):
                xml_invoice.append(self._get_party('company'))
                xml_invoice.append(self._get_party('party'))
            else:
                xml_invoice.append(self._get_party('party'))
                xml_invoice.append(self._get_party('company'))

            if type in ('F', 'CN', 'DN'):
                xml_invoice.append(self._get_delivery_terms())

            xml_invoice.append(self.get_payment_means())

            if self.invoice.currency.id != self.company_currency_id:
                xml_invoice.append(self.get_payment_exchange_rate())

            if hasattr(self.invoice, 'charges'):
                sequence = 0
                for charge in self.invoice.charges:
                    sequence += 1
                    xml_invoice.append(self.get_global_charges(charge, sequence))
            tax_code_list = list(set(taxline.tax.classification_tax for taxline in self.taxes if tax_valid(taxline.tax)))
            xml_invoice = self._get_taxes_total(tax_code_list, xml_invoice)

            tax_code_list = list(set(taxline.tax.classification_tax for taxline in self.taxes if tax_valid_witholding(taxline.tax)))
            xml_invoice = self._get_withholding_total(tax_code_list, xml_invoice)

            totals = self._get_total()
            xml_invoice.append(totals)

            sequence = 0
            for line in self.lines:
                if line.unit_price < 0:
                    continue
                sequence += 1
                xml_invoice.append(self._get_line_(line, str(sequence)))

            if type in ('E', 'F'):
                xml_invoice.attrib[attr_qname] = attr_xsd
            elif type in ('CN', 'EN'):
                xml_invoice.attrib[attr_qname] = attr_xsd_cn
            elif type == 'DN':
                xml_invoice.attrib[attr_qname] = attr_xsd_dn

        exml = etree.tostring(xml_invoice, encoding='UTF-8', pretty_print=True, xml_declaration=True)
        return exml


def make_customer(invoice):
    party_obligation_tax = [of.code.replace(" ", "") for of in invoice.company.party.party_obligation_tax]
    if not party_obligation_tax:
        cadena = 'ZZ'
    else:
        cadena = ";".join(party_obligation_tax)
    element = builder.ElementMaker()
    issue_date, issue_time = invoice.get_datetime_local()
    party_company = invoice.company.party
    _party = invoice.party
    attached_document = AttachedDocument.AttachedDocument(
        cbc_customer.UBLVersionID('UBL 2.1'),
        cbc_customer.CustomizationID('Documentos adjuntos'),
        cbc_customer.ProfileID('Factura Electrónica de Venta'),
        cbc_customer.ProfileExecutionID('1'),
        cbc_customer.ID(str(invoice.id)),
        cbc_customer.IssueDate(issue_date),
        cbc_customer.IssueTime(issue_time),
        cbc_customer.DocumentType('Contenedor de Factura Electrónica'),
        cbc_customer.ParentDocumentID(invoice.number),
    )

    party_tax_scheme_company = cac_customer.PartyTaxScheme(
        cbc_customer.RegistrationName(party_company.name),
        cbc_customer.CompanyID(
            str(party_company.id_number),
            schemeAgencyID="195",
            schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
            schemeID=str(invoice.get_check_digit(party_company.id_number)),
            schemeName=party_company.type_document,
            ),
        cbc_customer.TaxLevelCode(cadena.replace(" ", ""), listName=party_company.fiscal_regimen),
    )
    if not invoice.company.party.party_tributes:
        party_tax_scheme_company.append(
            cac_customer.TaxScheme(
                cbc_customer.ID('ZZ'),
                cbc_customer.Name('No aplica'),
                ),
        )
    for tribute in invoice.company.party.party_tributes:
        party_tax_scheme_company.append(
            cac_customer.TaxScheme(
                cbc_customer.ID(tribute.code),
                cbc_customer.Name(tribute.name),
                ),
        )
    sender_party = cac_customer.SenderParty()
    sender_party.append(party_tax_scheme_company)
    #-------------------------------- ReceiverParty -----------------------
    party_tax_scheme_customer = cac_customer.PartyTaxScheme(
        cbc_customer.RegistrationName(_party.name),
        cbc_customer.CompanyID(_party.id_number, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
            schemeID=str(invoice.get_check_digit(_party.id_number)), schemeName=_party.type_document),
        cbc_customer.TaxLevelCode(cadena.replace(" ", ""), listName=invoice.party.fiscal_regimen),
    )
    if not invoice.company.party.party_tributes:
        party_tax_scheme_customer.append(
            cac_customer.TaxScheme(
                cbc_customer.ID('ZZ'),
                cbc_customer.Name('No aplica'),
                ),
            )
    for tribute in invoice.company.party.party_tributes:
        party_tax_scheme_customer.append(
            cac_customer.TaxScheme(
                cbc_customer.ID(tribute.code),
                cbc_customer.Name(tribute.name),
                ),
        )
    receiver_party = cac_customer.ReceiverParty()
    receiver_party.append(party_tax_scheme_customer)

    attached_document.append(sender_party)
    attached_document.append(receiver_party)
    attachment = cac_customer.Attachment(
        cac_customer.ExternalReference(
            cbc_customer.MimeCode('text/xml'),
            cbc_customer.EncodingCode('UTF-8'),
            cbc_customer.Description(etree.CDATA(invoice.xml_face_.decode('utf8'))),
        )
    )
    attached_document.append(attachment)

    parent_document_line = cac_customer.ParentDocumentLineReference(
        cbc_customer.LineID('1'),
        cac_customer.DocumentReference(
            cbc_customer.ID(invoice.number),
            cbc_customer.UUID(invoice.cufe, schemeName="CUFE-SHA384"),
            cbc_customer.IssueDate(issue_date),
            cbc_customer.DocumentType(issue_time),
            cac_customer.Attachment(
                cac_customer.ExternalReference(
                    cbc_customer.MimeCode('text/xml'),
                    cbc_customer.EncodingCode('UTF-8'),
                    cbc_customer.Description(etree.CDATA(invoice.xml_response_dian_.decode('utf8'))),
                ),
            ),
            cac_customer.ResultOfVerification(
                cbc_customer.ValidatorID('Unidad Especial Dirección de Impuestos y Aduanas Nacionales'),
                cbc_customer.ValidationResultCode('02'),
                cbc_customer.ValidationDate(issue_date),
                cbc_customer.ValidationTime(issue_time),
            ),
        ),
    )
    attached_document.append(parent_document_line)
    xml_customer = etree.tostring(attached_document, encoding='UTF-8', pretty_print=True, xml_declaration=True)

    return str(xml_customer, 'utf-8')

def makeResponse(invoice):
    pass


if __name__ == '__main__':
    einvoice = ElectronicInvoice()
    inv = einvoice.make()
    xml = etree.tostring(inv, encoding='utf-8', pretty_print=True)
    file_ = open("/home/psk/Desktop/fe_test2.xml", "w")
    file_.write(xml.decode("utf-8"))
    file_.close()
