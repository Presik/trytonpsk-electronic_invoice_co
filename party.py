# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval

FISCAL_REGIMEN = [
    ('', ''),
    ('48', 'Impuesto sobre las ventas – IVA'),
    ('49', 'No responsable de IVA'),
]


class Configuration(metaclass=PoolMeta):
    __name__ = 'party.configuration'
    fiscal_regimen = fields.Selection(FISCAL_REGIMEN, 'Regimen Fiscal')


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    fiscal_regimen = fields.Selection(FISCAL_REGIMEN, 'Regimen Fiscal')
    party_obligation_tax = fields.Many2Many('party.obligation_tax', 'party',
        'obligation_fiscal', 'Party Obligations Tax')
    party_tributes = fields.Many2Many('party.tribute_tax', 'party',
        'classification_tax', 'Party Tributes')
    commercial_registration = fields.Char('Commercial Registration')
    second_currency = fields.Many2One('currency.currency',
        'Secondary Currency', help='Select second currency \n'
        'for view in reports converted', ondelete="RESTRICT",
        domain=[
            ('id', '!=', Eval('currency', -1)),
            ],
        depends=['currency'])

    @staticmethod
    def default_fiscal_regimen():
        config = Pool().get('party.configuration')(1)
        if config and config.fiscal_regimen:
            return config.fiscal_regimen


class PartyObligationFiscal(ModelSQL, ModelView):
    'Party Obligation Fiscal'
    __name__ = 'party.obligation_fiscal'
    _rec_name = 'name'
    code = fields.Char('Code')
    name = fields.Char('Name')
    active = fields.Boolean('Active')


class PartyObligationTax(ModelSQL):
    'Party Obligations'
    __name__ = 'party.obligation_tax'
    _table = 'party_obligation_tax_rel'
    party = fields.Many2One('party.party', 'Party',
        ondelete='CASCADE', select=True, required=True)
    obligation_fiscal = fields.Many2One('party.obligation_fiscal',
        'Obligation Fiscal', ondelete='RESTRICT', select=True, required=True)


class ClassificationTax(ModelSQL, ModelView):
    'Classification Taxes'
    __name__ = 'party.classification_tax'
    _rec_name = 'name'
    code = fields.Char('Code', readonly=True)
    name = fields.Char('Name', readonly=True)


class PartyTributeTax(ModelSQL):
    'Party Tribute Taxes'
    __name__ = 'party.tribute_tax'
    _table = 'party_obligation_tribute_tax_rel'
    party = fields.Many2One('party.party', 'Party',
        ondelete='CASCADE', select=True, required=True)
    classification_tax = fields.Many2One('party.classification_tax', 'Classification Tax',
        ondelete='RESTRICT', select=True, required=True)
