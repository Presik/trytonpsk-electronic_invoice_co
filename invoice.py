# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import hashlib
import qrcode
import xmltodict
import zipfile
from io import BytesIO
from dateutil import tz
from datetime import datetime, timedelta
from trytond.model import fields, ModelView, ModelSQL
from trytond.report import Report
from trytond.transaction import Transaction
from decimal import Decimal
from datetime import date
from trytond.pyson import Eval, Or, Not, And
from trytond.pool import PoolMeta, Pool
from trytond.exceptions import UserError
from trytond.modules.account_col.invoice import TYPE_INVOICE
from .it_supplier_psk import ElectronicInvoicePsk, ApplicationResponse
from .builder_phase_2 import make_customer, ElectronicInvoice_2
from .builder_response import RESPONSES, TYPE_EVENT, make_customer_response
from .response_supplier_ws import get_status_events, get_email_reception


CONCEPTS_CLAIM = [
        ('', ''),
        ('01', 'Documento con inconsistencias'),
        ('02', 'Mercancía no entregada totalmente'),
        ('03', 'Mercancía no entregada parcialmente'),
        ('04', 'Servicio no prestado'),
    ]

_STATES = {
    'readonly': Eval('state') != 'draft',
}

PRIMOS = [71, 67, 59, 53, 47, 43, 41, 37, 29, 23, 19, 17, 13, 7, 3]
K = 11

USER = 'psk'

_DEPENDS = ['state']

ELECTRONIC_STATES = [
    ('none', 'None'),
    ('submitted', 'Submitted'),
    ('pending', 'Pending'),
    ('rejected', 'Rejected'),
    ('authorized', 'Authorized'),
    ('accepted', 'Accepted'),
]

OPERATION_TYPE_OUT = [
    ('', ''),
    ('09', 'Servicios AIU'),
    ('10', 'Estandar'),
    ('11', 'Mandatos bienes'),
    ('12', 'Transporte'),
    ('13', 'Cambiario'),
    ('20', 'Nota Crédito que referencia una factura electrónica'),
    ('22', 'Nota Crédito sin referencia a facturas'),
    ('30', 'Nota Débito que referencia una factura electrónica'),
    ('32', 'Nota Débito sin referencia a facturas'),
    ('SS‐CUFE', 'SS‐CUFE'),
    ('SS‐CUDE', 'SS‐CUDE'),
    ('SS‐POS', 'SS‐POS'),
    ('SS‐SNum', 'SS‐SNum'),
    ('SS‐Recaudo', 'SS‐Recaudo'),
    ('SS‐Reporte', 'SS‐Reporte'),
    ('SS‐SinAporte', 'SS‐SinAporte'),
]

OPERATION_TYPE_IN = [
    ('', ''),
    ('10', 'Residente'),
    ('11', 'No Residente'),
]

TYPE_DOCUMENT_REFERENCE = [
    ('', ''),
    ('1001-A ', 'Bienes Propios'),
    ('1002-A ', 'Mercancías ADA'),
    ('1003-A', 'Bienes en dación en Pago'),
    ('1004-B', 'Selección abreviada (Enajenación directa por oferta en sobre cerrado)'),
    ('1005-B', 'Selección abreviada (Enajenación directa por subasta)'),
    ('1006-B', 'Venta por Intermediario idóneo'),
    ('1007-B', 'Venta a través de promotores'),
    ('1008-B', 'Venta a través de banca de inversión'),
    ('1009-B ', 'Venta a través de martillo'),
    ('1010-B ', 'Venta a través de comisionista de bolsa'),
    ('1011-B', 'Venta a Precios fijos'),
    ('1012-B', 'Ventas por Ley 80'),
    ('1015-B ', 'Orden de Pedido'),
    ('1016-B ', 'Soporte de Pago'),
    ('ACL', 'Código de la Actividad Económica Principal'),
    ('LC ', 'Carta de Crédito'),
    ('AAJ', 'Orden de Entrega (remisión)'),
    ('AFO', 'Referencia del beneficiario'),
    ('AGW', 'Número de plan/esquema'),
    ('MSC', 'Informe de consumo de servicios medidos'),
    ('AHJ', 'Referencia para el recaudo'),
    ('TN', 'Referencia de transacción'),
    ('AIJ', 'Referencia de transacción individual de un cliente (Número de Entrada / aceptación de mercancía / Servicio)'),
    ('NCP', 'Número de convenio/promoción/ regalo'),
    ('PED', 'Número del pedido'),
    ('DEV', 'Número de devolución'),
]

CREDIT_NOTE_CONCEPT = [
    ('', ''),
    ('1', 'Devolución de parte de los bienes; no aceptación de partes del servicio '),
    ('2', 'Anulacion de Factura Electronica'),
    ('3', 'Rebaja total aplicada'),
    ('4', 'Descuento total aplicado '),
    ('5', 'Rescisión: nulidad por falta de requisitos'),
    ('6', 'Otros'),
]

CREDIT_NOTE_CONCEPT_OUT = [
    ('', ''),
    ('1', 'Devolución parcial de los bienes y/o no aceptación parcial del servicio'),
    ('2', 'Anulación del documento soporte en adquisiciones efectuadas a sujetos no obligados a expedir factura de venta o documento equivalente'),
    ('3', 'Rebaja o descuento parcial o total'),
    ('4', 'Ajuste de precio'),
    ('5', 'Otros'),
]

DEBIT_NOTE_CONCEPT = [
    ('', ''),
    ('1', 'Intereses'),
    ('2', 'Gastos por cobrar'),
    ('3', 'Cambio del valor'),
    ('4', 'Otro'),
]

ALLOWANCE_CHARGE_REASON = [
    (None, ''),
    ('00', 'Descuento no condicionado'),
    ('01', 'Descuento condicionado'),
]


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'
    pack_quantity = fields.Numeric('Pack Quantity',
        help='Adding in this field the quantity for packet')


class InvoiceCharge(ModelSQL, ModelView):
    "Invoice Charge"
    __name__ = "invoice.charge"
    invoice = fields.Many2One('account.invoice', 'Invoice')
    product = fields.Many2One('product.product', 'Product')
    amount = fields.Numeric('Amount', digits=(16, 2))
    description = fields.Char('Description', required=True)
    charge_concept = fields.Selection(
        ALLOWANCE_CHARGE_REASON, 'Charge Concept')
    charge_percentage = fields.Numeric('Charge Percentage', digits=(2, 2))
    base_amount = fields.Numeric('Base Amount', digits=(16, 2))
    account = fields.Many2One('account.account', "Account")

    @classmethod
    def __setup__(cls):
        super(InvoiceCharge, cls).__setup__()


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'
    electronic_state = fields.Selection(ELECTRONIC_STATES, 'Electronic State',
        states={'invisible': And(Eval('type') != 'out', ~Eval('equivalent_invoice'))}, readonly=True)
    credit_note_concept = fields.Selection('get_credit_cote_concept', 'Credit Note Concept',
        states={
            'invisible':  Not(Eval('invoice_type').in_(['91', '95'])),
            'readonly':  Not(Eval('invoice_type').in_(['91', '95'])),
            })
    debit_note_concept = fields.Selection(DEBIT_NOTE_CONCEPT, 'Debit Note Concept',
        states={
            'invisible': Eval('invoice_type') != '92',
            'readonly':  Eval('invoice_type') != '92',
            })
    cufe = fields.Char('CUFE',
        states={
            'readonly': False,
            # 'invisible': And(Eval('type') != 'out', ~Eval('equivalent_invoice')) ,
            })
    provider_link = fields.Function(fields.Char('Link Mail',
        states={
            'invisible': Not(Eval('cufe'))
            }), 'get_link_dian')
    original_invoice = fields.Many2One('account.invoice', 'Original Invoice', domain=[
        ('type', '=', Eval('type')),
        ('party', '=', Eval('party')), ],
        states={
            'invisible': Not(Eval('invoice_type').in_(['91', '92', '95'])),
            'readonly': Eval('state').in_(['posted', 'paid'])
            }
    )
    operation_type = fields.Selection('get_operation_type', 'Operation Type',
        states={
            'invisible': And(Eval('type') != 'out', ~Eval('equivalent_invoice')),
            'readonly': Eval('state').in_(['posted', 'paid'])
            })
    type_document_reference = fields.Selection(TYPE_DOCUMENT_REFERENCE, 'Type Document Reference',
        states={
            'invisible': Not(Eval('invoice_type').in_(['3', '4'])),
            'readonly': Eval('state').in_(['posted', 'paid']),
            # 'required':  Eval('invoice_type').in_(['3', '4']) and Eval('state') != 'draft',
        })
    type_invoice_reference = fields.Selection(TYPE_INVOICE, 'Type Invoice Reference',
        states={
            'invisible': Not(Eval('invoice_type').in_(['91', '92', '95'])),
            'readonly': Eval('state').in_(['posted', 'paid']),
            'required':  And(
                Or(
                    Eval('invoice_type').in_(['3', '4']),
                    And(
                        Eval('invoice_type').in_(['91', '92', '95']),
                        ~Eval('operation_type').in_(['22', '32'])
                    )
                ),
                Eval('state') != 'draft'),
            })
    number_document_reference = fields.Char('Number Document Reference',
        states={
            'invisible': Not(Eval('invoice_type').in_(['3', '4', '91', '92', '95'])),
            'readonly': Eval('state').in_(['posted', 'paid']),
            'required': And(
                Or(
                    Eval('invoice_type').in_(['3', '4']),
                    And(
                        Eval('invoice_type').in_(['91', '92', '95']),
                        ~Eval('operation_type').in_(['22', '32'])
                    )
                ),
                Eval('state') != 'draft'),
            })
    cufe_document_reference = fields.Char('Cufe Document Reference',
        states={
            'invisible': Not(Eval('invoice_type').in_(['3', '4', '91', '92', '95'])),
            'readonly': Eval('state').in_(['posted', 'paid']),
            'required':  And(
                Or(
                    Eval('invoice_type').in_(['3', '4']),
                    And(
                        Eval('invoice_type').in_(['91', '92', '95']),
                        ~Eval('operation_type').in_(['22', '32'])
                    )
                ),
                Eval('state') != 'draft'),
            },
        )
    date_document_reference = fields.Date('Date Document Reference',
        states={
            'invisible': Not(Eval('invoice_type').in_(['3', '4', '91', '92', '95'])),
            'readonly': Eval('state').in_(['posted', 'paid']),
            'required':  And(
                Or(
                    Eval('invoice_type').in_(['3', '4']),
                    And(
                        Eval('invoice_type').in_(['91', '92', '95']),
                        ~Eval('operation_type').in_(['22', '32'])
                    )
                ),
                Eval('state') != 'draft'),
            },
        )
    payment_code = fields.Function(
        fields.Char('Payments Code'), 'get_payment_code')
    due_date = fields.Function(fields.Date('Due Date'), 'get_due_date')
    electronic_message = fields.Char('Electronic Message',
        states={
            'readonly': False,
            'invisible': And(Eval('type') != 'out', ~Eval('equivalent_invoice')),
            })
    xml_face_ = fields.Binary('XML Face', readonly=True,
        states={
            'invisible': And(Eval('type') != 'out', ~Eval('equivalent_invoice')),
            })
    xml_response_dian_ = fields.Binary('XML Response Dian', readonly=True,
        states={
            'invisible': And(Eval('type') != 'out', ~Eval('equivalent_invoice')),
            })
    qr_image = fields.Function(fields.Binary('QR Image'), 'get_qrcode_image')
    sended_mail = fields.Boolean('Sended Email', depends=['cufe'],
        states={
            'invisible': Eval('type') != 'out',
        })
    charges = fields.One2Many('invoice.charge', 'invoice', string='Charges')
    total_charges = fields.Function(fields.Numeric('Total Charges',
        digits=(16, 2)), 'get_total_charges')
    total_payable_amount = fields.Function(fields.Numeric('Total Payable Amount',
        digits=(16, 2)), 'get_payable_amount')
    file_number = fields.Char('File Number', readonly=True, states={
        'invisible':  Eval('type') != 'out'})
    rules_fail = fields.Text('Rules Fail', states={
            'invisible': Eval('electronic_state') != 'rejected',
            'readonly': True
        }, depends=['electronic_state'])

    type_event = fields.Selection('get_responses', 'Response Type',
        states={
            'invisible': Not(Eval('cufe')),
            'readonly': Not(Eval('cufe'))
        })
    event = fields.Many2One('account.invoice.event_radian', 'Event',
        states={
            'invisible': Not(Eval('cufe')),
            'readonly': Or(Not(Eval('cufe')), Eval('event'))
        },domain=[('invoice','=',Eval('id'))])
    last_event = fields.Selection('get_responses', 'Last Event',
        states={
            'invisible': Not(Eval('cufe')),
            'readonly': True,
        })
    type_event_string = type_event.translated('type_event')

    issue_date = fields.DateTime('Issue Date', states={
        'invisible': And(Eval('type') != 'out', ~Eval('equivalent_invoice')),
        'readonly': True})

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        fields_names = [
            'electronic_state', 'credit_note_concept', 'debit_note_concept',
            'cufe', 'provider_link', 'original_invoice', 'operation_type',
            'type_document_reference', 'number_document_reference',
            'cufe_document_reference', 'date_document_reference',
            'electronic_message', 'xml_face_', 'xml_response_dian_',
            'sended_mail', 'file_number', 'type_event', 'event', 'send_event', 'last_event'
        ]
        cls._check_modify_exclude.update(fields_names)
        cls._buttons.update({
            'submit': {
                'invisible': Or(
                    And(Eval('type') != 'out', ~Eval('equivalent_invoice')),
                    Eval('electronic_state') == 'authorized',
                    Eval('number', None) == None,
                    Eval('authorization', None) == None,
                    Eval('state') != 'validated',
                )},
            'force_response': {
                'invisible': Or(
                    And(Eval('type') != 'out', ~Eval('equivalent_invoice')),
                    Eval('number', None) == None,
                    Eval('authorization', None) == None,
                    Eval('electronic_state') != 'rejected',
                    Eval('state') == 'draft',
                )},
            'send_email': {
                'invisible': Not(Eval('cufe'))
                },
            'send_event': {
                'invisible': Not(Eval('cufe'))
                },
            # 'send_all_event': {
            #     'invisible': Not(Eval('cufe')),
            #     'help': 'Enviar todos los eventos; acuse y aceptacion'
            #     },
            'status_event': {
                'invisible': Not(Eval('cufe'))
                },
            },)

    @classmethod
    def view_attributes(cls):
        return super().view_attributes() + [
            ('/form/notebook/page[@id="invoice_charges"]', 'states', {
                    'invisible': And(Eval('type') != 'out', ~Eval('equivalent_invoice'))
                    }),
            ]

    @staticmethod
    def default_electronic_state():
        return 'none'

    @staticmethod
    def default_operation_type():
        return '10'

    @staticmethod
    def default_credit_note_concept():
        return ''

    @staticmethod
    def default_debit_note_concept():
        return ''

    def on_change_party(self, name=None):
        super(Invoice, self).on_change_party()
        if self.party and self.party.invoice_type and self.type == 'out':
            self.invoice_type = self.party.invoice_type
            self.on_change_invoice_type()

    @fields.depends('invoice_type', 'operation_type')
    def on_change_invoice_type(self):
        super(Invoice, self).on_change_invoice_type()
        invoice_type = self.invoice_type
        if invoice_type == '92':
            self.operation_type = '30'
        elif invoice_type == '91':
            self.operation_type = '20'
        elif invoice_type == '1':
            self.operation_type = '10'

    @fields.depends('type')
    def get_operation_type(self):
        if self.type == 'in':
            return OPERATION_TYPE_IN
        else:
            return OPERATION_TYPE_OUT

    @fields.depends('type')
    def get_credit_cote_concept(self):
        if self.type == 'in':
            return CREDIT_NOTE_CONCEPT_OUT
        else:
            return CREDIT_NOTE_CONCEPT

    @classmethod
    def get_responses(cls):
        return list(RESPONSES.items())

    @classmethod
    @ModelView.button
    def validate_invoice(cls, invoices):
        super(Invoice, cls).validate_invoice(invoices)
        for inv in invoices:
            if inv.type == 'out' and inv.invoice_type not in ['', None, 'C', 'P', 'M']:
                ec_invoice = ElectronicInvoice_2(inv, inv.authorization)
                if ec_invoice.status != 'ok':
                    transaction = Transaction().context
                    if 'exception' in transaction.keys() and transaction['exception']:
                        return ec_invoice.status
                    else:
                        raise UserError('message', ec_invoice.status)

    @classmethod
    @ModelView.button
    def submit(cls, records, raise_exception=True):
        config = Pool().get('account.configuration')(1)
        for invoice in records:
            if not invoice.issue_date:
                tz_ = tz.gettz(invoice.company.timezone)
                now_ = datetime.now(tz=tz_)
                invoice.issue_date = now_
                invoice.save()
            if invoice.invoice_type in ('C', 'P'):
                continue
            if invoice.validate_for_send():
                if invoice.authorization.provider == 'psk':
                    _ = ElectronicInvoicePsk(invoice, invoice.authorization)
                    if config.auto_send_mail and invoice.type == 'out':
                        invoice.save()
                        if invoice.electronic_state == 'authorized':
                            try:
                                cls.send_email([invoice])
                            except:
                                pass
                        elif invoice.electronic_state == 'rejected':
                            try:
                                cls.force_response([invoice])
                            except:
                                pass
                else:
                    invoice.get_message(
                        'El campo proveedor de autorización no ha sido seleccionado')

    @classmethod
    def process_invoice(cls, invoices):
        with Transaction().set_context(_skip_warnings=True):
            for invoice in invoices:
                try:
                    cls.validate_invoice([invoice])
                except:
                    pass
                if invoice.state == 'validated':
                    try:
                        cls.submit(invoices)
                    except:
                        pass
                    try:
                        cls.post(invoices)
                    except:
                        pass

    @classmethod
    @ModelView.button
    def send_event(cls, records):
        pool = Pool()
        EventRadian = pool.get('account.invoice.event_radian')
        config = pool.get('account.configuration')(1)
        for invoice in records:
            if invoice.payment_term.payment_type == '1':
                raise UserError(
                    'Eventos solo disponible para facturas credito')
            result = cls.validate_event(invoice)
            if result:
                attribute_date = 'date_' + TYPE_EVENT[invoice.type_event]
                attribute_number = 'number_' + TYPE_EVENT[invoice.type_event]
                date_event = datetime.now()
                sequence = config.sequence_application_response
                receptionist = config.receptionist_invoice
                if invoice.type_event == '032':
                    receptionist = config.receptionist_good_service
                if invoice.event:
                    number = sequence.get() if not getattr(invoice.event, attribute_number) else getattr(invoice.event, attribute_number)
                    if getattr(invoice.event, attribute_date):
                        date_event = getattr(invoice.event, attribute_date)
                    EventRadian.write([invoice.event], {attribute_date: date_event, attribute_number: number})
                else:
                    number = config.sequence_application_response.get()
                    event, = EventRadian.create([{'invoice': invoice.id, attribute_date: date_event, attribute_number: number}])
                    cls.write([invoice], {'event': event.id})
                cls.send_event_radian(invoice, receptionist)

    @classmethod
    @ModelView.button
    def send_all_event(cls, records):
        list_event = ['030', '032', '033']
        for invoice in records:
            for l in list_event:
                cls.write([invoice], {'type_event': l})
                cls.send_event(invoice)

    @classmethod
    def validate_event(cls, invoice):
        event = invoice.event
        type_ = invoice.type_event

        res = True
        if type_ == '030':
            if event and event.response_acknowledgment:
                res = False
        elif not event and type_ != '030':
            res = False
        elif type_ == '032':
            if not event.response_acknowledgment:
                res = False
        elif type_ not in ['032', '030', '034'] and (not event.response_acknowledgment or not event.receive_good_service):
            res = False
        elif type_ in ['031', '033']:
            if event.response_acceptance or event.response_claim:
                res = False
        elif type_ == '034':
            res = False
            if invoice.event.date_receive_good_service and invoice.type != 'in':
                print('ingresa a ')
                now_ = datetime.now()
                date_ = invoice.event.date_receive_good_service - timedelta(days=3)
                if date_ < now_:
                    res = True
        return res

    @classmethod
    def send_event_radian(cls, invoice, receptionist):
        _ = ApplicationResponse(invoice, receptionist)
        response = getattr(invoice.event, 'response_' + TYPE_EVENT[invoice.type_event])
        if response:
            cls.write([invoice], {'last_event': invoice.type_event})
            try:
                cls.send_email([invoice])
            except Exception as e:
                print(e, 'error send mail')

    @classmethod
    @ModelView.button
    def force_response(cls, records):
        for invoice in records:
            if invoice.authorization.provider != 'psk':
                continue
            _ = ElectronicInvoicePsk.send_request_status_document(invoice)

    @classmethod
    @ModelView.button
    def cancel(cls, invoices):
        for inv in invoices:
            if inv.type == 'out' and inv.invoice_type in ['1', '91', '92'] and inv.cufe not in ['', None]:
                raise UserError(
                    'No se puede cancelar factura porque esta ya tiene cufe')
        super(Invoice, cls).cancel(invoices)

    @classmethod
    @ModelView.button
    def send_email(cls, records):
        for invoice in records:
            if invoice.authorization and invoice.authorization.provider == 'psk' and not invoice.event:
                file_name = invoice.number + '_' + str(invoice.invoice_date)
                invoice.send_emails(file_name)
            elif invoice.event and invoice.type_event:
                invoice.send_emails()

    @classmethod
    @ModelView.button
    def status_event(cls, records):
        for invoice in records:
            result = get_status_events(invoice)
            EventRadian = Pool().get('account.invoice.event_radian')
            if result and invoice.event:
                EventRadian.write([invoice.event], result)
            elif result:
                result['invoice'] = invoice.id
                event, = EventRadian.create([result])
                invoice.event = event.id
                invoice.save()

    @classmethod
    def post(cls, invoices):
        for inv in invoices:
            if inv.invoice_type in ('1', '2', '3', '4', '91', '92') and not inv.cufe:
                raise UserError('La factura no tiene cufe!')
            elif inv.type == 'in' and inv.invoice_type == '1' and not inv.last_event \
                and inv.payment_term.payment_type == '2':
                raise UserError('Debe emitir los eventos para contabilizar')
        super(Invoice, cls).post(invoices)

    def get_move(self):
        move = super(Invoice, self).get_move()
        if self.charges:
            total_charge = self.total_charges
            n_lines = len([line for line in move.lines if line.account == self.account])
            charge = total_charge/n_lines
            for line in move.lines:
                if line.account == self.account:
                    if self.total_amount > 0:
                        line.debit = line.debit - charge
                        line.credit = 0
                    else:
                        line.credit =  line.credit - charge
                        line.debit = 0

            pool = Pool()
            MoveLine = pool.get('account.move.line')
            for charge in self.charges:
                line = MoveLine()
                line.account = charge.account
                if self.account.party_required:
                    line.party = self.party
                line.description = charge.description
                if self.total_amount < 0:
                    line.debit, line.credit = 0, charge.amount
                else:
                    line.debit, line.credit = charge.amount, 0

                line.amount_second_currency = None
                line.second_currency = None
                if hasattr(self, 'operation_center') and self.operation_center:
                    line.operation_center = self.operation_center

                if hasattr(charge, 'analytic_account') and charge.analytic_account:
                    Analytic = pool.get('analytic_account.line')
                    analytic_line = Analytic()
                    analytic_line.debit = line.debit
                    analytic_line.credit = line.credit
                    analytic_line.account = charge.analytic_account
                    analytic_line.date = move.date
                    line.analytic_lines = [analytic_line]
                move_lines = list(move.lines) + [line]
                move.lines = move_lines
        return move

    @classmethod
    def copy(cls, invoices, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['invoice_type'] = None
        default['electronic_state'] = 'none'
        default['cufe'] = None
        default['provider_link'] = None
        default['electronic_message'] = None
        default['file_number'] = None
        default['rules_fail'] = None
        default['event'] = None
        default['xml_face_'] = None
        default['xml_response_dian_'] = None
        default['last_event'] = None
        return super(Invoice, cls).copy(invoices, default=default)

    def get_link_dian(self, name=None):
        if self.type == 'in' and self.cufe and not self.equivalent_invoice:
            return 'https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey=' + self.cufe
        if not self.authorization or self.authorization.kind in ['C', 'P', 'M'] or self.state == 'draft' or not self.number:
            return ''
        link_qr = None
        if self.authorization:
            link_qr = 'https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey='
            if self.authorization.environment == '2':
                link_qr = 'https://catalogo-vpfe-hab.dian.gov.co/document/searchqr?documentkey='
            if self.invoice_type and self.electronic_state != 'authorized':
                try:
                    link_qr = link_qr + str(self.get_cufe())
                except:
                    pass
            elif self.cufe and self.electronic_state == 'authorized':
                link_qr = link_qr + self.cufe
        return link_qr

    def get_file_name(self, doc_type=None, is_event=False):
        if is_event:
            number = getattr(self.event, 'number_' + TYPE_EVENT[self.type_event])
        else:
            if not doc_type:
                doc_type = 'fv'
                if self.invoice_type == '91':
                    doc_type = 'nc'
                elif self.invoice_type == '92':
                    doc_type = 'nd'
                if not self.file_number:
                    if not self.authorization or not self.authorization.sequence_file:
                        raise UserError(
                            'Fallo al enviar, la factura no tiene autorizacion o no se ha definido la secuencia de envio!')
                    else:
                        number = self.authorization.sequence_file.get()
                        self.write([self], {'file_number': number})
                else:
                    number = self.file_number
            else:
                number = self.file_number
        number = hex(int(number)).zfill(8)
        company_number = self.company.party.id_number.zfill(10)
        year = self.create_date.strftime("%y")

        file_name = doc_type + company_number + '000' + year + number
        return file_name

    def check_note(self):
        if self.invoice_type == '91':
            if not self.credit_note_concept:
                raise UserError(
                    'Fallò al enviar, La factura no tiene concepto de nota credito!')
            if self.operation_type == '20' and (not self.type_invoice_reference
                                                or not self.number_document_reference or not self.cufe_document_reference
                                                or not self.date_document_reference):
                raise UserError(
                    'Fallò al enviar, La nota credito no tiene factura original relacionada!')
        elif self.invoice_type == '92':
            if not self.debit_note_concept:
                raise UserError(
                    'Fallò al enviar, La factura no tiene concepto de nota debito!')
            if self.operation_type == '30' and (not self.type_invoice_reference
                                                or not self.number_document_reference or not self.cufe_document_reference
                                                or not self.date_document_reference):
                raise UserError(
                    'Fallò al enviar, La nota debito no tiene factura original relacionada!')

    def get_due_date(self, name):
        if self.invoice_date:
            due_date = (self.invoice_date + timedelta(
                days=self.payment_term.lines[0].relativedeltas[0].days
                )) if self.payment_term.lines[0].relativedeltas else ''
            if not due_date:
                due_date = self.invoice_date
            return due_date
        else:
            return None

    @fields.depends('original_invoice', 'type_invoice_reference', 'number_document_reference', 'date_document_reference')
    def on_change_original_invoice(self):
        if self.original_invoice:
            number = self.original_invoice.number
            if self.original_invoice.equivalent_invoice:
                number = self.original_invoice.number_alternate
            self.type_invoice_reference = self.original_invoice.invoice_type
            self.number_document_reference =  number
            self.cufe_document_reference = self.original_invoice.cufe
            self.date_document_reference = self.original_invoice.invoice_date
        else:
            self.type_invoice_reference = None
            self.number_document_reference = None
            self.cufe_document_reference = None
            self.date_document_reference = None

    def get_total_charges(self, name=None):
        res = 0
        if hasattr(self, 'charges'):
            for charge in self.charges:
                res += charge.amount
        return Decimal(res)

    def get_payment_code(self, name):
        # _sale = self._get_sale_origin()
        # print(_sale, _sale.payments)
        payment_means_code = '10'
        # if _sale and hasattr(_sale, 'payments') and _sale.payments:
        #     payment_means_code = _sale.payments[0].statement.journal.payment_means_code or ''
        # else:
        #     if self.payment_lines:
        #         payment_means_code = self.payment_lines[0].origin.payment_mode.payment_means_code
        return payment_means_code

    def _get_sale_origin(self):
        sale = None
        if self.lines:
            origins = [line.origin for line in self.lines if line.origin]
            if len(origins) > 0 and hasattr(origins[0], 'sale'):
                sale = origins[0].sale
                return sale
            else:
                return None
        else:
            return None

    def validate_for_send(self):
        today = date.today()
        if self.invoice_type == 'C':
            return True
        if not self.authorization:
            raise UserError(
                'Fallo al enviar, la factura no tiene autorizacion!')
        self.check_note()
        if self.electronic_message and self.electronic_message.count('Transacción id') > 0:
            return False
        if self.type == 'out' and self.authorization and \
            self.electronic_state != 'authorized' and self.number and \
                self.invoice_type not in ('C', 'P', 'M'):
            if (today - self.invoice_date).days > 45:
                raise UserError(
                    'Fallo al enviar, la fecha de la factura tiene mas de 10 dias a fecha de hoy!')
            if self.currency.id != self.company.currency.id and \
                self.currency.rates and self.invoice_date != self.currency.rates[0].date:
                raise UserError(
                    f'Fallo al enviar, debe configurar la tasa de cambio a fecha de venta! {self.invoice_date}',)
            return True
        elif self.type == 'in' and self.authorization and \
            self.electronic_state != 'authorized' and self.number:
            return True
        else:
            return False

    def get_message(self, message):
        print(message)
        raise UserError('message', message)

    def get_check_digit(self, id_number):
        id_number_ = id_number.replace(".", "")
        if not id_number_.isdigit():
            return None
        c = 0
        p = len(PRIMOS)-1
        for n in reversed(id_number_):
            c += int(n) * PRIMOS[p]
            p -= 1

        dv = c % 11
        if dv > 1:
            dv = 11 - dv
        return dv

    def get_cuds(self):
        inv_date, issue_time_ = self.get_datetime_local()
        number = self.number_alternate
        company = self.company.party.id_number
        party = self.party.id_number
        payable_amount = str(abs(round(self.get_payable_amount(), 2)))
        untaxed_amount = str(abs(round(self.untaxed_amount, 2)))
        tax = sum(t.amount for t in self.taxes if t.tax.classification_tax == '01')
        tax = str(abs(round(tax, 2))) if tax > 0 else '0.00'
        cufe = self.get_cufe()
        cadena = number + inv_date + issue_time_ + party + company + untaxed_amount + tax + payable_amount + cufe
        print(cadena, 'cad')
        hash_ = hashlib.new("sha384", cadena.encode('utf8'))
        return hash_.hexdigest()

    def get_cufe(self):
        cadena = ''
        auth_ = self.authorization
        inv_date, issue_time_ = self.get_datetime_local(self.issue_date)
        invoice_type = self.invoice_type
        # print(invoice_type, 'invoice_type')
        if auth_ and auth_.environment:
            untaxed_amount = round(self.untaxed_amount, 2)
            number = self.number
            if self.type == 'in':
                number = self.number_alternate
            cadena = number + inv_date + \
                issue_time_ + str(abs(untaxed_amount))
            # print('number ', self.number)
            # print('inv_date ', inv_date)
            # print('issue_time_ ', issue_time_)
            # print('untaxed_amount ', str(abs(untaxed_amount)))

            taxes_cufe = {
                '01': 0,
                '04': 0,
                '03': 0,
            }
            for tl in self.taxes:
                if invoice_type in ['1', '2', '3', '4'] and tl.amount <= 0 or tl.tax.classification_tax not in taxes_cufe.keys():
                    continue

                taxes_cufe[tl.tax.classification_tax] += abs(tl.amount)

            for k, v in taxes_cufe.items():
                # print('Tax ', k, str(round(v, 2)) if v else '0.00')
                if self.type == 'in' and k != '01':
                    continue
                cadena += k + (str(round(v, 2)) if v else '0.00')

            payable_amount = round(self.get_payable_amount(), 2)
            if invoice_type in ['1', '2', '3', '4']:
                cadena = cadena + str(abs(payable_amount)) + self.company.party.id_number \
                            + self.party.id_number + auth_.technical_key + auth_.environment
            elif invoice_type in ['91', '92']:
                cadena = cadena + str(abs(payable_amount)) + self.company.party.id_number \
                            + self.party.id_number + auth_.pin_software + auth_.environment
            elif invoice_type in ['05', '95'] and self.type == 'in':
                cadena = cadena + str(abs(payable_amount)) + self.party.id_number \
                            + self.company.party.id_number + auth_.pin_software + auth_.environment
            # print('payable_amount ', str(abs(payable_amount)))
            # print('inv_date, issue_time_ ', inv_date, issue_time_)
            # print('company ', self.company.party.id_number)
            # print('party ', self.party.id_number)
            # print('pin_software ', auth_.pin_software)
            # print('auth_.environment ', auth_.environment)
            # print('auth_.technical_key ', auth_.technical_key)
            print('CUDE .....', cadena)
            hash_ = hashlib.new("sha384", cadena.encode('utf8'))
            return hash_.hexdigest()
        # else:
        #     return self.get_message('Error al generar Cufe')

    def get_cude(self):
        cadena = None
        event = self.event
        if event:
            number = getattr(event, 'number_' + TYPE_EVENT[self.type_event])
            date_time = getattr(event, 'date_' + TYPE_EVENT[self.type_event])
            issue_date, issue_time = self.get_datetime_local(date_time=date_time)
            sender_party = self.company.party.id_number
            receiver_party = self.party.id_number
            event_code = self.type_event
            type_code = '0'+self.invoice_type if len(self.invoice_type) == 1 else self.invoice_type
            reference = self.reference
            if self.type_event == '034':
                receiver_party = '800197268'
                reference = self.number
            pin_software = self.company.pin_software
            cadena = number + issue_date + issue_time + sender_party + receiver_party + event_code + reference + type_code + pin_software
            hash_ = hashlib.new("sha384", cadena.encode('utf8'))
            return hash_.hexdigest()

    def get_security_code(self):
        number = self.number
        if self.type == 'in' and self.equivalent_invoice:
            number = self.number_alternate
        elif self.type_event:
            number = getattr(self.event, 'number_' + TYPE_EVENT[self.type_event])
        cadena = self.company.software_id + self.company.pin_software + number
        if cadena:
            hash_ = hashlib.new("sha384", cadena.encode('utf8'))
            return hash_.hexdigest()
        return self.get_message('Error al generar Código de Seguridad del Software')

    def get_datetime_local(self, date_time=None):
        if not date_time:
            date_time = self.create_date
        create_date = self.company.convert_timezone(date_time, company_id=self.company.id)
        tzinfo = create_date.replace(microsecond=0)
        return str(tzinfo).split(' ')

    def get_taxes_face_1(self):
        amount_ = sum([
            taxline.amount for taxline in self.taxes
            if taxline.tax.classification_tax == '01'
            ])
        return amount_

    def get_qrcode_image(self, name=None, ):
        qr_code = self.provider_link
        if qr_code:
            iva = 0
            otro_imp = 0
            for tax in self.taxes:
                classification = tax.tax.classification_tax
                if classification == '01':
                    iva += tax.amount
                elif classification in [ '02', '03', '04']:
                    otro_imp += tax.amount
            issue_date, issue_time = self.get_datetime_local(self.issue_date)
            qr_data = f'''
                NumFac: {self.number}
                FecFac: {str(issue_date)}
                HorFac: {str(issue_time)}
                NitFac: {self.company.party.id_number}
                DocAdq: {self.party.id_number}
                ValFac: {str(round(self.untaxed_amount, 2))}
                ValIva: {str(round(iva,2))}
                ValOtroIm: {str(round(otro_imp,2))}
                ValTolFac: {str(round(self.total_amount, 2))}
                CUFE: {self.cufe}
                {qr_code}'''
        
            img = qrcode.make(qr_data)
            buffered = BytesIO()
            img.save(buffered, 'PNG')
            img_str = buffered.getvalue()
            return [img_str, 'image/png']
        else:
            return None

    def create_zip(self, files_to_compress=[]):
        ''' files_to_compress list dics
            files_to_compress = [{'file_name': 'xxx', content: b'bytes'}]
            content in bytes or str
            and return buffer
        '''
        if files_to_compress:
            memory_zip = BytesIO()
            with zipfile.ZipFile(memory_zip, 'x', allowZip64=True) as zf:
                for file in files_to_compress:
                    zf.writestr(file['file_name'],  file['content'],
                                compress_type=zipfile.ZIP_DEFLATED)
                zf.close()
            return memory_zip.getbuffer()

    def get_data_response_dian(self, xml_response):
        if xml_response:
            xml = xmltodict.parse(xml_response)
            issue_date = xml['ApplicationResponse']['cbc:IssueDate']
            issue_time = xml['ApplicationResponse']['cbc:IssueTime']
            return issue_date, issue_time


    def send_emails(self, file_name='document'):
        pool = Pool()
        config = pool.get('account.configuration')(1)
        Template = pool.get('email.template')
        is_event = False
        if self.event and self.type_event:
            is_event = True
            file_name = self.get_file_name(doc_type='ad', is_event=is_event)
            number  = getattr(self.event, 'number_' + TYPE_EVENT[self.type_event])
            subject = 'Evento'+';'+ self.reference + ';'+ self.company.party.id_number + ';' \
                + self.company.party.name + ';' + number + ';' \
                + self.type_event
            xml_customer = make_customer_response(self)
            template = config.event_mail_template
        else:
            template = config.electronic_invoice_mail_template
            type_doc = self.invoice_type if self.invoice_type != '1' else '01'
            xml_customer = make_customer(self)
            file_name = self.get_file_name()
            subject = self.company.party.id_number + ';' \
                + self.company.party.name + ';' + self.number + ';' \
                + type_doc + ';' + self.company.party.name

        if template:
            files_to_compress = []
            if template.report:
                ext, data, _, file_name_ = template.render_report(self)
                template.report = None

                files_to_compress.append(
                    {
                        'file_name': file_name + '.' + ext,
                        'content': data
                    })
            files_to_compress.append({
                'file_name': file_name + '.xml',
                'content': xml_customer
            })
            file_n_zip = self.get_file_name(doc_type='z', is_event=is_event)
            zip_file = self.create_zip(files_to_compress)
            attach_dict = {
                'attachment': zip_file,
                'file_name': file_n_zip,
                'extension': 'zip',
            }

            template.subject = subject
            # email = get_email_reception(self)
            emails = []
            if not emails:
                emails = list(set(c.value for c in self.party.contact_mechanisms if c.type == 'email' and c.invoice))
                if not emails:
                    emails = [self.party.email]
            attachments = [attach_dict]
            if self.party.second_currency:
                ext, data, _, file_name_ = self.render_report_second_currency_invoice()
                attachments.append({
                    'attachment': data,
                    'file_name': 'invoice_' + str(self.number),
                    'extension': ext
                })

            response = Template.send(template, self, emails,
                                     attach=True, attachments=attachments)
            if response.status_code == 202:
                self.write([self], {'sended_mail': True})
        else:
            self.get_message(
                'No se ha definido una plantilla para el envío del correo.')

    def create_file(self, file, file_name='_'):
        file_ = open(file_name, "w")
        file_.write(file)
        file_.close()

    def render_report_second_currency_invoice(self):
        '''Render the report and returns a tuple of data
        '''
        Report = Pool().get('report.invoice_second_currency', type='report')
        report = Report.execute([self.id], {'id': self.id})
        ext, data, filename, file_name = report
        return ext, data, filename, file_name

    def test_print(self, xml_invoice, _type=''):
        file_name = ''
        if _type:
            if _type == 'F':
                file_name = 'fe_' + self.authorization.provider + '.xml'
            elif _type == 'CN':
                file_name = 'cn_' + self.authorization.provider + '.xml'
            elif _type == 'E':
                file_name = 'e_' + self.authorization.provider + '.xml'
            elif _type == 'EN':
                file_name = 'en_' + self.authorization.provider + '.xml'
            elif _type == 'DN':
                file_name = 'dn_' + self.authorization.provider + '.xml'
            else:
                file_name = 'event_' + '.xml'
        file_ = open("/home/" + USER + "/" + file_name, "w")
        file_.write(xml_invoice.decode('utf-8'))
        file_.close()

    def get_payable_amount(self):
        tax_amount = sum([
            taxline.amount for taxline in self.taxes
            if taxline.tax.classification_tax != 'NA'
        ])
        total_charges = self.get_total_charges()
        res = Decimal(
            abs(self.untaxed_amount)) + abs(tax_amount) - abs(total_charges)

        return res

    def get_attribute_event(self, attr=None):
        event = self.event
        if attr == 'date':
            date_time = getattr(event, 'date_' +TYPE_EVENT[self.type_event])
            issue_date, issue_time = self.get_datetime_local(date_time)
            res = issue_date + ' ' + issue_time
        elif attr == 'number':
            res = getattr(event, 'number_' +TYPE_EVENT[self.type_event])
        return res


class InvoiceFaceReport(Report):
    __name__ = 'electronic_invoice_co.invoice_face'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Company = Pool().get('company.company')
        report_context['company'] = Company(Transaction().context['company'])
        return report_context


class InvoiceSecondCurrencyReport(Report):
    __name__ = 'report.invoice_second_currency'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Currency = pool.get('currency.currency')
        comp_currency = Currency.compute

        def compute_currency(invoice, amount):
            with Transaction().set_context(date=invoice.currency_date):
                try:
                    amount = comp_currency(
                        invoice.company.currency,
                        Decimal(amount), invoice.party.second_currency, round=False)
                except:
                    pass
                return amount

        report_context['company'] = Company(Transaction().context['company'])
        report_context['compute_currency'] = compute_currency
        return report_context


class EventReport(Report):
    __name__ = 'electronic_invoice_co.event_invoice.report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        Company = Pool().get('company.company')
        report_context['company'] = Company(Transaction().context['company'])
        return report_context


class Template(metaclass=PoolMeta):
    __name__ = 'email.template'

    def render_report(self, record):
        ext, data, filename, file_name = super(Template, self).render_report(record)
        timestamp_ = Transaction().timestamp
        if timestamp_:
            ids = list(timestamp_.keys())
            if len(ids) > 0 and 'account.invoice' in ids[0]:
                id_ = ids[0].replace('account.invoice,', '')
                invoices = Pool().get('account.invoice').search(
                    [('id', '=', id_), ])
                if invoices:
                    file_name = invoices[0].number + \
                        '_' + str(invoices[0].invoice_date)
        return ext, data, filename, file_name


class EventRadian(ModelSQL, ModelView):
    "Event Radian"
    __name__ = 'account.invoice.event_radian'

    # _rec_name = 'invoice'
    invoice = fields.Many2One('account.invoice', 'Invoice', ondelete='CASCADE')
    date_acknowledgment = fields.DateTime('Date Acknowledgment')
    number_acknowledgment = fields.Char('Number Acknowledgment',states={'readonly':True})
    response_acknowledgment =fields.Binary('Response Acknowledgment', states={'readonly':True})
    acknowledgment = fields.Binary('Acknowledgment', states={'readonly':True})
    date_acceptance = fields.DateTime('Date Acceptance')
    number_acceptance = fields.Char('Number Acceptance', states={'readonly':True})
    acceptance = fields.Binary('Acceptance', states={'readonly':True})
    response_acceptance = fields.Binary('Response Acceptance', states={'readonly':True})
    date_tacit_acceptance = fields.DateTime('Date Tacit Acceptance')
    number_tacit_acceptance = fields.Char('Number Tacit Acceptance', states={'readonly':True})
    tacit_acceptance = fields.Binary('Tacit Acceptance', states={'readonly':True})
    response_tacit_acceptance = fields.Binary('Response Tacit Acceptance', states={'readonly':True})
    date_claim = fields.DateTime('Date Claim')
    number_claim = fields.Char('Number Claim', states={'readonly':True})
    claim = fields.Binary('Claim', states={'readonly':True})
    response_claim = fields.Binary('Response Claim', states={'readonly':True})
    date_receive_good_service = fields.DateTime('Date Receive Good & Service')
    number_receive_good_service = fields.Char('Number Receive Good & Service', states={'readonly':True})
    receive_good_service = fields.Binary('Receive Good And Service', states={'readonly':True })
    response_receive_good_service = fields.Binary('Receive Good And Service', states={'readonly':True })
    concept = fields.Selection(CONCEPTS_CLAIM, 'Concept Claim')
    comment = fields.Text('Comment')

    def get_rec_name(self, name):
        if self.invoice:
            return self.invoice.rec_name

    @classmethod
    def search_rec_name(cls, name, clause):
        return [('invoice',) + tuple(clause[1:])]

    # @fields.depends('invoice', 'concept')
    # def on_change_invoice(self):
    #     print('ingresa a esta funcion', self.invoice.concept_claim)
    #     if self.invoice and self.invoice.concept_claim:
    #         self.concept = self.invoice.concept_claim
