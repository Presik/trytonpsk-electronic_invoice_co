from functools import partial
from decimal import Decimal
from datetime import timedelta
from lxml import etree, builder

from .maker_response import (ext, cbc, cac, ds, sts, root, attr_qname, attr_xsd)
from .maker_customer import (AttachedDocument, ext_customer, cbc_customer, cac_customer, ds_customer, xades_customer, xades141_customer)

TYPE_PERSON = {
    'persona_juridica': '1',
    'persona_natural': '2',
}

MESSAGES = {
    'software_id': 'Falta ID del software Facturador',
    'company_id': 'Falta numero NIT de la empresa',
}

RESPONSES = {
    '': '',
    '030': 'Acuse de recibo de Factura Electrónica de Venta',
    '031': 'Reclamo de la Factura Electrónica de Venta',
    '032': 'Recibo del bien y/o prestación del servicio',
    '033': 'Aceptación expresa',
    '034': 'Aceptación Tácita'
}

TYPE_EVENT = {
    '': '',
    '030': 'acknowledgment',
    '031': 'claim',
    '032': 'receive_good_service',
    '033': 'acceptance',
    '034': 'tacit_acceptance'
}

CONCEPTS_CLAIM = {
        '01': 'Documento con inconsistencias',
        '02': 'Mercancía no entregada totalmente',
        '03': 'Mercancía no entregada parcialmente',
        '04': 'Servicio no prestado',
    }


class ElectronicResponse(object):

    def __init__(self, invoice, receptionist):
        self.type = invoice.type
        self.invoice = invoice
        self.receptionist = receptionist
        self.reference = invoice.reference
        self.number = getattr(self.invoice.event, 'number_'+ TYPE_EVENT[self.invoice.type_event])
        # Company information --------------------------------------------------
        self.company_id = invoice.company.party.id_number
        self.company_type_id = invoice.company.party.type_document or ''
        self.company_name = invoice.company.party.name
        self.company_phone = invoice.company.party.phone or invoice.company.party.mobile
        self.company_city = invoice.company.party.city_name
        self.company_city_code = invoice.company.party.department_code+invoice.company.party.city_code
        self.company_department_code = invoice.company.party.department_code
        self.company_check_digit = invoice.company.party.check_digit
        self.company_country_code = invoice.company.party.get_country_iso(invoice.company.party.country_code, 'code')
        self.company_country_name = invoice.company.party.get_country_iso(invoice.company.party.country_code, 'name')
        self.company_department = invoice.company.party.department_name
        self.company_email = invoice.company.party.email
        self.company_tributes = invoice.company.party.party_tributes or []
        self.company_type_person = TYPE_PERSON[invoice.company.party.type_person]
        # Party information------------------------------------------------------------------------------------------
        self.party_department_code = invoice.party.department_code or ''
        self.party_city_code = invoice.party.city_code or ''
        self.party_city_code = self.party_department_code+self.party_city_code
        self.party_name = invoice.party.name or invoice.party.commercial_name
        self.party_id = invoice.party.id_number
        self.party_check_digit = invoice.party.check_digit
        self.party_type_id = invoice.party.type_document or ''
        self.invoice_type_code = invoice.invoice_type if invoice.invoice_type else ''
        if self.invoice_type_code in ['1', '2', '3', '4']:
            self.invoice_type_code = '0' + invoice.invoice_type
        self.party_address = invoice.party.street
        self.party_country_code = invoice.party.get_country_iso(invoice.party.country_code, 'code')  # 'CO'
        self.party_country_name = invoice.party.get_country_iso(invoice.party.country_code, 'name')  # 'CO'
        self.party_department = invoice.party.department_name if self.party_country_code == 'CO' else ''
        self.party_city = invoice.party.city_name if self.party_country_code == 'CO' else ''
        self.party_phone = invoice.party.phone or invoice.party.mobile
        self.party_email = invoice.party.email
        self.party_tributes = invoice.party.party_tributes or []
        self.party_type_person = TYPE_PERSON[invoice.party.type_person]
        self.reference = invoice.reference or ''
        self.notes = invoice.comment or ' '
        self.due_date = str(self.invoice.due_date)
        date_time = getattr(self.invoice.event, 'date_' + TYPE_EVENT[self.invoice.type_event])
        self.issue_date, self.issue_time = invoice.get_datetime_local(date_time=date_time)
        self.status = 'ok'
        self.software_id = invoice.company.software_id
        self.software_provider_id = self.company_id
        self.check_digit_provider = self.company_check_digit
        self.operation_type = invoice.operation_type
        self.dian_id = '800197268'
        self.dian_name = 'Unidad Administrativa Especial Dirección de Impuestos y Aduanas Nacionales'
        self.dian_type_id = '31'
        self.dian_type_person = '1'

    def _get_head_(self):
        software_id_ = self._get_software_info()
        head = root.ApplicationResponse()
        note = ''
        if self.invoice.type_event == '034':
            note  = f'''Manifiesto bajo la gravedad de juramento que transcurridos 3 días hábiles 
                    contados desde la creación del Recibo de bienes y servicios {self.invoice.number} con CUDE {self.invoice.cufe},
                    el adquirente {self.invoice.party.name} identificado con NIT {self.invoice.party.id_number} no manifestó expresamente
                    la aceptación o rechazo de la referida factura, ni reclamó en contra de su contenido.'''

        elements = (ext.UBLExtensions(
                ext.UBLExtension(
                    ext.ExtensionContent(
                        sts.DianExtensions(
                            sts.InvoiceSource(
                                cbc.IdentificationCode('CO', listAgencyID="6", listAgencyName="United Nations Economic Commission for Europe", listSchemeURI="urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1")
                            ),
                            sts.SoftwareProvider(
                                sts.ProviderID(
                                    self.software_provider_id,
                                    schemeAgencyID="195",
                                    schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
                                    schemeID=str(self.check_digit_provider),
                                    schemeName="31"),
                                sts.SoftwareID(
                                    self.software_id,
                                    schemeAgencyID="195",
                                    schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            ),
                            sts.SoftwareSecurityCode(self.invoice.get_security_code(), schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)"),
                            sts.AuthorizationProvider(
                                sts.AuthorizationProviderID(
                                    '800197268',
                                    schemeAgencyID="195",
                                    schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
                                    schemeID="4",
                                    schemeName="31"),
                            ),
                            sts.QRCode(self.invoice.get_link_dian()),
                        ),
                    ),
                ),
                ext.UBLExtension(
                    ext.ExtensionContent(),
                ),
            ),
            cbc.UBLVersionID('UBL 2.1'),
            cbc.CustomizationID('1'),
            cbc.ProfileID('DIAN 2.1: ApplicationResponse de la Factura Electrónica de Venta'),
            cbc.ProfileExecutionID('1'),
            cbc.ID(self.number),
            cbc.UUID(self.invoice.get_cude(), schemeID="2", schemeName='CUDE-SHA384'),
            cbc.IssueDate(self.issue_date),
            cbc.IssueTime(self.issue_time),
            cbc.Note(note))

        for e in elements:
            head.append(e)
        return head

    def _get_software_info(self):
        software_id_ = sts.SoftwareID()
        if self.software_id:
            software_id_ = sts.SoftwareID(
                self.software_id,
                schemeAgencyID="195",
                schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)")
        return software_id_

    def get_party(self, context):
        type_event = self.invoice.type_event
        if context == 'party' and type_event != '034' or context == 'dian' and type_event == '034':
            party_ = cac.ReceiverParty()
        else:
            party_ = cac.SenderParty()
        dv = self.invoice.get_check_digit(getattr(self, context+'_id'))

        party_tax_scheme = cac.PartyTaxScheme(
            cbc.RegistrationName(getattr(self, context+'_name')),
            cbc.CompanyID(
                    str(getattr(self, context+'_id')),
                    schemeAgencyID="195",
                    schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
                    schemeID=str(dv) if dv is not None else '',
                    schemeName=getattr(self, context+'_type_id'),
                    schemeVersionID=getattr(self, context+'_type_person'),
                ))

        if context == 'dian':
            party_tax_scheme.append(
                cac.TaxScheme(
                    cbc.ID('01'),
                    cbc.Name('IVA'),
                    ),
                )
        elif not getattr(self, context+'_tributes'):
            party_tax_scheme.append(
                cac.TaxScheme(
                    cbc.ID('ZZ'),
                    cbc.Name('No aplica'),
                    ),
                )
        else:
            for tribute in getattr(self, context+'_tributes'):
                party_tax_scheme.append(
                    cac.TaxScheme(
                        cbc.ID(tribute.code),
                        cbc.Name(tribute.name),
                        ),
                )
        party_.append(party_tax_scheme)

        return party_

    def get_document_response(self):

        reference = self.reference
        code = cbc.ResponseCode(self.invoice.type_event)
        if self.invoice.type_event == '031' and self.invoice.event.concept:
            concept = self.invoice.event.concept
            code.set('name', CONCEPTS_CLAIM[concept])
            code.set('listID', concept)
        elif self.invoice.type_event == '034':
            reference = self.invoice.number
        element = cac.DocumentResponse(
            cac.Response(
                code,
                cbc.Description(RESPONSES[self.invoice.type_event])
            ),
            cac.DocumentReference(
                cbc.ID(reference),
                cbc.UUID(
                    self.invoice.cufe,
                    schemeName='CUFE-SHA384'
                    ),
                cbc.DocumentTypeCode(self.invoice_type_code),
            ),
        )
        if self.invoice.type_event in ['030', '032'] and self.receptionist:
            employee = self.receptionist
            element.append(
                cac.IssuerParty(
                    cac.Person(
                        cbc.ID(
                            employee.party.id_number,
                            schemeID="",
                            schemeName=employee.party.type_document,
                        ),
                        cbc.FirstName(employee.party.first_name),
                        cbc.FamilyName(employee.party.first_family_name),
                        cbc.JobTitle(employee.position.name if employee.position else ''),
                        cbc.OrganizationDepartment(employee.department.name if employee.department else ''),
                    )
                )
            )
        return element

    def make(self):
        xml_app_response = self._get_head_()
        if self.invoice.type_event != '034':
            xml_app_response.append(self.get_party('company'))
            xml_app_response.append(self.get_party('party'))
        else:
            xml_app_response.append(self.get_party('company'))
            xml_app_response.append(self.get_party('dian'))
        xml_app_response.append(self.get_document_response())
        # xml_app_response.attrib[attr_qname] = attr_xsd
        exml = etree.tostring(xml_app_response, encoding='UTF-8', pretty_print=True, xml_declaration=True)
        return exml


def make_customer_response(invoice):
    party_obligation_tax = [of.code.replace(" ", "") for of in invoice.company.party.party_obligation_tax]
    if not party_obligation_tax:
        cadena = 'ZZ'
    else:
        cadena = ";".join(party_obligation_tax)
    number = getattr(invoice.event, 'number_' + TYPE_EVENT[invoice.type_event])
    issue_date, issue_time = invoice.get_datetime_local()
    party_company = invoice.company.party
    _party = invoice.party
    response_xml = getattr(invoice.event, 'response_' + TYPE_EVENT[invoice.type_event])
    document_xml = getattr(invoice.event, TYPE_EVENT[invoice.type_event])
    date_validation, time_validation = invoice.get_data_response_dian(response_xml)
    attached_document = AttachedDocument.AttachedDocument(
        cbc_customer.UBLVersionID('UBL 2.1'),
        cbc_customer.CustomizationID('Documentos adjuntos'),
        cbc_customer.ProfileID('Factura Electrónica de Venta'),
        cbc_customer.ProfileExecutionID('1'),
        cbc_customer.ID(str(invoice.id)),
        cbc_customer.IssueDate(issue_date),
        cbc_customer.IssueTime(issue_time),
        cbc_customer.DocumentType('Contenedor de Factura Electrónica'),
        cbc_customer.ParentDocumentID(number),
    )

    party_tax_scheme_company = cac_customer.PartyTaxScheme(
        cbc_customer.RegistrationName(party_company.name),
        cbc_customer.CompanyID(
            str(party_company.id_number),
            schemeAgencyID="195",
            schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
            schemeID=str(invoice.get_check_digit(party_company.id_number)),
            schemeName=party_company.type_document,
            ),
        cbc_customer.TaxLevelCode(cadena.replace(" ", ""), listName=party_company.fiscal_regimen),
    )
    if not invoice.company.party.party_tributes:
        party_tax_scheme_company.append(
            cac_customer.TaxScheme(
                cbc_customer.ID('ZZ'),
                cbc_customer.Name('No aplica'),
                ),
        )
    for tribute in invoice.company.party.party_tributes:
        party_tax_scheme_company.append(
            cac_customer.TaxScheme(
                cbc_customer.ID(tribute.code),
                cbc_customer.Name(tribute.name),
                ),
        )
    sender_party = cac_customer.SenderParty()
    sender_party.append(party_tax_scheme_company)
    #-------------------------------- ReceiverParty -----------------------
    party_tax_scheme_customer = cac_customer.PartyTaxScheme(
        cbc_customer.RegistrationName(_party.name),
        cbc_customer.CompanyID(_party.id_number, schemeAgencyID="195", schemeAgencyName="CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)",
            schemeID=str(invoice.get_check_digit(_party.id_number)), schemeName=_party.type_document),
        cbc_customer.TaxLevelCode(cadena.replace(" ", ""), listName=invoice.party.fiscal_regimen),
    )
    if not invoice.company.party.party_tributes:
        party_tax_scheme_customer.append(
            cac_customer.TaxScheme(
                cbc_customer.ID('ZZ'),
                cbc_customer.Name('No aplica'),
                ),
            )
    for tribute in invoice.company.party.party_tributes:
        party_tax_scheme_customer.append(
            cac_customer.TaxScheme(
                cbc_customer.ID(tribute.code),
                cbc_customer.Name(tribute.name),
                ),
        )
    receiver_party = cac_customer.ReceiverParty()
    receiver_party.append(party_tax_scheme_customer)

    attached_document.append(sender_party)
    attached_document.append(receiver_party)
    attachment = cac_customer.Attachment(
        cac_customer.ExternalReference(
            cbc_customer.MimeCode('text/xml'),
            cbc_customer.EncodingCode('UTF-8'),
            cbc_customer.Description(etree.CDATA(document_xml.decode('utf8'))),
        )
    )
    attached_document.append(attachment)

    parent_document_line = cac_customer.ParentDocumentLineReference(
        cbc_customer.LineID('1'),
        cac_customer.DocumentReference(
            cbc_customer.ID(invoice.number),
            cbc_customer.UUID(invoice.cufe, schemeName="CUFE-SHA384"),
            cbc_customer.IssueDate(issue_date),
            cbc_customer.DocumentType("ApplicationResponse"),
            cac_customer.Attachment(
                cac_customer.ExternalReference(
                    cbc_customer.MimeCode('text/xml'),
                    cbc_customer.EncodingCode('UTF-8'),
                    cbc_customer.Description(etree.CDATA(response_xml.decode('utf8'))),
                ),
            ),
            cac_customer.ResultOfVerification(
                cbc_customer.ValidatorID('Unidad Especial Dirección de Impuestos y Aduanas Nacionales'),
                cbc_customer.ValidationResultCode('02'),
                cbc_customer.ValidationDate(date_validation),
                cbc_customer.ValidationTime(time_validation),
            ),
        ),
    )
    attached_document.append(parent_document_line)
    xml_customer = etree.tostring(attached_document, encoding='UTF-8', pretty_print=True, xml_declaration=True)

    return str(xml_customer, 'utf-8')