from trytond.pool import Pool
from . import invoice
from . import company
from . import configuration
from . import invoice_authorization
from . import party
from . import payment_term
from . import voucher
from . import electronic_document


def register():
    Pool.register(
        invoice.EventRadian,
        party.Configuration,
        company.Company,
        configuration.ConfigurationDefaultAccount,
        configuration.AccountConfiguration,
        configuration.CreateCompanyElectronicInvoiceStart,
        configuration.CreateCompanyElectronicInvoiceDone,
        invoice.Invoice,
        invoice.InvoiceLine,
        invoice.Template,
        invoice_authorization.InvoiceAuthorization,
        invoice_authorization.DigitalCertificate,
        party.PartyObligationFiscal,
        party.PartyObligationTax,
        party.ClassificationTax,
        party.PartyTributeTax,
        party.Party,
        payment_term.PaymentTerm,
        voucher.VoucherPayMode,
        invoice.InvoiceCharge,
        electronic_document.Context,
        electronic_document.ElectronicDocument,
        module='electronic_invoice_co', type_='model')
    Pool.register(
        configuration.CreateCompanyElectronicInvoice,
        invoice_authorization.UpdateTechnicalKey,
        module='electronic_invoice_co', type_='wizard')
    Pool.register(
        invoice.InvoiceFaceReport,
        invoice.EventReport,
        invoice.InvoiceSecondCurrencyReport,
        module='electronic_invoice_co', type_='report')
