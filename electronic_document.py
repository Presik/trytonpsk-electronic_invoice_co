try:
    import pygal
except ImportError:
    pygal = None
from sql import Null, Literal, Table, CombiningQuery
from sql.aggregate import Sum, Count
from sql.functions import CurrentTimestamp, DateTrunc, ToChar
from sql.operators import Concat
from trytond.model import fields, ModelView, ModelSQL
from trytond.pyson import Eval, If
from trytond.transaction import Transaction
from trytond.pool import Pool
from dateutil.relativedelta import relativedelta
from itertools import tee, zip_longest
from trytond.i18n import lazy_gettext


def pairwise(iterable):
    a, b = tee(iterable)
    next(b)
    return zip_longest(a, b)


class UnionALL(CombiningQuery):
    __slots__ = ()
    _operator = 'UNION ALL'


class Abstract(ModelSQL):

    company = fields.Many2One(
        'company.company', lazy_gettext("electronic_invoice_co.msg_electronic_reporting_company"))
    invoice = fields.Integer(lazy_gettext("electronic_invoice_co.msg_electronic_reporting_invoice"))
    equivalent_invoice = fields.Integer(lazy_gettext("electronic_invoice_co.msg_electronic_reporting_equivalent_invoice"))
    event = fields.Integer(lazy_gettext("electronic_invoice_co.msg_electronic_reporting_event"))
    payroll = fields.Integer(lazy_gettext("electronic_invoice_co.msg_electronic_reporting_payroll"))

    @classmethod
    def get_union_queries(cls, context):
        queries = [
            *cls.get_invoice_query(context),
            cls.get_payroll_query(context),
            *cls.get_event_query(context)
        ]
        return UnionALL(*queries)

    @classmethod
    def get_invoice_query(cls, context):
        invoice = Table("account_invoice")
        type_doc = ['out', 'in']

        queries = []
        for t in type_doc:
            query = invoice.select(
                DateTrunc(context.get('period'), invoice.invoice_date).as_("period"),
                Concat('invoice_', t).as_("type_doc"),
                Count(invoice.id).as_("records"),
                where=(invoice.electronic_state.__eq__('authorized') &
                    invoice.type.__eq__(t) &
                    invoice.invoice_date.__ge__(context.get('from_date')) &
                    invoice.invoice_date.__le__(context.get('to_date'))),
                group_by=(DateTrunc(context.get('period'), invoice.invoice_date))
            )
            queries.append(query)
        return queries

    @classmethod
    def get_payroll_query(cls, context):
        payroll = Table("staff_payroll_electronic")
        query = payroll.select(
            DateTrunc(context.get('period'), payroll.start).as_("period"),
            Literal('payroll').as_("type_doc"),
            Count(payroll.id).as_("records"),
            where=((payroll.electronic_state == 'authorized') &
                payroll.start.__ge__(context.get('from_date')) &
                payroll.start.__le__(context.get('to_date'))),
            group_by=(DateTrunc(context.get('period'), payroll.start))
        )
        return query

    @classmethod
    def get_event_query(cls, context):
        type_events = ["acceptance", "receive_good_service",  "acknowledgment", "tacit_acceptance", "claim"]
        events = []
        event = Table("account_invoice_event_radian")
        for t in type_events:
            col_date = 'date_' + t
            col_response = 'response_' + t
            query = event.select(
                DateTrunc(context.get('period'), getattr(event, col_date)).cast("TIMESTAMP WITH TIME ZONE") .as_("period"),
                Literal('event').as_("type_doc"),
                Count(event.id).as_("records"),
                where=((getattr(event, col_response) != Null) & (getattr(event, t) != Null)&
                getattr(event, col_date).__ge__(context.get('from_date')) & getattr(event, col_date).__le__(context.get('to_date'))),
                group_by=(DateTrunc(context.get('period'), getattr(event, col_date)))
            )
            events.append(query)
        return events

    @classmethod
    def table_query(cls):
        context = Transaction().context
        union_query  = cls.get_union_queries(context)

        col_equivalent_invoice = Sum(union_query.records, filter_=(union_query.type_doc == 'invoice_in')).as_("equivalent_invoice")
        col_invoice = Sum(union_query.records, filter_=(union_query.type_doc == 'invoice_out')).as_("invoice")
        col_payroll= Sum(union_query.records, filter_=(union_query.type_doc == 'payroll')).as_("payroll")
        col_event= Sum(union_query.records, filter_=(union_query.type_doc == 'event')).as_("event")

        return union_query.select(
            ToChar(union_query.period, 'YYYYMMDD').cast('integer').as_("id"),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            cls.write_uid.sql_cast(Literal(Null)).as_('write_uid'),
            cls.write_date.sql_cast(Literal(Null)).as_('write_date'),
            cls.company.sql_cast(Literal(context.get("company"))).as_('company'),
            union_query.period,
            col_equivalent_invoice,
            col_invoice,
            col_payroll,
            col_event,
            group_by=(ToChar(union_query.period, 'YYYYMMDD').cast('integer'), union_query.period),
        )

    @property
    def time_series_all(self):
        delta = self._period_delta()
        for ts, next_ts in pairwise(self.time_series or []):
            yield ts
            if delta and next_ts:
                date = ts.date + delta
                while date < next_ts.date:
                    yield None
                    date += delta

    @classmethod
    def _period_delta(cls):
        context = Transaction().context
        return {
            'year': relativedelta(years=1),
            'month': relativedelta(months=1),
            'day': relativedelta(days=1),
            }.get(context.get('period'))

    def get_trend(self, name):
        name = name[:-len('_trend')]
        if pygal:
            chart = pygal.Line()
            chart.add('', [getattr(ts, name) if ts else 0
                    for ts in self.time_series_all])
            return chart.render_sparktext()


class AbstractTimeseries(Abstract):
    period = fields.Date("Date")


class Context(ModelView):
    'Electronic Document Context'
    __name__ = 'electronic.document.context'

    company = fields.Many2One('company.company', "Company", required=True)
    from_date = fields.Date("From Date",
        domain=[
            If(Eval('to_date') & Eval('from_date'),
               ('from_date', '<=', Eval('to_date')),
               ()),
        ],
        depends=['from_date'], required=True)
    to_date = fields.Date("To Date",
        domain=[
            If(Eval('from_date') & Eval('to_date'),
                ('to_date', '>=', Eval('from_date')),
                ()),
            ],
        depends=['from_date'], required=True)
    period = fields.Selection([
            ('year', "Year"),
            ('month', "Month"),
            ('day', "Day"),
            ], "Period", required=True)

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def default_from_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        context = Transaction().context
        if 'from_date' in context:
            return context['from_date']
        return Date.today() - relativedelta(years=1)

    @classmethod
    def default_to_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        context = Transaction().context
        if 'to_date' in context:
            return context['to_date']
        return Date.today()

    @classmethod
    def default_period(cls):
        return Transaction().context.get('period', 'month')


class ElectronicDocument(AbstractTimeseries, ModelView):
    "Reporting per Document"
    __name__ = 'reporting.electronic_document'
