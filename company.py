# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'
    token_api = fields.Char('Token Api', readonly=True)
    software_id = fields.Char('Software ID')
    pin_software = fields.Char('Pin Software')

    @classmethod
    def __setup__(cls):
        super(Company, cls).__setup__()

    @staticmethod
    def default_enviroment_type():
        return '1'

    @staticmethod
    def default_broadcast_type():
        return '1'

    def get_password(self, name):
        return 'x' * 10
