# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


PAYMENT_CODES = [
    ('', ''),
    ('1', 'Instrumento no definido'),
    ('10', 'Efectivo'),
    ('44', 'Nota Cambiaria'),
    ('20', 'Cheque'),
    ('48', 'Tarjeta Crédito'),
    ('49', 'Tarjeta Débito'),
    ('42', 'Consignación bancaria'),
    ('47', 'Transferencia Débito Bancaria'),
    ('45', 'Transferencia Crédito Bancaria'),
]


class VoucherPayMode(metaclass=PoolMeta):
    __name__ = 'account.voucher.paymode'

    payment_means_code = fields.Selection(PAYMENT_CODES, 'Payment Means Code',
        required=True)
