# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import base64
import requests
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Id
from trytond.transaction import Transaction
from trytond.model import ModelView, Workflow, ModelSQL, fields
from trytond.wizard import Wizard, StateTransition
# from olimpia_api import login, signed, get_certificates
from trytond.exceptions import UserError

PRIMOS = [71, 67, 59, 53, 47, 43, 41, 37, 29, 23, 19, 17, 13, 7, 3]
K = 11
STATES = {'readonly': (Eval('state') != 'draft')}


class InvoiceAuthorization(metaclass=PoolMeta):
    __name__ = 'account.invoice.authorization'
    api_key = fields.Char('Provider API Key', states={
            'readonly': Eval('state') != 'draft',
            'invisible': Eval('provider') != 'estp',
        })
    software_id = fields.Char('Software ID', states={
            'readonly': Eval('state') != 'draft',
            'required': Eval('provider') == 'psk',
            'invisible': Eval('kind').in_([None, '', 'C', 'P', 'M']),
        })
    pin_software = fields.Char('Pin Software', states={
            'readonly': Eval('state') != 'draft',
            'invisible': Eval('kind').in_([None, '', 'C', 'P', 'M']),
        })
    test_set_id = fields.Char('TestSetId', states={
            'readonly': Eval('state') != 'draft',
            'invisible': Eval('provider') != 'psk',
        })
    technical_key = fields.Char('Technical Key', states={
            'readonly': Eval('state') != 'draft',
            'required': Eval('provider') == 'psk',
            'invisible': Eval('kind').in_([None, '', 'C', 'P', 'M']),
        })
    provider = fields.Selection([
        ('', ''),
        ('estp', 'ESTP'),
        ('psk', 'PSK'),
        ], 'Provider',
        states={
            'readonly': Eval('state') != 'draft',
            'required': Eval('kind').in_(['92', '91', '1', '2', '3', '4']),
            'invisible': Eval('kind').in_([None, '', 'C', 'P', 'M']),
        })
    environment = fields.Selection([
        ('', ''),
        ('1', 'Produccion'),
        ('2', 'Pruebas'),
        ], 'Environment',
            states={
                'readonly': Eval('state') != 'draft',
                'invisible': Eval('kind').in_([None, '', 'C', 'P', 'M']),
                'required': Eval('kind').in_(['92', '91', '1', '2', '3', '4']),
            })
    phase = fields.Selection([
        ('', ''),
        ('2', 'Fase 2'),
        ], 'Phase', states={
            'readonly': Eval('state') != 'draft',
            'invisible': Eval('kind').in_([None, '', 'C', 'P', 'M']),
            'required': Eval('kind').in_(['92', '91', '1', '2', '3', '4']),
        })
    check_digit_provider = fields.Function(fields.Integer('DV', states={
                'readonly': Eval('state') != 'draft',
                'invisible': Eval('kind').in_([None, '', 'C', 'P', 'M']),
            }), 'get_check_digit_provider',)
    send_method_api = fields.Selection([
        ('', ''),
        ('sincrono', 'Sincrono'),
        ('asincrono', 'Asincrono'),
        ], 'Send Method Api', states={
            'invisible': Eval('kind').in_([None, '', 'C', 'P', 'M']),
            'required': Eval('kind').in_(['92', '91', '1', '2', '3', '4']),
        })
    sequence_file = fields.Many2One('ir.sequence.strict', 'Sequence File',
        states=STATES, domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=', Id('electronic_invoice_co', 'sequence_type_xml')),
        ])

    @classmethod
    def __register__(cls, module_name):
        super().__register__(module_name)
        table_h = cls.__table_handler__(module_name)

        # Migration from 5.0: remove user_api password_api point_sale format_id provider comfiar
        table_h.drop_column('user_api')
        table_h.drop_column('password_api')
        table_h.drop_column('point_sale')
        table_h.drop_column('format_id')

    @staticmethod
    def default_send_method_api():
        return 'sincrono'

    @staticmethod
    def default_environment():
        return '2'

    def get_check_digit_provider(self, name):
        if not self.software_provider_id:
            return None
        id_number = self.software_provider_id.replace(".", "")
        if not id_number.isdigit():
            return None
        c = 0
        p = len(PRIMOS) - 1
        for n in reversed(id_number):
            c += int(n) * PRIMOS[p]
            p -= 1

        dv = c % 11
        if dv > 1:
            dv = 11 - dv
        return dv


class UpdateTechnicalKey(Wizard):
    'Update Technical Key'
    __name__ = 'authorization.update_technical_key'
    start_state = 'force_draft'
    force_draft = StateTransition()

    def transition_force_draft(self):
        Authorization = Pool().get('account.invoice.authorization')
        id = Transaction().context['active_id']
        if not id:
            return 'end'
        auth, = Authorization.browse([id])

        HEADERS = {
            "Authorization": 'Bearer {}'.format(auth.company.token_api),
            "Content-type": "application/json",
            "accept": "application/json",
        }

        company_id = auth.company.party.id_number
        software_id = auth.software_id
        api_url = 'https://presik.soenac.com/api/ubl2.1/numbering/range/' + company_id +'/'+ company_id +'/' + software_id
        response = requests.post(api_url, headers=HEADERS, data={})
        res = response.json()
        result = res['responseDian']['Envelope']['Body']['GetNumberingRangeResponse']['GetNumberingRangeResult']
        code = result['OperationCode']
        if code == '100':
            list_res = result['ResponseList']['NumberRangeResponse']
            if isinstance(list_res, list):
                for res in list_res:
                    if res['ResolutionNumber'] == auth.number and res['Prefix'] == auth.sequence.prefix:
                        Authorization.write([auth], {'technical_key': res['TechnicalKey']})
            if isinstance(list_res, dict):
                if list_res['ResolutionNumber'] == auth.number:
                    Authorization.write([auth], {'technical_key': list_res['TechnicalKey']})
        return 'end'


class DigitalCertificate(Workflow, ModelSQL, ModelView):
    'Digital Certificate'
    __name__ = 'account.invoice.digital_certificate'

    start_date = fields.Date('Start Date', required=True,
        states=STATES)
    end_date = fields.Date('End Date', required=True, states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
        states=STATES)
    number = fields.Char('Number', readonly=True)
    certificate = fields.Binary('Certificate', required=True, states=STATES)
    csr_file = fields.Binary('CSR File', states=STATES)
    private_key = fields.Binary('Private Key', states=STATES)
    password = fields.Char('Password', states=STATES)
    certificate_b64 = fields.Function(fields.Char('Certificate B64'),
        'get_certificate_b64')
    state = fields.Selection([
            ('draft', 'Draft'),
            ('active', 'Active'),
            ('finished', 'Finished'),
        ], 'State', readonly=True, select=True)

    @classmethod
    def __setup__(cls):
        super(DigitalCertificate, cls).__setup__()
        cls._order.insert(0, ('number', 'DESC'))
        cls._transitions |= set((
            ('draft', 'active'),
            ('active', 'draft'),
            ('active', 'finished'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'active',
            },
            'active': {
                'invisible': Eval('state') != 'draft',
            },
            'finished': {
                'invisible': Eval('state') != 'active',
            },
        })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    def get_certificate_b64(self, name):
        file64 = ''
        if self.certificate:
            file64 = base64.b64encode(self.certificate)
            file64 = file64.decode('utf-8')
        return file64

    def get_rec_name(self, name):
        number = self.number or ''
        start_date = self.start_date
        end_date = self.end_date
        _rec_name = number + '[' + str(start_date) + ']' + '[' + str(end_date) + ']'
        return _rec_name

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('active')
    def active(cls, records):
        for rec in records:
            rec.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finished(cls, records):
        pass

    def set_number(self):
        pool = Pool()
        Config = pool.get('account.configuration')
        config = Config(1)
        if self.number:
            return
        if not config.certificate_sequence:
            raise UserError('No existe secuencia de certificado')
        number = config.certificate_sequence.get()
        self.write([self], {'number': number})
