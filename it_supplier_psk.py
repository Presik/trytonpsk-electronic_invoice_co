#!/usr/bin/python
from .builder_phase_2 import ElectronicInvoice_2
from .builder_response import TYPE_EVENT, ElectronicResponse
import xmltodict
from .response_supplier_ws import (
    send_invoice_psk, encode_invoice, send_request_status_psk,
)

API_SEND = {
    '1': 'https://presik.soenac.com/api/ubl2.1/send-bill-async',
    '2': 'https://presik.soenac.com/api/ubl2.1/send-test-set-async',
    '3': 'https://presik.soenac.com/api/ubl2.1/status/document/',
    '4': 'https://presik.soenac.com/api/ubl2.1/mail/send/',
    '5': 'https://presik.soenac.com/api/ubl2.1/contingency-billing/',
    '6': 'https://presik.soenac.com/api/ubl2.1/send-event-sync',
}

API_SIGN = {
    '1': 'https://presik.soenac.com/api/ubl2.1/signature/invoice',
    '2': 'https://presik.soenac.com/api/ubl2.1/signature/invoice',
    '91': 'https://presik.soenac.com/api/ubl2.1/signature/credit-note',
    '92': 'https://presik.soenac.com/api/ubl2.1/signature/debit-note',
    '05': 'https://presik.soenac.com/api/ubl2.1/signature/invoice',
    '95': 'https://presik.soenac.com/api/ubl2.1/signature/credit-note', # fix me
    '03': 'https://presik.soenac.com/api/ubl2.1/signature/event'
}


class ElectronicInvoicePsk(object):

    def __init__(self, invoice, auth):
        self.invoice = invoice
        self.auth = auth
        self._create_electronic_invoice_phase_2()

    def _create_electronic_invoice_phase_2(self):
        ec_invoice = ElectronicInvoice_2(
            self.invoice, self.invoice.authorization)
        _type = ''
        if ec_invoice.status != 'ok':
            self.invoice.get_message(ec_invoice.status)
        if self.invoice.credit_note_concept and self.invoice.invoice_type == '91':
            _type = 'CN'
            xml_invoice = ec_invoice.make('CN')
        elif self.invoice.debit_note_concept and self.invoice.invoice_type == '92':
            _type = 'DN'
            xml_invoice = ec_invoice.make('DN')
        elif self.invoice.type == 'in' and self.invoice.invoice_type == '05':
            _type = 'E'
            xml_invoice = ec_invoice.make('E')
        elif self.invoice.type == 'in' and self.invoice.invoice_type == '95':
            _type = 'EN'
            xml_invoice = ec_invoice.make('EN')
        else:
            _type = 'F'
            xml_invoice = ec_invoice.make('F')
        # self.invoice.test_print(xml_invoice, _type)
        self._send_electronic_invoice(xml_invoice)

    def send_request_status_document(invoice):
        res = send_request_status_psk(
            invoice,
            API_SEND['3'],
        )

        if not res:
            return
        #     invoice.get_message('Error de solicitud')

        response = res['responseDian']['Envelope']['Body']['GetStatusResponse']['GetStatusResult']
        if response['IsValid'] == 'true':
            xml_response_dian = encode_invoice(
                response['XmlBase64Bytes'], 'decode')
            invoice.write([invoice], {
                'cufe': response['XmlDocumentKey'],
                'electronic_state': 'authorized',
                'electronic_message': response['StatusMessage'],
                'xml_response_dian_': xml_response_dian.encode('utf8'),
            })
        else:
            cadena = ''
            error_msg = response['ErrorMessage']['string']
            if isinstance(error_msg, list):
                for r in error_msg:
                    cadena += r + '\n'
            else:
                cadena = error_msg
            invoice.write([invoice], {
                'rules_fail': cadena,
                'electronic_message': response['StatusMessage'],
                'electronic_state': 'rejected',
            })
        return

    def _send_electronic_invoice(self, xml_invoice):
        api_send = API_SEND[self.auth.environment]
        res, xml_signed = send_invoice_psk(
            self.invoice,
            xml_invoice,
            api_send,
            API_SIGN[self.invoice.invoice_type],
        )
        if res.get('result') and res.get('result') == 'false':
            self.invoice.get_message(res['message'])
            return
        response = res['responseDian']['Envelope']['Body']
        if 'SendTestSetAsyncResponse' in response:
            self.invoice.write([self.invoice], {
                'electronic_state': 'submitted',
                'cufe': self.invoice.get_cufe(),
                'xml_face_': xml_signed.encode('utf8'),
            })
            return
        else:
            response = res['responseDian']['Envelope']['Body']['SendBillSyncResponse']['SendBillSyncResult']

        if type(response['XmlBase64Bytes']) == dict:
            xml_response_dian = None
        else:
            xml_response_dian = encode_invoice(
                response['XmlBase64Bytes'], 'decode')
            xml_response_dian = xml_response_dian.encode('utf8')

        if response['IsValid'] == 'true':
            self.invoice.write([self.invoice], {
                'cufe': self.invoice.get_cufe(),
                'xml_face_': xml_signed.encode('utf8'),
                'electronic_state': 'authorized',
                'electronic_message': response['StatusMessage'],
                'xml_response_dian_': xml_response_dian,
            })
        else:
            cadena = ''
            error_msg = response['ErrorMessage']['string']
            if isinstance(error_msg, list):
                for r in error_msg:
                    cadena += r + '\n'
            else:
                cadena = error_msg
            self.invoice.write([self.invoice], {
                'rules_fail': cadena,
                'xml_face_': xml_signed.encode('utf8'),
                'electronic_message': response['StatusMessage'],
                'electronic_state': 'rejected',
                'xml_response_dian_': xml_response_dian,
            })

        # except Exception as e:
        #     self.invoice.get_message(e)


class ApplicationResponse(object):

    def __init__(self, invoice, receptionist):
        self.invoice = invoice
        self.receptionist = receptionist
        self.create_application_response()

    def create_application_response(self):
        event_response = ElectronicResponse(self.invoice, self.receptionist)
        event_xml = event_response.make()
        # self.invoice.test_print(event_xml, 'application_response')
        self._send_electronic_event(event_xml)

    def _send_electronic_event(self, event_xml):
        res, xml_signed = send_invoice_psk(
            self.invoice,
            event_xml,
            API_SEND['6'],
            API_SIGN['03'],
            event=True
        )
        if res.get('result') and res.get('result') == 'false':
            self.invoice.get_message(res['message'])
            return

        if res.get('result', None) == 'false' and xml_signed:
            attribute = TYPE_EVENT[self.invoice.type_event]
            self.invoice.event.__class__.write([self.invoice.event], {
                attribute: xml_signed.encode('utf8'),
            })
        else:
            response = res['responseDian']['Envelope']['Body']['SendEventUpdateStatusResponse']['SendEventUpdateStatusResult']
            xml_response_dian = encode_invoice(response['XmlBase64Bytes'], 'decode')
            attribute = TYPE_EVENT[self.invoice.type_event]
            attribute_response = 'response_' + TYPE_EVENT[self.invoice.type_event]
            if response['IsValid'] == 'true':
                self.invoice.event.__class__.write([self.invoice.event], {
                    attribute: xml_signed.encode('utf8'),
                    attribute_response: xml_response_dian.encode('utf8'),
                })
            else:
                cadena = ''
                try:
                    xml_response_dian = res['responseDian']['Envelope']['Body']['SendEventUpdateStatusResponse']['SendEventUpdateStatusResult']
                    xml_response_dian = encode_invoice(response['XmlBase64Bytes'], 'decode')
                    dict_res = xmltodict.parse(xml_response_dian)
                    doc_response = dict_res['ApplicationResponse']['cac:DocumentResponse']
                    if doc_response['cac:Response']['cbc:ResponseCode']  == '02':
                        cude = doc_response['cac:DocumentReference']['cbc:UUID']['#text']
                        if cude == self.invoice.get_cude():
                            self.invoice.event.__class__.write([self.invoice.event], {
                                attribute_response: xml_response_dian.encode('utf8'),
                            })
                        cadena += cude + '\n'
                        if not getattr(self.invoice.event, attribute):
                            self.invoice.event.__class__.write([self.invoice.event], {
                                attribute: xml_signed.encode('utf8'),
                            })
                except:
                    pass
                error_msg = response['ErrorMessage']['string']
                if isinstance(error_msg, list):
                    for r in error_msg:
                        cadena += r + '\n'
                else:
                    cadena += error_msg
                self.invoice.event.__class__.write([self.invoice.event], {
                    'comment': cadena,
                })
                # print(cadena)
